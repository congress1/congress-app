export class ApiUtils {
    static catchLogicListGenerator(xhr, status, app) {
        if (status >= 400 && status <= 499) {
            const response = JSON.parse(xhr.responseText).errors;
            if (response.length > 0) {
                console.log("error: ", response)
                console.log("error0: ", response[0])
                app.dialog.alert(response[0], 'Error');
            } else if (typeof response === 'object') {
                console.log('La respuesta es un objeto');
                console.log(response)
                const firstErrorValue = Object.values(response)[0];
                console.log(firstErrorValue);
                app.dialog.alert(firstErrorValue, 'Error');
            }
        }
    }

    static closeSpinner(app) {
        console.log("fin spinner eAssistance")
        app.dialog.close();
    }


    static loadSpinner(app) {
        app.dialog.preloader('cargando...');
    }

    static rolesAdmins = ['ADMIN', 'SUPPORT_SALES', 'SCANNERS']
    static isAdmin = () => {
        const user = JSON.parse(localStorage.getItem('user'))
        const rol = user.rol
        return rol === 'ADMIN';
    }
    static isScanner = () => {
        const user = JSON.parse(localStorage.getItem('user'))
        const rol = user.rol
        return rol === 'SCANNERS';
    }
    static isSupportSales = () => {
        const user = JSON.parse(localStorage.getItem('user'))
        const rol = user.rol
        return rol === 'SUPPORT_SALES';
    }
    static isPARTICIPANTS = () => {
        const user = JSON.parse(localStorage.getItem('user'))
        const rol = user.rol
        return rol === 'PARTICIPANT';
    }
}