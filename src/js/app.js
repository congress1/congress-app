// Dom7
import Authenticate from './app/authenticate.js';
import university from "./app/university.js";
import profile from "./app/profile.js";
import layout from "./app/layout.js";
import sponsor from "./app/sponsor.js";
import speaker from "./app/speaker.js";
import participant from "./app/participant.js";
import organizer from "./app/organizer.js";
import eAcademic from "./app/event/eAcademic.js";
import eCulturalPartner from "./app/event/eCulturalPartner.js";
import eAssistance from "./app/assistance/eAssistance.js";
import oAcademic from "./app/olimpianeic/oAcademic.js";
import oCulturalPartner from "./app/olimpianeic/oCulturalPartner.js";
import oDeport from "./app/olimpianeic/oDeport.js";
import eRegistration from "./app/registration/eRegistration.js";
import forgotPassword from "./app/forgotPassword/forgotPassword.js";
import inscription from "./app/inscription/index.js";
import home from "./app/home/home.js";
import iParticipant from "./app/inscription/iParticipant.js";
import hSpeaker from "./app/home/speaker.js";
import hSponsor from "./app/home/sponsor.js";
import eSummary from "./app/summary/eSummary.js";
import hLocation from "./app/home/location.js";
import hUniversity from "./app/home/university.js";
import eCalendar from "./app/calendar/index.js";
import confirmCode from "./app/confirmCode/confirmCode.js";
import locations from "./app/locations.js";
import statistics from "./app/statistics/index.js";

import {store} from './store.js'
import {routes} from './routes.js'
import pushNotifications from "./app/pushNotifications";
import question from "./app/question";
import hQuestion from "./app/home/question";
import hOrganizer from "./app/home/organizer";
import user from "./app/user";
import statisticsView from "./app/statistics/view";

var $ = Dom7;

// Theme
// var theme = 'ios';
var theme = 'ios';

// Init App
const app = new Framework7({
    id: 'com.coneic.congress',
    el: '#app',
    theme,
    iosTranslucentBars: false,
    iosTranslucentModals: false,
    view: {
        // stackPages: false,
        animate: false,
    },
    // store.js,
    store: store,
    // routes.js,
    routes: routes,
    clearPreviousHistory: false,
    popup: {
        closeOnEscape: true,
    },
    sheet: {
        closeOnEscape: true,
    },
    popover: {
        closeOnEscape: true,
    },
    actions: {
        closeOnEscape: true,
    },
    vi: {
        placementId: 'pltd4o7ibb9rc653x14',
    },
});
if (app.device.ios) {
    console.log('[DEVICE] It is ios device');
    document.documentElement.style.setProperty('--f7-navbar-height', '120px');
    document.documentElement.style.setProperty('--f7-navbar-padding-top', '80px');
    document.documentElement.style.setProperty('--f7-navbar-padding-bottom', '40px');
    document.documentElement.style.setProperty('--f7-safe-area-top', '0px');
    document.documentElement.style.setProperty('--sidebar-margin-top', '30px');
    // let htmlElement = document.getElementsByTagName('html')[0];
    // htmlElement.classList.remove('ios-translucent-bars');
}
const selectTitleSplashIndex = $('#title-splash-index')
selectTitleSplashIndex.append('Por favor espere...')

const {unsubscribeTopics} = pushNotifications(app)
app.request.setup({
    beforeSend: function (xhr) {
        if (!xhr.requestUrl.includes('auth') && !xhr.requestUrl.includes('password-recovery') && !xhr.requestUrl.includes('validate-code')) {
            const token = localStorage.getItem('token');
            if (token) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'));
            }
        }
    },
    complete: (xhr, status) => {
        if (!xhr.responseURL.includes('/auth')) {
            if (xhr.status === 403 || xhr.status === 401) {
                app.dialog.close()
                console.log('limpiando token')
                const user = JSON.parse(localStorage.getItem('user'))
                unsubscribeTopics(user.rol)
                localStorage.removeItem('token');
                selectTitleSplashIndex.empty()
                selectTitleSplashIndex.append('Cerrando sesión')
                $('.loader-wrap').show();
                setTimeout(function () {
                    $('.loader-wrap').hide();
                    selectTitleSplashIndex.append('Por favor espere...')
                }, 1000);
                setTimeout(function () {
                    app.views.main.router.navigate('/login/', {clearPreviousHistory: true})
                    app.dialog.close()
                }, 100);
            }
        }
    },
});

const {redirectToLogin, login, initLogin, logicForm} = Authenticate(app)
const {generateOptions, closePopup} = layout(app)
$(document).on('page:init', function (e) {
    // Do something here when page loaded and initialized for all pages

    /* coverimg */
    $('.coverimg').each(function () {
        var imgpath = $(this).find('img');
        $(this).css('background-image', 'url(' + imgpath.attr('src') + ')');
        imgpath.hide();
    });

    $('.accordion-toggle').on('click', function () {
        $(this).toggleClass('active')
        $(this).closest('.accordion-list').find('.accordion-content').toggleClass('show')
    })
    console.log('init global')
    generateOptions(app.views.main.router.url)
    $('#logoutAction').on('click', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        localStorage.removeItem('token')
        localStorage.removeItem('user')
        const mainView = app.views.get('.view-main', {
            router: {
                animate: false
            }
        });

        selectTitleSplashIndex.empty()
        if (mainView) {
            selectTitleSplashIndex.append('Cerrando sesión')
            $('.loader-wrap').show();
            setTimeout(function () {
                $('.loader-wrap').hide();
                selectTitleSplashIndex.append('Por favor espere...')
            }, 1000);
            // Limpia el historial del router
            mainView.router.clearPreviousHistory();
            // Si quieres redirigir a la página de inicio o de inicio de sesión, puedes hacerlo así
            mainView.router.navigate('/login/', {clearPreviousHistory: true});
        }
    })
});

// $(document).on('click', 'a', function (e) {
//   console.log('limpiando')
//   e.preventDefault();  // Evitar la navegación predeterminada
//   e.stopImmediatePropagation();
//   // var path = $(this).attr('href');
//   // navigateAndClearHistory(path);  // Navegar manualmente con tu función personalizada
// });
$(document).on('page:afterin', function (e) {
    /* scroll from top and add class */
    $('.view-main .page-current .page-content').on('scroll', function () {
        if ($(this).scrollTop() > '10') {
            $('.view-main .navbar-current').addClass('active');
        } else {
            $('.view-main .navbar-current').removeClass('active');
        }
    });

    /* static footer*/
    if ($('.page.page-current .footer').length > 0) {
        $('.view.view-main .page-content').addClass('has-footer');
    } else {
        $('.view.view-main .page-content').removeClass('has-footer');
    }
    $('.centerbutton .nav-link').on('click', function () {
        $(this).toggleClass('active')
    })


});
$(document).on('page:init', '.page[data-name="login"]', function (e) {
    console.log('login')
    $('#loadingLogin').hide();
    //ini ocultar campo de error de formulario
    $("#login-email-error").hide();
    $("#login-password-error").hide();
    initLogin()
    //fin ocultar campo de error de formulario
    $("#buttonLogin").click(function () {
        login()
    });
})
const oForgotPassword = forgotPassword(app)
$(document).on('page:init', '.page[data-name="forgotpassword"]', function (e) {
    console.log('forgot')
    $('#loadingForgotPassword').hide();
    //ini ocultar campo de error de formulario
    $("#fg-login-email-error").hide();
    //fin ocultar campo de error de formulario
    $("#buttonForgotPassword").click(function () {
        oForgotPassword.sendCode()
    });
})
$(document).on('page:init', '.page[data-name="sendCode"]', function (e) {
    console.log('sendCode')
    oForgotPassword.validateCode()
})

const oConfirmCode = confirmCode(app)
$(document).on('page:init', '.page[data-name="confirmCode"]', function (e) {
    console.log('sendCode')
    oConfirmCode.validateCode()
})
$(document).on('page:init', '.page[data-name="resetPassword"]', function (e) {
    console.log('resetPassword')
    $('#loadingResetPassword').hide();
    //ini ocultar campo de error de formulario
    $("#fg-new-password-error").hide();
    //fin ocultar campo de error de formulario
    $("#buttonResetPassword").click(function () {
        oForgotPassword.changePassword()
    });
})
$(document).on('page:init', '.page[data-name="configuration"]', function (e) {
    /* dark mode switch*/
    let isDarkModeEnabled = $('html').hasClass('theme-dark');
    if (isDarkModeEnabled) {
        $('#darkmodeswitch').prop('checked', true);
    }
    $('#darkmodeswitch').on('click', function () {
        if ($(this).is(':checked')) {
            $('html').addClass('theme-dark');
        } else {
            $('html').removeClass('theme-dark');
        }
    });
})
$(document).on('page:init', '.page[data-name="splash"]', function (e) {
    setTimeout(function () {
        $('.loader-wrap').hide();
    }, 1000);

    setTimeout(function () {
        // app.views.main.router.navigate('/login/');
        redirectToLogin()
    }, 500);

    console.log('verificando...')
})


/* pwa app install */
var deferredPrompt;
window.addEventListener('beforeinstallprompt', function (e) {
    console.log('beforeinstallprompt Event fired');
    e.preventDefault();
    deferredPrompt = e;
    return false;
});
$(document).on('page:init', '.page[data-name="events"]', function (e) {
    console.log('event')
})

const {initGenerateCrud, initForm, generateList} = university(app)
$(document).on('page:init', '.page[data-name="events_2"]', function (e) {
    console.log('eventos 2')
})
$(document).on('page:init', '.page[data-name="panel-university"]', function (e) {
    console.log('panel-university')
    initGenerateCrud()
    initForm()
    generateList()
})
const obSponsor = sponsor(app)
$(document).on('page:init', '.page[data-name="panel-sponsor"]', function (e) {
    console.log('panel-sponsor')
    obSponsor.initGenerateCrud()
    obSponsor.initForm()
    obSponsor.generateList()
})
const obSpeaker = speaker(app)
$(document).on('page:init', '.page[data-name="panel-speaker"]', function (e) {
    console.log('panel-speaker')
    obSpeaker.initGenerateCrud()
    obSpeaker.initForm()
    obSpeaker.generateList()
})
const obParticipant = participant(app)
$(document).on('page:init', '.page[data-name="panel-participant"]', function (e) {
    console.log('panel-participant')
    obParticipant.initGenerateCrud()
    obParticipant.initForm()
    obParticipant.generateList()
})
const obOrganizer = organizer(app)
$(document).on('page:init', '.page[data-name="panel-organizer"]', function (e) {
    console.log('panel-organizer')
    obOrganizer.initGenerateCrud()
    obOrganizer.initForm()
    obOrganizer.generateList()
})
const obLocation = locations(app)
$(document).on('page:init', '.page[data-name="panel-location"]', function (e) {
    console.log('panel-location')
    obLocation.initGenerateCrud()
    obLocation.initForm()
    obLocation.generateList()
})
const obQuestion = question(app)
$(document).on('page:init', '.page[data-name="panel-question"]', function (e) {
    console.log('panel-question')
    obQuestion.initGenerateCrud()
    obQuestion.initForm()
    obQuestion.generateList()
})
const obUser = user(app)
$(document).on('page:init', '.page[data-name="panel-user"]', function (e) {
    console.log('panel-question')
    obUser.initGenerateCrud()
    obUser.initForm()
    obUser.generateList()
})
$(document).on('page:init', '.page[data-name="scanner-qr"]', function (e) {

    /* escaner qr*/

    /* fin escaner qr*/
})

$(document).on('page:init', '.page[data-name="home"]', function (e) {
    const iHome = home(app)
    console.log('app ->', app)
    iHome.initHome()
    iHome.showBanner()
})
const iHSpeaker = hSpeaker(app)
$(document).on('page:init', '.page[data-name="home-speaker-view"]', function (e) {
    iHSpeaker.init()
})
$(document).on('page:init', '.page[data-name="home-speakers"]', function (e) {
    iHSpeaker.listSpeaker()
})
const iHQuestion = hQuestion(app)
$(document).on('page:init', '.page[data-name="home-questions"]', function (e) {
    iHQuestion.init()
})
const iHOrganizer = hOrganizer(app)
$(document).on('page:init', '.page[data-name="home-organizer-view"]', function (e) {
    iHOrganizer.init()
})
$(document).on('page:init', '.page[data-name="home-organizers"]', function (e) {
    iHOrganizer.listOrganizer()
})
const iHSponsor = hSponsor(app)
$(document).on('page:init', '.page[data-name="home-sponsor-view"]', function (e) {
    iHSponsor.init()
})
$(document).on('page:init', '.page[data-name="home-sponsors"]', function (e) {
    iHSponsor.listSponsor()
})
const iHLocation = hLocation(app)
$(document).on('page:init', '.page[data-name="home-location-list"]', function (e) {
    iHLocation.init()
})
const iHUniversity = hUniversity(app)
$(document).on('page:init', '.page[data-name="home-university-list"]', function (e) {
    iHUniversity.init()
})

const opEAcademic = eAcademic(app)
$(document).on('page:init', '.page[data-name="event-academic"]', function (e) {
    console.log('event-academic')
    opEAcademic.init()
})
const opECulturalPartner = eCulturalPartner(app)
$(document).on('page:init', '.page[data-name="event-cultural-partner"]', function (e) {
    console.log('event-cultural-partner')
    opECulturalPartner.init()
})
const {submit, assistanceManual, initLinksAssistance, viewParticipant, loadQr} = eAssistance(app)
$(document).on('page:init', '.page[data-name="event-assistance"]', function (e) {
    initLinksAssistance()
    /* escaner qr*/
    loadQr()
    /* fin escaner qr*/
})
const oSummary = eSummary(app)
$(document).on('page:init', '.page[data-name="event-summary"]', function (e) {
    // initLinksAssistance()
    console.log('load event summary')
    oSummary.init()
})
$(document).on('page:init', '.page[data-name="event-assistance-qr"]', function (e) {
    console.log('event-assistance-qr')
    /* escaner qr*/
    // loadQr()
    // /* fin escaner qr*/
    // $("#close-qr").on("click", function() {
    //   console.log('close')
    // })
})
$(document).on('page:init', '.page[data-name="event-assistance-qr"]', function (e) {
    console.log('events-academic-assistance-manual')
    initLinksAssistance()
    assistanceManual()
})
const oParticipant = iParticipant(app)
$(document).on('page:init', '.page[data-name="events-assistance-participant"]', function (e) {
    console.log('events-academic-assistance-participant')
    initLinksAssistance()
    viewParticipant()
    oParticipant.init()
})

const olAcademic = oAcademic(app)
$(document).on('page:init', '.page[data-name="olimpianeic-academic"]', function (e) {
    console.log('olimpianeic-academic')
    olAcademic.init()
})
$(document).on('page:init', '.page[data-name="olimpianeic-cultural-partner"]', function (e) {
    console.log('olimpianeic-cultural-partner')
    oCulturalPartner(app).init()
})
$(document).on('page:init', '.page[data-name="olimpianeic-deport"]', function (e) {
    console.log('olimpianeic-deport')
    oDeport(app).init()
})
const oInscription = inscription(app)
$(document).on('page:init', '.page[data-name="inscription"]', function (e) {
    console.log('olimpianeic-deport')
    oInscription.init()
})
$(document).on('page:init', '.page[data-name="inscription-view"]', function (e) {
    console.log('olimpianeic-deport')
    oInscription.detail()
})
$(document).on('page:init', '.page[data-name="events-registration"]', function (e) {
    console.log('events-registration')
    eRegistration(app).registration()
})

$(document).on('page:init', '.page[data-name="profile-change-password"]', function (e) {
    console.log('profile-profile')
    profile(app).changePassword()
})
$(document).on('page:init', '.page[data-name="profile-profile"]', function (e) {
    console.log('profile-profile')
    profile(app).initProfile()
})
$(document).on('page:init', '.page[data-name="profile-benefit"]', function (e) {
    console.log('profile-benefit')
})
$(document).on('page:init', '.page[data-name="calendar"]', function (e) {
    console.log('calendar')
    eCalendar(app).init()
})
$(document).on('page:init', '.page[data-name="statistics"]', function (e) {
    console.log('statistics')
    statistics(app).init()
})
$(document).on('page:init', '.page[data-name="statistics-inscription-university-view"]', function (e) {
    console.log('statistics-inscription-university-view')
    statisticsView(app).viewInscriptionUniversity()
})
$(document).on('page:init', '.page[data-name="statistics-rank-university-view"]', function (e) {
    console.log('statistics-rank-university-view')
    statisticsView(app).viewRankUniversity()
})
$(document).on('page:init', '.page[data-name="statistics-event-with-most-income-view"]', function (e) {
    console.log('statistics-event-with-most-income-view')
    statisticsView(app).viewEventWithMostIncome()
})
$(document).on('page:init', '.page[data-name="statistics-olimpianeic-with-most-income-view"]', function (e) {
    console.log('statistics-olimpianeic-with-most-income-view')
    statisticsView(app).viewOlimpianeicWithMostIncome()
})