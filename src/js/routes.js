export var routes = [

    // Splash page
    {
        path: '/',
        // redirect: function ({to, resolve, reject}) {
        //   const isLogin = true
        //   // if we have "user" query parameter
        //   if (isLogin) {
        //     // redirect to such url
        //     resolve('/home/');
        //   }
        //   // otherwise do nothing
        //   else {
        //     resolve('/login/')
        //   }
        // },
        // redirect: '/home/',
        url: './pages/splash.html',
        // name: 'splash',
    },

    // Index page
    {
        path: '/home/',
        componentUrl: './pages/home.html',
        name: 'home',
        routes: []
    },
    {
        path: '/home-location/list/',
        componentUrl: './pages/app/location/list.html',
        name: 'home-location-list'
    },
    {
        path: '/home-university/list/',
        componentUrl: './pages/app/university/list.html',
        name: 'home-university-list'
    },
    {
        path: '/home-speaker/:speakerId/',
        componentUrl: './pages/app/speaker/view.html',
        name: 'home-speaker-view'
    },
    {
        path: '/home-speakers/',
        componentUrl: './pages/app/speaker/index.html',
        name: 'home-speakers'
    },
    {
        path: '/home-sponsor/:sponsorId/',
        componentUrl: './pages/app/sponsor/view.html',
        name: 'home-sponsor-view'
    },
    {
        path: '/home-sponsors/',
        componentUrl: './pages/app/sponsor/index.html',
        name: 'home-sponsors'
    },
    {
        path: '/home-questions/',
        componentUrl: './pages/app/question/index.html',
        name: 'home-questions'
    },
    {
        path: '/home-organizers/',
        componentUrl: './pages/app/organizer/index.html',
        name: 'home-organizers'
    },
    {
        path: '/home-organizer/:organizerId/',
        componentUrl: './pages/app/organizer/view.html',
        name: 'home-organizer-view'
    },
    // statistics
    {
        path: '/statistics/',
        componentUrl: './pages/app/statistics/index.html',
        name: 'statistics',
    },
    {
        path: '/statistics-rank-university-view/',
        componentUrl: './pages/app/statistics/viewRankUniversity.html',
        name: 'statistics-rank-university-view',
    },
    {
        path: '/statistics-inscription-university-view/',
        componentUrl: './pages/app/statistics/viewInscriptionUniversity.html',
        name: 'statistics-inscription-university-view',
    },
    {
        path: '/statistics-event-with-most-income-view/',
        componentUrl: './pages/app/statistics/viewEventWithMostIncome.html',
        name: 'statistics-event-with-most-income-view',
    },
    {
        path: '/statistics-olimpianeic-with-most-income-view/',
        componentUrl: './pages/app/statistics/viewOlimpianeicWithMostIncome.html',
        name: 'statistics-olimpianeic-with-most-income-view',
    },
    // calendar
    {
        path: '/calendar/',
        componentUrl: './pages/app/calendar/index.html',
        name: 'calendar',
    },
    // events
    {
        path: '/event-academic/',
        componentUrl: './pages/app/event/academic/index.html',
        name: 'event-academic',
    },
    {
        path: '/event-academic/assistance/:eventId/',
        componentUrl: './pages/app/event/academic/assistance.html',
        name: 'event-academic-assistance',
    },
    {
        path: '/event-academic/assistance-participant/:eventId/',
        componentUrl: './pages/app/event/academic/assistance-participant.html',
        name: 'event-academic-assistance-participant'
    },
    {
        path: '/event/assistance/:eventId/:type/',
        componentUrl: './pages/app/assistance/index.html',
        name: 'event-assistance',
    },
    {
        path: '/event/summary/:eventId/:type/',
        componentUrl: './pages/app/summary/index.html',
        name: 'event-summary',
    },
    {
        path: '/event/registration/:eventId/',
        componentUrl: './pages/app/registration/index.html',
        name: 'event-registration',
    },
    {
        path: '/event/assistance/qr/:eventId/',
        componentUrl: './pages/app/assistance/qr.html',
        name: 'event-academic-assistance-qr'
    },
    // {
    //   path: '/event/assistance-manual/:eventId/',
    //   componentUrl: './pages/app/assistance/manual.html',
    //   name: 'event-assistance-manual'
    // },
    {
        path: '/event/assistance-participant/:eventId/:type/',
        componentUrl: './pages/app/assistance/participant.html',
        name: 'event-assistance-participant'
    },
    {
        path: '/event-cultural-partner/',
        componentUrl: './pages/app/event/cultural-partner.html',
        name: 'event-cultural-partner'
    },
    // events
    {
        path: '/olimpianeic-academic/',
        componentUrl: './pages/app/olimpianeic/academic.html',
        name: 'olimpianeic-academic'
    },
    {
        path: '/olimpianeic-cultural-partner/',
        componentUrl: './pages/app/olimpianeic/cultural-partner.html',
        name: 'olimpianeic-cultural-partner'
    },
    {
        path: '/olimpianeic-deport/',
        componentUrl: './pages/app/olimpianeic/deport.html',
        name: 'olimpianeic-deport'
    },
    {
        path: '/inscription/',
        componentUrl: './pages/app/inscription/index.html',
        name: 'inscription'
    },
    {
        path: '/inscription-view/:inscriptionId/',
        componentUrl: './pages/app/inscription/view.html',
        name: 'inscription-view'
    },
    {
        path: '/inscription-view/:inscriptionId/:eventId',
        componentUrl: './pages/app/inscription/view.html',
        name: 'inscription-view'
    },
    // inicio panel

    {
        path: '/scanner-qr/',
        componentUrl: './pages/app/qr/scanner.html',
        name: 'scanner-qr'
    },
    {
        path: '/panel/university/',
        componentUrl: './pages/panel/university.html',
        name: 'panel-university'
    },
    {
        path: '/panel/sponsor/',
        componentUrl: './pages/panel/sponsor.html',
        name: 'panel-sponsor'
    },
    {
        path: '/panel/speaker/',
        componentUrl: './pages/panel/speaker.html',
        name: 'panel-speaker'
    },
    {
        path: '/panel/participant/',
        componentUrl: './pages/panel/participant.html',
        name: 'panel-participant'
    },
    {
        path: '/panel/organizer/',
        componentUrl: './pages/panel/organizer.html',
        name: 'panel-organizer'
    },
    {
        path: '/panel/location/',
        componentUrl: './pages/panel/location.html',
        name: 'panel-location'
    },
    {
        path: '/panel/question/',
        componentUrl: './pages/panel/question.html',
        name: 'panel-question'
    },
    {
        path: '/panel/user/',
        componentUrl: './pages/panel/user.html',
        name: 'panel-user'
    },
    // stats
    {
        path: '/stats/',
        url: './pages/stats.html',
        name: 'stats'
    },
    // shop
    {
        path: '/shop/',
        url: './pages/shop.html',
        name: 'shop'
    },
    {
        path: '/allproducts/',
        url: './pages/allproducts.html',
        name: 'allproducts'
    },
    {
        path: '/product/',
        url: './pages/product.html',
        name: 'product'
    },
    {
        path: '/cart/',
        url: './pages/cart.html',
        name: 'cart'
    },
    {
        path: '/myorders/',
        url: './pages/myorders.html',
        name: 'myorders'
    },
    {
        path: '/payment/',
        url: './pages/payment.html',
        name: 'payment'
    },
    {
        path: '/invoice/',
        url: './pages/invoice.html',
        name: 'invoice'
    },
    {
        path: '/track/',
        url: './pages/trackorder.html',
        name: 'track'
    },
    // style
    {
        path: '/style/',
        componentUrl: './pages/style.html',
    },

    // style
    {
        path: '/profile-profile/',
        componentUrl: './pages/app/profile/profile.html',
        name: 'profile-profile'
    },
    {
        path: '/profile-change-password/',
        componentUrl: './pages/app/profile/change-password.html',
        name: 'profile-change-password'
    },
    {
        path: '/benefit/',
        componentUrl: './pages/app/benefit/index.html',
        name: 'benefit'
    },
    // chat
    {
        path: '/chat/',
        url: './pages/chat.html',
    },

    // messages
    {
        path: '/messages/',
        componentUrl: './pages/messages.html',
    },

    // notification list
    {
        path: '/notificationlist/',
        url: './pages/notificationlist.html',
    },

    // settings
    {
        path: '/settings/',
        url: './pages/settings.html',
    },


    // Components
    {
        path: '/components/',
        componentUrl: './pages/components.html',
    },

    // pages
    {
        path: '/pages/',
        url: './pages/pages.html',
    },

    // About us
    {
        path: '/aboutus/',
        url: './pages/aboutus.html',
    },

    // Blogs
    {
        path: '/blogs/',
        url: './pages/blogs.html',
    },

    // blog details
    {
        path: '/blogdetails/',
        url: './pages/blogdetails.html',
    },

    // landing
    {
        path: '/landing/',
        url: './pages/landing.html',
    },

    // users
    {
        path: '/userlist/',
        url: './pages/userlist.html',
    },

    // login
    {
        path: '/login/',
        componentUrl: './pages/login.html',
    },

    // register
    {
        path: '/register/',
        componentUrl: './pages/register.html',
    },

    // forgot password
    {
        path: '/forgotpassword/',
        componentUrl: './pages/forgotPassword/index.html',
        name: 'forgotpassword',
    },
    {
        path: '/sendCode/:email/',
        componentUrl: './pages/forgotPassword/sendCode.html',
        name: 'sendCode',
    },
    {
        path: '/resetPassword/:email/:code/',
        componentUrl: './pages/forgotPassword/resetPassword.html',
        name: 'resetPassword',
    },
    {
        path: '/confirmCode/:email/',
        componentUrl: './pages/confirmCode/index.html',
        name: 'confirmCode',
    },
    {
        path: '/configuration/',
        componentUrl: './pages/configuration/index.html',
        name: 'configuration',
    },

    // reset password
    {
        path: '/resetpassword/',
        componentUrl: './pages/resetpassword.html',
    },

    // verify
    {
        path: '/verify/',
        componentUrl: './pages/verify.html',
    },

    // Thank you
    {
        path: '/thankyou/',
        url: './pages/thankyou.html',
    },
    {
        path: '/thankyou2/',
        url: './pages/thankyou2.html',
    },
    {
        path: '/thankyou3/',
        url: './pages/thankyou3.html',
    },
    {
        path: '/thankyou4/',
        url: './pages/thankyou4.html',
    },

    // payments
    {
        path: '/pay/',
        url: './pages/pay.html',
    },
    {
        path: '/receivemoney/',
        url: './pages/receivemoney.html',
    },
    {
        path: '/bills/',
        url: './pages/bills.html',
    },
    {
        path: '/sendmoney/',
        url: './pages/sendmoney.html',
    },
    {
        path: '/sendmoney1/',
        url: './pages/sendmoney1.html',
    },
    {
        path: '/sendmoney2/',
        url: './pages/sendmoney2.html',
    },
    {
        path: '/sendmoney3/',
        url: './pages/sendmoney3.html',
    },
    {
        path: '/rewards/',
        url: './pages/rewards.html',
    },
    {
        path: '/allmyrequests/',
        url: './pages/allmyrequests.html',
    },
    {
        path: '/wallet/',
        url: './pages/wallet.html',
    },
    {
        path: '/addmoney/',
        url: './pages/addmoney.html',
    },
    {
        path: '/withdraw/',
        url: './pages/withdraw.html',
    },
    // FAQs
    {
        path: '/faqs/',
        url: './pages/faqs.html',
    },


    // Contact us
    {
        path: '/contactus/',
        url: './pages/contactus.html',
    },

    // Terms and conditions
    {
        path: '/termsandconditions/',
        url: './pages/termsandconditions.html',
    },

    // Default route (404 page). MUST BE THE LAST
    {
        path: '(.*)',
        url: './pages/404.html',
    },


];
