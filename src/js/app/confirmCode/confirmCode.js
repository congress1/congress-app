import getRouteApi from "../api.js";
import pushNotifications from "../pushNotifications/index.js";

const $ = Dom7;

export default function confirmCode(app) {
    const {initAndroid} = pushNotifications(app)

    // const decodeJwt = (token) => {
    //     var base64Url = token.split('.')[1]; // Tomar sólo el payload
    //     var base64 = base64Url.replace('-', '+').replace('_', '/'); // Reemplazar caracteres no válidos
    //     return JSON.parse(window.atob(base64)); // Convertir de Base64 a string y parsear a JSON
    // }

    const decodeJwt = (token) => {
        try {

            const base64Url = token.split('.')[1];

            let base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
            switch (base64.length % 4) {
                case 2:
                    base64 += '==';
                    break;
                case 3:
                    base64 += '=';
                    break;
            }

            const jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
            }).join(''));

            return JSON.parse(jsonPayload);
        } catch (e) {
            console.error('Invalid JWT', e);
            return null;
        }
    };

    const loadSpinner = () => {
        console.log("inicio spinner home/Speaker")
        app.dialog.preloader('cargando...');
    }

    const closeSpinner = () => {
        console.log("fin spinner home/Speaker")
        app.dialog.close();
    }
    const resendCode = (email) => {
        console.log('buttonResendCode')
        const sButtonResendCode = $('#buttonResendCode')
        sButtonResendCode.on('click', () => {
            const formForgotPassword = {
                email: email
            }
            loadSpinner()
            console.log('click')
            app.request({
                url: `${getRouteApi()}/congress/v1/api/mail/send-code?type=2`,
                method: 'POST',
                contentType: 'application/json',
                data: formForgotPassword,
                success: function (data, status, xhr) {
                    console.log(data)
                    app.dialog.alert(JSON.parse(data).message, 'Éxito');
                    closeSpinner()
                },
                error: function (xhr, status) {
                    console.log('Error: ' + status);
                    console.log(JSON.parse(xhr.responseText).errors)
                    const response = JSON.parse(xhr.responseText).errors;
                    app.dialog.alert(response[0], 'Error');
                    closeSpinner()
                }
            });
        })
    }
    const validateCode = () => {
        const email = app.views.main.router.currentRoute.params.email
        $('.digit-input-2').on('input', function () {
            const input = $(this);
            if (input.val().length) {
                const nextInput = input.next();
                if (nextInput.length) {
                    // Si hay otro cuadro de texto, mover el enfoque.
                    nextInput.focus();
                } else {
                    let value = '';
                    $('.digit-input-2').each(function () {
                        value += $(this).val();
                    });
                    const requestForgotPassword = {
                        "email": email,
                        "code": value,
                        "type": "2"
                    }
                    app.request({
                        url: `${getRouteApi()}/congress/v1/api/mail/validate-code`,
                        method: 'POST',
                        contentType: 'application/json',
                        data: requestForgotPassword,
                        success: function (data, status, xhr) {

                            localStorage.setItem("token", JSON.parse(data).data)
                            // Decodificar el token
                            var decodedToken = decodeJwt(data);

                            console.log('decodedToken', decodedToken);
                            localStorage.setItem("user", JSON.stringify(decodedToken))
                            initAndroid()
                            app.views.main.router.navigate('/home/', {clearPreviousHistory: true});
                        },
                        error: function (xhr, status) {
                            console.log('Error: ' + status);
                            const response = JSON.parse(xhr.responseText).errors;
                            app.dialog.alert(response[0], 'Error');
                            // console.log(JSON.parse(xhr.responseText).errors)
                            // const response = JSON.parse(xhr.responseText).errors;
                            // app.dialog.alert(response[0], 'Error');

                            $('.digit-input-2').val('');
                            $('.digit-input-2').first().focus();
                        }
                    });
                }
            }
        });
        resendCode(email)
    }

    return {
        validateCode
    }
}