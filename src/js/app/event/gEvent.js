import getRouteApi from "../api.js";
import FormGenerator from "../formGenerator.js";
import {ApiUtils} from "../../utils/apiUtils";

const $ = Dom7;

export default class GEvent {
    idComponent = 'content'
    popupClass = 'popupClassAcademic'
    idContentPopup = 'idContentPopup'
    idContentMap = 'map-container-e-academic'
    linkAssistance = 'goAssistant'
    linkRegistration = 'goRegistration'
    eventRegistration = 'goRegistration'
    idAddButton = 'addButton'
    idPopup = 'popup'
    containerFormId = 'containerFormId'
    dataList = []
    dataEvent = {}
    select = {}
    idEvent = null
    attributes = []
    specialEvent = null
    randomNumber = null

    constructor(app, name, code, attributes, hasRegistration = false, specialEvent = 'normal') {
        this.app = app;
        this.name = name
        this.idComponent = this.name + '-' + this.idComponent
        this.popupClass = this.name + '-' + this.popupClass
        this.idContentPopup = this.name + '-' + this.idContentPopup
        this.idContentMap = this.name + '-' + this.idContentMap
        this.linkAssistance = this.name + '-' + this.linkAssistance
        this.linkRegistration = this.name + '-' + this.linkRegistration
        this.eventRegistration = this.name + '-' + this.eventRegistration
        this.idAddButton = this.name + '-' + this.idAddButton
        this.idPopup = this.name + '-' + this.idPopup
        this.containerFormId = this.name + '-' + this.containerFormId
        this.code = code
        this.hasRegistration = hasRegistration
        this.attributes = attributes
        this.specialEvent = specialEvent
        this.randomNumber = Math.floor(Math.random() * 1000000)

        this.app.on('reloadListevent' + this.randomNumber, (data) => {
            this.app.popup.close(`.${this.idPopup}`)
            this.init()
        });
    }

    loadSpinner() {
        console.log("inicio spinner gEvent")
        this.app.dialog.preloader('cargando...');
    }

    closeSpinner() {
        console.log("fin spinner gEvent")
        this.app.dialog.close();
    }

    resolveTitle() {
        if (this.idEvent) {
            return 'Editar'
        }
        return 'Registrar'
    }

    createList = () => {
        console.log('dataList', this.dataList)
        const sIdComponet = $(`#${this.idComponent}`)
        sIdComponet.empty()


        $(`#${this.containerFormId}-case`).empty()
        const sIdComponent = `#${this.idComponent}`
        const sIdComponentData = `#${this.idComponent}-data`
        let cardElement = `
            <div style="margin-bottom: 10px">
                <form data-search-container=".search-list" data-search-in=".item-title" class="searchbar searchbar-init search-${this.randomNumber}">
                    <div class="searchbar-inner">
                        <div class="searchbar-input-wrap">
                            <input type="search" placeholder="Buscar"/>
                            <i class="searchbar-icon"></i>
                            <span class="input-clear-button"></span>
                        </div>
                        <span class="searchbar-disable-button if-not-aurora">Cancel</span>
                    </div>
                </form>
            </div>
            <div id="${this.idComponent}-data"></div>
        `;

        $(sIdComponent).append(cardElement);
        setTimeout(() => {
            document.querySelector(`form.search-${this.randomNumber}`).addEventListener('submit', function (evt) {
                evt.preventDefault();
                // whatever you want to do next
                console.log('Prevented search');
            });
        }, 0);
        //agregar evento de input search

        $('input[type="search"]').on('input', (event) => {
            let searchVal = $(event.target).val().toLowerCase();
            console.log('search')
            $(`#${this.idComponent}-data .file`).each((index) => {
                let tdElement = $(index).find('.title-event');
                if (tdElement.length > 0) {
                    let cellText = tdElement.text();
                    if (cellText === null) {
                        console.error("No text in cell:", tdElement);
                    } else {
                        cellText = cellText.toLowerCase();
                        if (!cellText.includes(searchVal)) {
                            // El texto de búsqueda no coincide, oculta la fila
                            $(index).hide();
                        } else {
                            // El texto de búsqueda coincide, muestra la fila
                            $(index).show();
                        }
                    }
                }
            })
        })
        this.dataList.forEach((item, index) => {
            // Create new card element
            let cardElement = `<div id="userCard${index}" class="card margin-bottom-half file">
                                <div class="card-content card-content-padding">
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="card">
                                                <div class="card-content padding-half-sm">
                                                    <div class="avatar avatar-44 elevation-2 rounded-15">
                                                        <img src="img/app/isotipo.png" alt="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col align-self-center no-padding-left">
                                             <p class="small margin-bottom-half"><div class="fw-medium mr-4 title-event">${item.name}</div>
                                            </p>
                                            ${this.hasRegistration ? ((item.is_group ?? false) ? '<p>Grupal (Cupos: ' + item.available_seats + ')</p>' : '<p>Individual (Cupos: ' + item.available_seats + ')</p>') : ''}
                                            ${this.hasRegistration ? ((item.is_group ?? false) ? '<p>Minimo: ' + item.minimum + ' Máximo: ' + item.maximum + ' </p>' : '') : ''}
                                            <p>
                                                 ${item.date} hasta ${item.end_date}
                                            </p>
                                            <p class="d-flex justify-content-between align-items-baseline">
                                                ${item.start_time} a ${item.end_time}
                                                 ${this.getEventStatus(item.date, item.end_date, item.start_time, item.end_time) === 'Evento finalizado' ?
                '<span class="badge color-red">' + this.getEventStatus(item.date, item.end_date, item.start_time, item.end_time) + '</span>'
                : '<span class="badge color-green">' + this.getEventStatus(item.date, item.end_date, item.start_time, item.end_time) + '</span>'}
                                                 
                                            </p>
                                            <p class="d-flex justify-content-right align-items-baseline">
                                               <span class="badge color-deeporange"> Asistentes: ${item.number_attendees} </span>                                         
                                            </p>
                                        </div>
                                        <div class="col-auto">
                                        </div>
                                    </div>
                                </div>
                            </div>`;

            $(sIdComponentData).append(cardElement);
            $(`#userCard${index}`).click(() => {
                console.log('abrir modal')
                // this.app.preloader.show();
                this.loadSpinner();
                this.app.popup.open(`.${this.popupClass}`);
                console.log("popupClass :: ", this.popupClass)
                console.log("item modal :: ", item)
                this.getDataEvent(item.id)
                // console.log("valor nuevo despues del request :: ",this.dataEvent.name)
                // this.app.preloader.hide();
            })
        });
        const role = JSON.parse(localStorage.getItem('user'))?.rol
        if (role === 'ADMIN') {
            const addButton =
                `
            <button class="floating-button" id="${this.idAddButton}">+</button>
            `
            sIdComponet.append(addButton)
            let initPopup = `
            <div class="popup ${this.idPopup}">
              <div class="page">
                <div class="navbar">
                  <div class="navbar-bg"></div>
                  <div class="navbar-inner">
                    <div class="title" id="popup-title">${this.resolveTitle()}</div>
                    <div class="right"><a href="#" class="link popup-close" id="popup-close">Cerrar</a></div>
                  </div>
                </div>
                <div class="page-content">              
                    <div id="${this.containerFormId}"></div>
                </div>
              </div>
            </div>
            `;
            sIdComponet.append(initPopup)
            $(`#${this.idAddButton}`).click(() => {
                console.log('new event')
                this.openFormulario()
            })
        }
    }
    openFormulario = (id = null) => {
        console.log("open formula")
        this.app.popup.open(`.${this.idPopup}`)
        if (this.specialEvent === 'normal') {
            console.log('this.specialEvent -> normal')
            const formGenerator = new FormGenerator('event', this.attributes, this.containerFormId, this.idAddButton, this.app, this.randomNumber)
            //formGenerator.clearForm()
            formGenerator.initForm()
            formGenerator.setPopupClass(this.idPopup)
            if (id) {
                formGenerator.setId(id)
            }
        } else {
            this.specialEventParty(this.containerFormId)
        }
        // this.closeSpinner();
    }
    specialEventParty = (container) => {
        //obtener datos
        $(`#${container}`).empty()
        this.app.request({
            url: `${getRouteApi()}/congress/v1/api/setting/list-internal-code/TICL000?internalCode=true`,
            method: 'GET',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                const dataL = JSON.parse(data)
                console.log('specialEventParty',)
                const dataTemp = dataL.data;
                const options = dataTemp.map(option => `<option value="${option.code}">${option.name}</option>`).join('');

                const html = `
            <div class="row margin-bottom margin-top">
                <div class="col-100 medium-50 large-33 margin-bottom-half">
                    <div class="list form-list no-margin">
                        <ul>
                            <li class="item-content item-input">
                                <div class="item-inner">
                                    <div class="item-title item-floating-label">Tipo de evento</div>
                                    <div class="item-input-wrap">
                                        <select required id="classCode">
                                            ${options}
                                        </select>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="${container}-case"></div>`
                $(`#${container}`).append(html)
                const sClassCode = $(`#classCode`)
                sClassCode.val(null)
                sClassCode.on('change', e => {
                    $(`#${container}-case`).empty()
                    let attributesTemp = []
                    if (sClassCode.val() === 'TICL003') {
                        attributesTemp = [
                            {
                                label: 'Nombre',
                                value: 'name',
                                type: 'Text',
                                isShow: true,
                                required: true
                            },
                            {
                                label: 'Descripción',
                                value: 'description',
                                type: 'Text',
                                isShow: true,
                                required: true
                            },
                            {
                                label: 'Fecha Inicio',
                                value: 'date',
                                type: 'Date',
                                isShow: true,
                                default: new Date(),
                                required: true
                            },
                            {
                                label: 'Fecha Fin',
                                value: 'endDate',
                                type: 'Date',
                                values: {
                                    valueEdit: 'end_date'
                                },
                                isShow: true,
                                default: new Date(),
                                required: true
                            },
                            {
                                label: 'Hora de inicio (09:00)',
                                value: 'startTime',
                                type: 'Time',
                                isShow: true,
                                required: true
                            },
                            {
                                label: 'Hora de fin (09:00)',
                                value: 'endTime',
                                type: 'Time',
                                isShow: true,
                                required: true
                            },
                            {
                                label: 'Color Label',
                                value: 'labelColor',
                                default: '#FFA500',
                                type: 'Text',
                                isShow: true,
                                required: true
                            },
                            {
                                label: 'Locación',
                                value: 'locationId',
                                type: 'Table',
                                uri: 'location/list-all',
                                isShow: true,
                                required: true
                            },
                            {
                                label: 'typeCode',
                                value: 'typeCode',
                                default: 'TIEV002',
                                type: 'Hidden',
                                isShow: true,
                                required: true
                            },
                            {
                                label: 'categoryCode',
                                value: 'categoryCode',
                                default: 'TICT001',
                                type: 'Hidden',
                                isShow: true,
                                required: true
                            },
                        ]
                    } else {
                        attributesTemp = this.attributes
                    }
                    attributesTemp.push(
                        {
                            label: 'classCode',
                            value: 'classCode',
                            default: sClassCode.val(),
                            type: 'Hidden',
                        })
                    console.log(attributesTemp)
                    const formGenerator = new FormGenerator('event', attributesTemp, container + "-case", this.idAddButton, this.app)
                    formGenerator.initForm()
                })
            },
            error: (xhr, status) => {
                console.log('Error: ' + status);
            }
        });
        //mapear datos en front
        //crear evento change jquery de enum
    }

    createContentPopup = (item) => {
        console.log("item value desde method create :: ", item)
        const cContentPopup = $(`#${this.idContentPopup}`)
        const user = JSON.parse(localStorage.getItem('user'))
        cContentPopup.empty();
        const dataTypeEventGroup = `
                <div class="col-50 text-align-center margin-bottom">
                    <p class="small ">
                        <p class="fw-medium">Mínimo de participantes:</p>
                        <span class="text-muted">${item.minimum}</span>
                    </p>
                    </div>
                <div class="col-50 text-align-center margin-bottom">
                    <p class="small ">
                        <p class="fw-medium">Máximo de participantes:</p>
                        <span class="text-muted">${item.maximum}</span>
                    </p>
                </div>
                <div class="col-50 text-align-center margin-bottom">
                    <p class="small ">
                        <p class="fw-medium">Cupos disponibles:</p>
                        <span class="text-muted">${item.available_seats}</span>
                    </p>
                </div>
        `;
        const dataEventPayment = `
            ${(item.is_group ?? false) === true ? dataTypeEventGroup : ''}
            <div class="col-${(item.is_group ?? false) === true ? '50' : '100'} text-align-center margin-bottom">    
                <p class="small ">
                    <p class="fw-medium">Tipo de evento:</p>
                    <span class="text-muted">${(item.is_group ?? false) === true ? 'Grupal' : 'Individual'}</span>
                </p> 
            </div>
            `;
        const dataEjeTematico = `
            <div class="col-100 text-align-center margin-bottom">    
                <p class="small ">
                    <p class="fw-medium">Eje temático:</p>
                    <span class="text-muted">${item.theme_description}</span>
                </p> 
            </div>
        `;
        const speakerContent = `
                        <p class="small">
                            <p class="fw-medium">Ponente:</p>
                            <span class="text-muted">${item?.speaker?.name}</span>
                        </p>
            `;
        let html = `
            <div>
            <div>
                <h3 class="text-align-center">${item.name}</h3>
            </div>            
            <div class="content-popup-academic ${user.rol === 'ADMIN' ? 'fixed-footer-event-160' : 'fixed-footer-event'}">
                <p class="small ">
                    <p class="fw-medium">Descripción:</p>
                    <span class="text-muted">${item.description}</span>
                </p>
                <p class="small " style="display: ${this.hasRegistration ? 'block' : 'none'};">
                    <p class="fw-medium" style="display: ${this.hasRegistration ? 'block' : 'none'};">Bases:</p>
                    <a href="${item.url_base}" class="external" style="display: ${this.hasRegistration ? 'block' : 'none'};">Clic para ver las bases</a>
                </p> 
                ${item?.speaker?.name ? speakerContent : ''}
                <p class="small ">
                    <p class="fw-medium">Lugar:</p>
                    <span class="text-muted">${item.location.name}</span>
                    <div id="${this.idContentMap}" style="width: 100%; height: 400px;"></div>
                </p>
                <div class="row margin-bottom">
                    <div class="col-50 text-align-center margin-bottom">
                         <p>
                            ${this.getEventStatus(item.date, item.end_date, item.start_time, item.end_time) === 'Evento finalizado' ?
            '<span class="badge color-red">' + this.getEventStatus(item.date, item.end_date, item.start_time, item.end_time) + '</span>'
            : '<span class="badge color-green">' + this.getEventStatus(item.date, item.end_date, item.start_time, item.end_time) + '</span>'}
                         </p>
                    </div>
                    <div class="col-50 text-align-center margin-bottom">
                         <p>
                            <span class="badge color-deeporange">Asistentes: ${item.number_attendees}</span>
                         </p>
                    </div>
                    <div class="col-50 text-align-center margin-bottom">
                        <p class="small ">
                            <p class="fw-medium">Fecha Inicio:</p>
                            <span class="text-muted">${item.date}</span>
                        </p>
                    </div>
                    <div class="col-50 text-align-center margin-bottom">
                        <p class="small ">
                            <p class="fw-medium">Fecha Fin:</p>
                            <span class="text-muted">${item.end_date}</span>
                        </p>
                    </div>
                    <div class="col-50 text-align-center margin-bottom">
                        <p class="small ">
                            <p class="fw-medium">Hora Inicio:</p>
                            <span class="text-muted">${item.start_time}</span>
                        </p>
                    </div> 
                    <div class="col-50 text-align-center margin-bottom">
                         <p class="small ">
                            <p class="fw-medium">Hora Fin:</p>
                            <span class="text-muted">${item.end_time}</span>
                        </p>
                    </div> 
                    ${this.hasRegistration ? dataEventPayment : ''}           
                    ${this.popupClass === 'academic-popupClassAcademic' ? dataEjeTematico : ''}           
                </div>
            </div>
        `
        const htmlButtonAssistance = ApiUtils.isAdmin() || ApiUtils.isScanner()
            ? `<a id="${this.linkAssistance}" class="button button-fill button-large button-raised color-theme">Asistentes</a>` : ''
        const htmlButtonEdit = ApiUtils.isAdmin()
            ? `<a id="${this.linkAssistance}-edit" class="button button-large button-raised button-fill color-theme">Editar</a>` : ''
        const htmlButtonDelete = ApiUtils.isAdmin()
            ? `<a id="${this.linkAssistance}-remove" class="button button-large button-raised button-fill color-theme-red">Eliminar</a>` : ''

        if (ApiUtils.rolesAdmins.includes(user.rol)) {
            if (this.getEventStatus(item.date, item.end_date, item.start_time, item.end_time) !== 'Evento finalizado') {
                html += `
            <div class="event-detail-footer">
                <!--<button class="button button-fill button-raised color-theme" id="${this.linkAssistance}">Asistentes</button>-->
                 <div class="row justify-content-center">
                    <div class="col-90 medium-50 large-40 text-align-center margin-top-auto">
                    ${htmlButtonAssistance}
                </div> 
            </div> 
                <!--<button class="button button-fill button-raised color-theme mt-2" id="${this.linkAssistance}-edit">Editar</button>-->
                <div class="col-90 margin-top-auto margin-left-auto margin-right-auto padding-vertical">
                    <div class="row ">
                      <div class="col-50 text-align-right">
                        ${htmlButtonEdit}
                      </div>
                      <div class="col-50">
                        ${htmlButtonDelete}
                      </div>
                    </div>
                </div>
              
            </div>`;
            } else {
                html += `
            <div class="row justify-content-center event-detail-footer">
                <!--<button class="button button-fill button-raised color-theme" id="${this.linkAssistance}-summary">Resumen de evento</button>-->
                <div class="col-90 medium-50 large-40 text-align-center margin-top-auto padding-vertical">
                    <a id="${this.linkAssistance}-summary" class="button button-fill button-large button-raised color-theme">Resumen de evento</a>
                </div> 
            </div>`;
            }
        } else if (this.hasRegistration) {
            if (user.datos.edecan && this.getEventStatus(item.date, item.end_date, item.start_time, item.end_time) !== 'Evento finalizado') {
                html += `
                            <!--<div class="row justify-content-center event-detail-footer">
                                    <button class="button button-fill button-raised color-theme" id="${this.linkRegistration}">
                                        Inscribirse
                                    </button>
                            </div> -->
                    <div class="row justify-content-center event-detail-footer">
                        <div class="col-90 medium-50 large-40 text-align-center margin-top-auto padding-vertical">
                            <a id="${this.linkRegistration}" class="button button-fill button-large button-raised color-theme">Inscribirse</a>
                        </div> 
                    </div>
                    `;
            } else if (this.getEventStatus(item.date, item.end_date, item.start_time, item.end_time) !== 'Evento finalizado') {
                html += `
            <div class="event-detail-footer">
                <!--<button class="button button-fill button-raised color-theme" id="${this.linkAssistance}-summary">Resumen de evento</button>-->
                <div class="col-90 medium-50 large-40 text-align-center margin-top-auto padding-vertical">
                <a id="${this.linkAssistance}-summary" class="button button-fill button-large button-raised color-theme">Resumen de evento</a>
                </div> 
            </div>`;
            }
        }
        html += `</div>`;
        cContentPopup.append(html);
        this.initMap(parseFloat(item.location.latitude), parseFloat(item.location.longitude));

        $(`#${this.linkAssistance}`).click(() => {
            console.log('ir a evento')
            this.loadSpinner();
            this.app.popup.close(`.${this.popupClass}`);
            const router = this.app.views.main.router;
            router.navigate(`/event/assistance/${item.id}/${this.hasRegistration ? 'payment' : 'free'}/`);
        })
        $(`#${this.linkAssistance}-edit`).click(() => {
            console.log('ir a editar :: ', item.id)
            this.app.popup.close(`.${this.popupClass}`);
            this.openFormulario(item.id)
        })
        $(`#${this.linkAssistance}-remove`).click(() => {
            console.log('eliminar :: ', item.id)
            this.app.dialog.confirm('¿Está seguro de eliminar este evento?', 'Mensaje', () => {
                this.deleteEvent(item.id);
            });
        })
        $(`#${this.linkAssistance}-summary`).click(() => {
            console.log('ir a resumen de evento')
            this.app.popup.close(`.${this.popupClass}`);
            const router = this.app.views.main.router;
            router.navigate(`/event/summary/${item.id}/${this.hasRegistration ? 'payment' : 'free'}/`);
        })

        if (this.hasRegistration) {
            $(`#${this.linkRegistration}`).click(() => {
                console.log('ir a registrarse')
                this.loadSpinner();
                this.app.popup.close(`.${this.popupClass}`);
                const router = this.app.views.main.router;
                router.navigate(`/event/registration/${item.id}/`);
            })
        }
    }

    deleteEvent = (eventId) => {
        console.log('evento a eliminar::', eventId)
        console.log('ruta falta definir para recargar')
        this.app.request({
            url: `${getRouteApi()}/congress/v1/api/event/${eventId}`,
            method: 'DELETE',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                console.log('response de eliminar evento ::', JSON.parse(data))
                this.app.dialog.alert(JSON.parse(data).message, 'Éxito');
                this.app.popup.close(`.${this.popupClass}`)
                this.getList()
            },
            error: (xhr, status) => {
                console.log('Error: ' + status);
                this.catchLogicEvent(xhr, status)
            }
        });
    }

    getDataEvent = (eventId) => {
        console.log("entrando a request")
        this.app.request({
            url: `${getRouteApi()}/congress/v1/api/event/${eventId}`,
            method: 'GET',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                const dataL = JSON.parse(data)
                console.log("data req: ", dataL)
                this.dataEvent = dataL.data
                this.createContentPopup(this.dataEvent)
                this.closeSpinner();
            },
            error: (xhr, status) => {
                console.log('Error: ' + status);
                this.catchLogicEvent(xhr, status);
                this.closeSpinner();
            }
        });
    }

    catchLogicEvent(xhr, status) {
        if (status >= 400 && status <= 499) {
            const response = JSON.parse(xhr.responseText).errors;
            if (response.length > 0) {
                this.app.dialog.alert(response[0], 'Error');
            } else if (typeof response === 'object') {
                console.log('La respuesta es un objeto');
                console.log(response)
                const firstErrorValue = Object.values(response)[0];
                console.log(firstErrorValue);
                this.app.dialog.alert(firstErrorValue, 'Error');
            }
        }
    }

    isDateInFuture(date, start_time) {
        // Concatenate date and start_time to create a complete datetime string
        const datetimeStr = date + ' ' + start_time;

        // Create Date objects for the provided datetime and current datetime
        const providedDatetime = new Date(datetimeStr);
        const currentDatetime = new Date();

        // If providedDatetime is not a valid Date object, throw an error
        if (isNaN(providedDatetime)) {
            throw new Error('Invalid datetime provided');
        }

        // Compare the date objects and return the result
        return providedDatetime > currentDatetime;
    }

    getEventStatus(date, end_date, start_time, end_time) {

        let current = new Date();

        let endTime = new Date(`${end_date}T${end_time}`);
        let startTime = new Date(`${date}T${start_time}`);
        // console.log('eval -> ', startTime, current, endTime)
        if (startTime <= current && current <= endTime) {
            return 'Evento iniciado';
        } else {
            if (current >= endTime) {
                return 'Evento finalizado';
            } else {
                return 'Evento Programado'
            }
        }
    }

    registrationSubmit(eventId) {
        this.app.request({
            url: `${getRouteApi()}/congress/v1/api/inscription/enroll`,
            method: 'POST',
            data: {
                eventId,
                name: '-',
                description: '-',
                participants: []
            },
            contentType: 'application/json',
            success: (data, status, xhr) => {
                const dataL = JSON.parse(data)
                console.log(dataL)
                this.app.dialog.alert(JSON.parse(data).message, 'Éxito');
            },
            error: (xhr, status) => {
                this.app.dialog.alert(JSON.parse(xhr.responseText).errors[0], 'Error');
                document.getElementsByClassName('dialog-title')[0].style.color = 'red';
                console.log('Error: ' + status);
            }
        });
    }

    createModal = (title) => {
        const html = `
        <div class="popup ${this.popupClass}">
          <div class="page">
            <div class="navbar">
              <div class="navbar-bg"></div>
              <div class="navbar-inner">
                <div class="title" id="popup-title">${title}</div>
                <div class="right"><a href="#" class="link" id="popup-close">Cerrar</a></div>
              </div>
            </div>
            <div class="page-content">              
                <div id="${this.idContentPopup}"></div>
            </div>
          </div>
        </div>
        `
        $(`#${this.idComponent}`).append(html);

        $(`#popup-close`).click(() => {
            console.log('cerrar')
            this.app.popup.close(`.${this.popupClass}`)
        })
    }

    createComponents = () => {
        console.log('create components')
        this.createList()
        this.createModal('DETALLE DE EVENTO')
    }

    initMap = (lat, lng) => {
        console.log('initMap')
        const location = {lat, lng};
        const cMap = $(`#${this.idContentMap}`)
        let map = new google.maps.Map(cMap[cMap.length - 1], {
            zoom: 15,
            center: location
        });
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            title: 'Click to zoom'
        });

        map.addListener('center_changed', function () {
            // Después de 3 segundos se moverá de nuevo  al marcador
            window.setTimeout(function () {
                map.panTo(marker.getPosition());
            }, 3000);
        });

        marker.addListener('click', function () {
            window.location.href = 'https://www.google.com/maps/search/?api=1&query=' + location.lat + ',' + location.lng;
        });
    }

    getList = () => {
        this.createLoadingList()
        this.app.request({
            url: `${getRouteApi()}/congress/v1/api/event/list/${this.code}`,
            method: 'GET',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                const dataL = JSON.parse(data)
                console.log(dataL)
                this.dataList = dataL.data
                this.createComponents()
            },
            error: function (xhr, status) {
                console.log('Error: ' + status);
            }
        });
    }

    init = () => {
        this.getList()
    }

    createLoadingList() {
        const sIdComponent = `#${this.idComponent}`
        $(sIdComponent).empty()
        $(sIdComponent).append(
            `<div style="margin-bottom: 10px" class="">
                <form data-search-container=".search-list" data-search-in=".item-title" class="searchbar searchbar-init">
                  <div class="searchbar-inner">
                    <div class="searchbar-input-wrap">
                      <input type="search" placeholder="Buscar"/>
                      <i class="searchbar-icon"></i>
                      <span class="input-clear-button"></span>
                    </div>
                    <span class="searchbar-disable-button if-not-aurora">Cancel</span>
                  </div>
                </form>
            </div>`)
        const dataFake = [
            {
                img: undefined,
                name: '--------------------------------',
                description: '--------'
            },
            {
                img: undefined,
                name: '--------------------------------',
                description: '--------'
            },
            {
                img: undefined,
                name: '--------------------------------',
                description: '--------'
            },
            {
                img: undefined,
                name: '--------------------------------',
                description: '--------'
            },
            {
                img: undefined,
                name: '--------------------------------',
                description: '--------'
            },
            {
                img: undefined,
                name: '--------------------------------',
                description: '--------'
            },
            {
                img: undefined,
                name: '--------------------------------',
                description: '--------'
            }
        ]
        dataFake.forEach((item, index) => {
            // Create new card element
            let cardElement =
                `<div class="card margin-bottom-half skeleton-text skeleton-effect-wave">
                    <div class="card-content card-content-padding">
                        <div class="row">
                            <div class="col-auto">
                                <div class="card">
                                    <div class="card-content padding-half-sm">
                                        <div class="skeleton-block" style="width: 40px; height: 40px; border-radius: 50%"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col align-self-center no-padding-left">
                                 <p class="small margin-bottom-half"><div class="fw-medium mr-4">${item.name}</div>
                                </p>
                                <p>${item.name}
                                    
                                </p>
                                <p>${item.description}</p>
                            </div>
                            <div class="col-auto">
                            </div>
                        </div>
                    </div>
                </div>`;
            $(`#${this.idComponent}`).append(cardElement);
        });
    }
}