import EventGenerator from "./EventGenerator.js";
import getRouteApi from "../api.js";

const $ = Dom7;
export default function eCalendar(app) {
    let navbarId = null
    let navbarDate = null
    const loadSpinner = () => {
        console.log("inicio spinner formGenerator")
        app.dialog.preloader('cargando...');
    }

    const closeSpinner = () => {
        console.log("fin spinner formGenerator")
        app.dialog.close();
    }
    const daysRender = (days, flag) => {
        console.log("ingreso days render:: ", days)
        let fechaLunes = days[0];
        let lunes = days[0].fecha;
        console.log("lunes:: ", fechaLunes)
        console.log("dayLunes:: ", lunes)
        if (!flag) {
            navbarDate = lunes
        }
        const sCalendarNavbarDays = $('#calendar-navbar-days')
        sCalendarNavbarDays.empty()
        let html = '<div class="segmented segmented-strong">'
        days.forEach((day, index) => {
            if (day.fecha === navbarDate) {
                html += `<button class="button button-active" id="navbar-day-${index}">${day.label.charAt(0)}</button>`
                if(flag) {
                    console.log("day fijo :: ", day)
                    getCalendarDetail(navbarDate, day)
                }
            } else {
                html += `<button class="button c-white" id="navbar-day-${index}">${day.label.charAt(0)}</button>`
            }

        })
        html += '<span class="segmented-highlight"></span></div>'

        sCalendarNavbarDays.append(html)
        if(!flag && fechaLunes) {
            getCalendarDetail(lunes, fechaLunes)
        }

        days.forEach((day, index) => {
            $(`#navbar-day-${index}`).on('click', () => {
                console.log('click ', navbarDate)
                navbarDate = day.fecha
                daysRender(days, true)
            })
        })
    }
    const getCalendarDetail = (navbarDate, day) => {
        console.log("ingresó a getCalendarDetail:: ", navbarDate, " day::", day)
        loadSpinner()
        app.request({
            url: `${getRouteApi()}/congress/v1/api/event/calendar-detail?date=${navbarDate}`,
            method: 'GET',
            contentType: 'application/json',
            success: function (data, status, xhr) {
                const eventsDay = JSON.parse(data).data
                console.log(eventsDay)
                generate(eventsDay, day)
                closeSpinner()
            },
            error: function (xhr, status) {
                console.log('Error: ' + status);
                console.log(JSON.parse(xhr.responseText).errors)
                // const response = JSON.parse(xhr.responseText).errors;
                // app.dialog.alert(response[0], 'Error');
                closeSpinner()
            }
        });
    }
    const generate = (events, day) => {
        const dayTitle = day.label + ': ' + day.fecha
        console.log("generate:: ", dayTitle)
        console.log("day:: ",day)
        const generator = new EventGenerator(events, 'calendar', dayTitle, app);
        generator.generar();
    }

    let currentIndex = 0;

    const navbarRender = (navbarList) => {
        const sCalendarNavbar = $('#calendar-navbar');
        const currentNavbar = navbarList[currentIndex];

        console.log("currentNavbar :: ", currentNavbar)
        navbarId = currentNavbar.label;

        sCalendarNavbar.empty();

        let html = '<button class="button button-fill w-10-calendar" id="prev-button">&lt;</button>';
        html += `<h5 class="text-align-center color-black-title" id="navbar-current">${currentNavbar.label}</h5>`;
        html += '<button class="button button-fill w-10-calendar" id="next-button">&gt;</button>';

        sCalendarNavbar.append(html);

        $('#prev-button').on('click', () => {
            if (currentIndex > 0) {
                currentIndex--;
                navbarRender(navbarList);
            }
        });

        $('#next-button').on('click', () => {
            if (currentIndex < navbarList.length - 1) {
                currentIndex++;
                navbarRender(navbarList);
            }
        });

        console.log("dias del navbar actual:: ", currentNavbar.dias)
        daysRender(currentNavbar.dias, false);
    }


    const getCalendarHead = () => {
        // loadSpinner()
        app.request({
            url: `${getRouteApi()}/congress/v1/api/event/calendar-head`,
            method: 'GET',
            contentType: 'application/json',
            success: function (data, status, xhr) {
                const events = JSON.parse(data).data
                console.log("events: ",events)
                navbarRender(events)
                closeSpinner()
            },
            error: function (xhr, status) {
                console.log('Error: ' + status);
                console.log(JSON.parse(xhr.responseText).errors)
                const response = JSON.parse(xhr.responseText).errors;
                app.dialog.alert(response[0], 'Error');
                closeSpinner()
            }
        });
    }
    const navbarAction = () => {
        loadSpinner()
        getCalendarHead()
    }
    const init = () => {
        console.log('eCalendar init')
        navbarAction()


    }
    return {
        init
    }
}

