import FormGenerator from "./formGenerator.js";

const $ = Dom7;

export default function profile(app) {
    let entity = 'password-recovery-change-password'
    const user = JSON.parse(localStorage.getItem('user'))
    let stage = user?.datos?.stage ? `img/app/stages/${user.datos.stage}.png` : 'img/user1.jpg';
    let attributes = [
        {
            label: 'Nueva contraseña',
            value: 'newPassword',
            type: 'Text',
            min: 5,
            max: 8,
            isShow: true,
            required: true
        },
        {
            label: 'Correo Electrónica',
            value: 'email',
            type: 'Hidden',
            default: user.email,
            isShow: true,
            required: true
        }
    ]
    const formGenerator = new FormGenerator(entity, attributes, 'content-profile', null, app)
    formGenerator.setUriCustomSubmit("mail/change-password")
    const changePassword = () => {
        formGenerator.initForm()
    }
    const initProfile = () => {
        const profileData = $("#profile-data")
        profileData.empty()
        const user = JSON.parse(localStorage.getItem('user'))
        const html = `
            <div class="row">
            <div class="col-100 profile-page">
                <div class="clearfix"></div>
                <div class="circle small one"></div>
                <div class="circle small two"></div>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="menubg"
                    width="282.062" height="209.359" viewBox="0 0 282.062 209.359">
                    <defs>
                        <linearGradient id="linear-gradient11" x1="0.5" x2="0.5" y2="1"
                            gradientUnits="objectBoundingBox">
                            <stop offset="0" stop-color="#09b2fd" />
                            <stop offset="1" stop-color="#6b00e5" />
                        </linearGradient>
                    </defs>
                    <path id="profilebg"
                        d="M751.177,233.459c-28.511,1.567-38.838,7.246-61.77,27.573s-27.623,71.926-65.15,70.883-27.624-21.369-79.744-40.132-47.13-53.005-23.676-84.8,4.009-57.671,33-75.867,83.269,30.223,127.232,21.5,64.157-41.353,82.329-26,5.953,29.138,8.773,46.369,13.786,23.5,13.786,37.91S779.688,231.893,751.177,233.459Z"
                        transform="translate(-503.892 -122.573)" fill="url(#linear-gradient11)" />
                </svg>

                <div class="row margin-vertical padding-vertical">
                    <div class="col-60 align-self-center">
                        <h1 class="margin-bottom-half"><span class="fw-light text-secondary">Hola!</span><br />${user.user}
                        </h1>
                        <p class="text-secondary size-12 text-uppercase">${user.rol === 'ADMIN' ? user.rol : user.datos.promotion}</p>
                    </div>

                    <div class="col align-self-center">
                        <figure class="avatar avatar-100 rounded-20 padding-half-sm bg-color-white elevation-2">
                            <img src="${stage}" alt="" class="rounded-18" id="img_stage_profile">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
        `
        profileData.append(html)
    }

    return {
        initProfile,
        changePassword,
    }
}