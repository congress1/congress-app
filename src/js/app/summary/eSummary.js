import getRouteApi from "../api.js";
import ListGenerator from "../listGenerator.js";

const $ = Dom7;

export default function eSummary(app) {


    const assistances_list = (event) => {
        let attributes = []

        attributes = [
            {
                label: 'Participante',
                value: 'name',
                type: 'Text',
                isShow: true,
                required: true
            },
            {
                label: 'Universidad',
                value: 'university',
                type: 'Text',
                isShow: true,
                required: true
            },
        ]

        const randomNumber = Math.floor(Math.random() * 1000000)
        const eventId = app.views.main.router.currentRoute.params.eventId
        $('#assistance-container-summary').empty()
        console.log('eventId', eventId)
        const gList = new ListGenerator('event/list-participants-event/' + eventId, attributes, 'assistance-container-summary', '', app, randomNumber, null, false, false)
        gList.noneMargin()
        gList.initList()
    }
    const eventResult = (eventId, type) => {
        const sAssistanceTitle = $('#event-result-table')
        sAssistanceTitle.empty()
        if (type === 'payment') {
            app.request({
                url: `${getRouteApi()}/congress/v1/api/event-result/${eventId}`,
                method: 'GET',
                contentType: 'application/json',
                // headers: {
                //     'Authorization': 'Bearer ' + localStorage.getItem('token') // reemplaza token con tu token
                // },
                success: (data, status, xhr) => {
                    const row = JSON.parse(data).data
                    console.log(row)
                    let html = '<hr>' +
                        '<h4>Tabla de posiciones</h4>' +
                        '<div>'
                    row.forEach((file, index) => {
                        html += `
                    <div id="sm-item-${index}" class="card margin-bottom-half file">
                        <div class="card-content card-content-padding">
                            <div class="row">
                                <div class="col-auto">
                                    <div class="card">
                                        <div class="card-content padding-half-sm">
                                            <div class="avatar avatar-44 elevation-2 rounded-15">
                                                ${index + 1}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col align-self-center no-padding-left">
                                     <p class="small margin-bottom-half"></p><div class="fw-medium mr-4 title-event">${file.insc_name}</div>
                                     <p>${file.univ_name}</p>
                                </div>
                                <div class="col-auto" style="align-self: center">
                                    <span class="badge color-green">${file.score ?? 0} Ptos</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    `
                    })
                    html += '</div>'
                    const sAssistanceTitle = $('#event-result-table')
                    sAssistanceTitle.append(html)
                    const user = JSON.parse(localStorage.getItem('user'))
                    if (user.rol === 'ADMIN') {
                        row.forEach((file, index) => {
                            $(`#sm-item-${index}`).on('click', () => {
                                formPoint(file, eventId)
                            })
                        })
                    }
                },
                error: function (xhr, status) {
                    console.log('Error: ' + status);
                }
            });
        } else {
            const sAssistanceTitle = $('#event-result-table')
            sAssistanceTitle.append(`
            <div id="event-result-table-contain"></div>`)
        }

    }
    const formPoint = (file, eventId) => {
        const type = app.views.main.router.currentRoute.params.type
        app.dialog.prompt('Ingrese puntaje', 'Mensaje', (points) => {
            ///congress/v1/api/event-result/2' \
            app.request({
                url: `${getRouteApi()}/congress/v1/api/event-result/${file.id}`,
                method: 'PUT',
                contentType: 'application/json',
                data: {
                    "score": points
                },
                // headers: {
                //     'Authorization': 'Bearer ' + localStorage.getItem('token') // reemplaza token con tu token
                // },
                success: (data, status, xhr) => {
                    console.log(data)
                    app.dialog.alert('Puntos ingresados correctamente', 'Coneic');
                    eventResult(eventId, type)
                },
                error: function (xhr, status) {
                    console.log('Error: ' + status);
                }
            });

        });
    }
    const init = () => {
        console.log('init sumamry')
        const eventId = app.views.main.router.currentRoute.params.eventId
        const type = app.views.main.router.currentRoute.params.type
        app.request({
            url: `${getRouteApi()}/congress/v1/api/event/${eventId}`,
            method: 'GET',
            contentType: 'application/json',
            // headers: {
            //     'Authorization': 'Bearer ' + localStorage.getItem('token') // reemplaza token con tu token
            // },
            success: (data, status, xhr) => {
                const row = JSON.parse(data).data
                const htmlLinkAssistance = '<div class="footer-fixed"><div style="width: 150px"><button id="button-open-assistance-summary" class="button button-white" style="z-index: 100;">Asistentes</button></div></div>'
                const html = `
                <div class="col-12">
                    <h3>${row.name}</h3>
                    <p>${row.description}</p>
                    <p>Fecha de evento: ${row.date} ${row.start_time} - ${row.end_time}</p>
                    ${htmlLinkAssistance}
                </div>
                <div class="col-12" id="event-result-table"></div>
                `
                const sAssistanceTitle = $('#summary-title')
                sAssistanceTitle.empty()
                sAssistanceTitle.append(html)
                $('#button-open-assistance-summary').on('click', (e) => {
                    console.log('button-open-assistance-summary');
                    // app.views.main.router.navigate(`/event/assistance/qr/${eventId}/`);
                    assistances_list(row)
                    app.popup.open(`.scanner-assistance-summary`);

                })
                eventResult(eventId, type)
            },
            error: function (xhr, status) {
                console.log('Error: ' + status);
            }
        });
    }

    return {
        init
    }
}