import getRouteApi from "../api.js";

const $ = Dom7;
export default function hSpeaker(app) {
    const init = () => {

        const loadSpinner = () => {
            console.log("inicio spinner home/Speaker")
            app.dialog.preloader('cargando...');
        }

        const closeSpinner = () => {
            console.log("fin spinner home/Speaker")
            app.dialog.close();
        }
        loadSpinner()
        const speakerId = app.views.main.router.currentRoute.params.speakerId
        console.log('init h speaker', speakerId)


        app.request({
            url: `${getRouteApi()}/congress/v1/api/speaker/${speakerId}`,
            method: 'GET',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                console.log(JSON.parse(data))
                const speaker = JSON.parse(data).data
                const color = "#F5F5F5"
                //
                $('#speaker-view').append(`
                <div class="row">
                    <div class="col-100 col-sm-6 medium-33 large-25">
                        <div class="card margin-bottom elevation-2 padding-half" style="border-radius: 10%;border: 4px solid ${color}; ">
                            <div class="card-content card-content-padding text-align-center elevation-2 rounded-15">
                                <div class="row align-items-center ">
                                    <div class="col-auto margin-left-auto margin-right-auto">
                                        <div class="card margin-bottom">
                                            <div class="card-content card-content-padding padding-half-sm">
                                                <figure class="avatar avatar-300 margin-left-auto margin-right-auto rounded-15">
                                                    <img src="${speaker.url_image}" alt=""/>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h6 class="card-title no-margin-bottom c-white text-align-center">${speaker.name}</h6>
                                <p class="text-small text-secondary small" style="color: ${color}!important;">Nacionalidad: ${speaker.nationality}</p>
                                <p>${speaker.description}</p>
                            </div>
                            <div class="card-footer justify-content-center text-align-center border-0 text-align-center">
                                <a href="${speaker.url_social_network}" class="c-white mx-2 external"><i class="bi bi-facebook"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                `)

                closeSpinner()
            },
            error: (xhr, status) => {
                console.log('Error: ' + status);
            }
        });
    }
    const fetchList = (select) => {
        app.request({
            url: `${getRouteApi()}/congress/v1/api/speaker/list`,
            method: 'GET',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                console.log(JSON.parse(data))
                const list = JSON.parse(data).data
                console.log(list)
                select.empty()
                renderList(select, "", list, "F5F5F5")
            },
            error: (xhr, status) => {
                console.log('Error: ' + status);
            }
        });
    }
    const listSpeaker = () => {
        console.log('INIT LIST SPEAKER')
        const select = $('#home-speakers')
        select.empty()
        renderListSkeleton(select, "")
        fetchList(select)
    }

    const renderList = (select, title, list, color) => {
        let html = `<div class="row"><div class="col"><h3>${title}</h3></div></div><div class="row">`

        list.forEach((element) => {
            html += `            
                <div class="col-50 col-sm-4 medium-25">
                    <div class="card margin-bottom elevation-2">
                        <div class="card-content card-content-padding text-align-center" style=" border-radius: 10%;border: 4px solid #${color}; ">
                            <div class="row align-items-center ">
                                <a href="/home-speaker/${element.id}/" class="col-auto margin-left-auto margin-right-auto">
                                    <div class="card margin-bottom">
                                        <div class="card-content card-content-padding padding-half-sm">
                                            <figure class="avatar avatar-80 margin-left-auto margin-right-auto rounded-15">
                                                <img src="${element.url_image}" alt=""/>
                                            </figure>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <p class="no-margin-bottom c-white text-align-center">${element.name}</p>
<!--                            <p class="text-small c-white small">Chief Designer</p>-->
                        </div>
                    </div>
                </div>
            `;
        })

        html += '</div>'
        select.append(html)
    }
    const renderListSkeleton = (select, title) => {
        let html = `<div class="row"><div class="col"><h3>${title}</h3></div></div><div class="row">`

        // Generar los items del esqueleto
        for (let i = 0; i < 4; i++) {
            html += `            
                <div class="col-50 col-sm-4 medium-25 skeleton-effect-wave">
                    <div class="card margin-bottom elevation-2 skeleton-text">
                        <div class="card-content card-content-padding text-align-center">
                            <div class="row align-items-center ">
                                <a href="/profile/" class="col-auto margin-left-auto margin-right-auto">
                                    <div class="card margin-bottom">
                                        <div class="card-content card-content-padding padding-half-sm">
                                            <figure class="avatar avatar-80 margin-left-auto margin-right-auto rounded-15">
                                                <div class="skeleton-block" style="width: 80px; height: 80px; border-radius: 10%"></div>
                                            </figure>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <p class="no-margin-bottom c-white text-align-center">John McMohan</p>
                            <p class="text-small c-white small">Chief Designer</p>
                        </div>
                    </div>
                </div>
            `;
        }

        html += '</div>'
        select.append(html)
    }
    return {
        init,
        listSpeaker
    }
}