import getRouteApi from "../api";
import {ApiUtils} from "../../utils/apiUtils";

const $ = Dom7;
export default function hQuestion(app) {
    const fetch = (select) => {
        app.request({
            url: `${getRouteApi()}/congress/v1/api/question/list`,
            method: 'GET',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                console.log(JSON.parse(data))
                const list = JSON.parse(data).data
                select.empty()
                renderList(select, list)
                ApiUtils.closeSpinner(app)
            },
            error: (xhr, status) => {
                console.log('Error: ' + status);
                ApiUtils.catchLogicListGenerator(xhr, status, app)
                ApiUtils.closeSpinner(app)
            }
        });
    }
    const renderList = (select, list) => {
        list.forEach((item, index) => {
            console.log(item)
            select.append(`
                <li class="accordion-item ">
                    <a class="item-content item-link" href="#">
                        <div class="item-inner">
                            <div class="item-title">${item.question}</div>
                        </div>
                    </a>
                    <div class="accordion-item-content padding-horizontal">
                        <div class="row">
                            <div class="col-100">
                                <p class="text-muted">${item.answer}</p>
                            </div>
                        </div>
                    </div>
                </li>
        `)
        })
    }
    const renderListSkeleton = (select) => {
        let html = ''
        for (let i = 0; i < 4; i++) {
            html += `
            <li class="accordion-item accordion-item skeleton-effect-wave skeleton-text ${i === 0 ? 'accordion-item-opened' : ''}">
                    <a class="item-content item-link" href="#">
                        <div class="item-inner">
                            <div class="item-title">How finwallapp is flexible template</div>
                        </div>
                    </a>
                    <div class="accordion-item-content padding-horizontal">
                        <div class="row">
                            <div class="col-100">
                                <p class="text-muted">finwallapp provides wide rang of different
                                    componentsor
                                    module based UI interface which are syc or consistance with the whole
                                    template. Thats the man reason
                                    finwallapp is flexible to create pages and development.</p>
                            </div>
                        </div>
                    </div>
                </li>
            `
        }
        select.append(html)
    }

    const contractionItem = (select) => {
        select.on('accordion:opened', function (event) {
            const $accordionItem = $(event.target).closest('.accordion-item');
            const $itemTitle = $accordionItem.find('.item-title');
            $itemTitle.css({
                'white-space': 'normal',
                'overflow': 'visible',
                'text-overflow': 'clip'
            });
        });

        select.on('accordion:closed', function (event) {
            const $accordionItem = $(event.target).closest('.accordion-item');
            const $itemTitle = $accordionItem.find('.item-title');
            $itemTitle.css({
                'white-space': 'nowrap',
                'overflow': 'hidden',
                'text-overflow': 'ellipsis'
            });
        });
    }

    const init = () => {
        ApiUtils.loadSpinner(app)
        const select = $('#home-questions')
        let html = `
            <div class="row margin-bottom-half">
            <div class="col">
                <h6 class="title">Encuentra la respuesta correcta</h6>
            </div>
        </div>
        <!-- expand collapse -->
        <div class="row">
            <div class="col-100">
                <div class="card margin-bottom">
                    <div class="list accordion-list">
                        <ul id="list-question">        
        `

        html += `
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        `
        select.append(html)
        const selectListQuestion = $('#list-question')
        renderListSkeleton(selectListQuestion)
        fetch(selectListQuestion)
        contractionItem(selectListQuestion)

    }
    return {
        init
    }
}