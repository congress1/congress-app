import getRouteApi from "../api.js";


const $ = Dom7;
export default function home(app) {
    const render = () => {
        $("#navbar").addClass("navbar-background")
        const user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null
        if (user && user.datos) {
            $('#title-home').text('¡BEKANWE, ' + user.user.split(' ')[0] + "!")

            $('#home-content').append(`
               <div class="row margin-vertical text-align-center">
                <div class="col-100">
                  <div class="navbar-logo">
                    <img src="img/app/logo.png" alt=""/>
                  </div>
                </div>
              </div>
                  <!--<div id="benefit-content" class="row margin-vertical text-align-center"></div>-->
                  <div id="social-networks" class="social-networks"></div>
                  <div id="account-data" class="info-container"></div>
                  <div id="cumulative-hours" class="info-container" style="background-image: url('img/app/fondo_orange.png');"></div>
                  <div id="home-sponsor" class="row margin-bottom"></div>
                  <div id="home-menu" class="grid-container-2 row margin-bottom"></div>
                  <div id="home-speaker" class="row margin-bottom"></div>
            `)
            renderBenefitContent(user)
            renderSocialNetworks()
            renderAccountData(user)
            renderCumulativeHoursSkeleton()
            renderCumulativeHours()
            getSponsorsSpeakers()
            getMenu()
        } else {
            $('#title-home').text('¡BEKANWE, ' + user.user.split(' ')[0] + "!")
            $('#home-content').append(`
           <div class="row margin-vertical text-align-center">
            <div class="col-100">
              <div class="navbar-logo">
                <img src="img/app/logo.png" alt=""/>
              </div>
            </div>
          </div>    
              <div id="social-networks" class="social-networks"></div>
              <div id="home-menu" class="grid-container-2"></div>
              <div id="home-sponsor" class="row margin-bottom margin-top"></div>      
              <div id="home-speaker" class="row margin-bottom margin-top"></div>
        `)
            renderSocialNetworks()
            renderMenu('admin')
            getSponsorsSpeakers()
        }
    }
    const getSponsorsSpeakers = () => {
        renderSponsorSkeleton()
        renderSpeakerSkeleton()
        app.request({
            url: `${getRouteApi()}/congress/v1/api/home`,
            method: 'GET',
            contentType: 'application/json',
            // headers: {
            //     'Authorization': 'Bearer ' + localStorage.getItem('token') // reemplaza token con tu token
            // },
            success: (data, status, xhr) => {
                const dataL = JSON.parse(data)
                const speaker = dataL.data.speaker
                const sponsor = dataL.data.sponsor
                console.log(speaker)
                setTimeout(() => {
                    renderSpeaker(speaker)
                    renderSponsor(sponsor)
                }, 1000)
            },
            error: function (xhr, status) {
                console.log('Error: ' + status);
            }
        });
    }
    const renderBenefitContent = (user) => {
        console.log('redner benefit')
        $('#benefit-content').append(`
            <div class="col-100">
                    <img src="${user.datos.benefitImage}" alt="" class="w-100">
                </div>
            `)
    }
    const renderSpeaker = (speaker) => {
        console.log('speaker', speaker)
        $('#home-speaker').empty()
        let html = `
              <div class="row margin-bottom-half">
                <div class="col">
                  <h6 class="title">Ponentes</h6>
                </div>
                <div class="col-auto">
                    <a href="/home-speakers/">Ver más</a>
                </div>
              </div>
                <div class="col-100 no-padding-horizontal">
                  <!-- swiper Companies -->
                  <div class="swiper-container connectionwiper swiperSpeaker">
                    <div class="swiper-wrapper">
            `
        speaker.forEach(sp => {
            html += `            
                  <div class="swiper-slide">
                    <a href="/home-speaker/${sp.id}/" class="card text-align-center">
                      <div class="card-content card-content-padding padding-half-sm">
                        <figure class="avatar avatar-default elevation-2 rounded-18">
                          <img src="${sp.url}" alt="${sp.name}"/>
                        </figure>
                      </div>
                    </a>
                  </div>
            `

        })
        html += `        
                    </div>
                  </div>
                </div>
        `
        $('#home-speaker').append(html)
        /* swiper carousel connectionwiper */
        var swiper1 = new Swiper('.swiperSpeaker', {
            slidesPerView: "auto",
            spaceBetween: 0,
            pagination: true,
            autoplay: {
                delay: 1500,
                disableOnInteraction: false,
            }
        }, false);
    }
    const renderSponsor = (sponsor) => {
        $('#home-sponsor').empty()
        console.log('sponsor', sponsor)
        let html = `
              <div class="row margin-bottom-half">
                <div class="col">
                  <h6 class="title">Sponsors</h6>
                </div>
                <div class="col-auto">
                    <a href="/home-sponsors/">Ver más</a>
                </div>
              </div>
                <div class="col-100 no-padding-horizontal">
                  <!-- swiper Companies -->
                  <div class="swiper-container connectionwiper swiperSponsors">
                    <div class="swiper-wrapper">
            `
        sponsor.forEach(sp => {
            html += `            
                  <div class="swiper-slide">
                    <a href="/home-sponsor/${sp.id}/" class="card text-align-center">
                      <div class="card-content card-content-padding padding-half-sm">
                        <figure class="avatar avatar-default elevation-2 rounded-18">
                          <img src="${sp.url}" alt="${sp.name}"/>
                        </figure>
                      </div>
                    </a>
                  </div>
            `

        })
        html += `        
                    </div>
                  </div>
                </div>
        `
        $('#home-sponsor').append(html)
        /* swiper carousel connectionwiper */
        var swiper2 = new Swiper(".swiperSponsors", {
            slidesPerView: "auto",
            spaceBetween: 0,
            pagination: true,
            autoplay: {
                delay: 1500,
                disableOnInteraction: false,
            }
        });
    }
    const renderSponsorSkeleton = () => {
        $('#home-sponsor').empty()
        const sponsor = [
            {
                name: ""
            },
            {
                name: ""
            },
            {
                name: ""
            },
            {
                name: ""
            },
            {
                name: ""
            },
        ]
        console.log('sponsor', sponsor)
        let html = `
              <div class="row margin-bottom-half">
                <div class="col">
                  <h6 class="title">Sponsors</h6>
                </div>
                <div class="col-auto">
                    <a href="/home-sponsors"/>
                </div>
              </div>
                <div class="col-100 no-padding-horizontal">
                  <!-- swiper Companies -->
                  <div class="swiper-container connectionwiper swiperSponsors">
                    <div class="swiper-wrapper">
            `
        sponsor.forEach(sp => {
            html += `            
                  <div class="swiper-slide">
                    <a href="/home-sponsor/${sp.id}/" class="card text-align-center">
                      <div class="card-content card-content-padding padding-half-sm skeleton-effect-wave">
                        <figure class="avatar avatar-default elevation-2 rounded-18 skeleton-block" style="height: 100px">
                        </figure>
                      </div>
                    </a>
                  </div>
                `
        })
        html += `        
                    </div>
                  </div>
                </div>
        `
        $('#home-sponsor').append(html)
    }
    const renderSpeakerSkeleton = () => {
        $('#home-speaker').empty()
        const sponsor = [
            {
                name: ""
            },
            {
                name: ""
            },
            {
                name: ""
            },
            {
                name: ""
            },
            {
                name: ""
            },
        ]
        console.log('sponsor', sponsor)
        let html = `
              <div class="row margin-bottom-half">
                <div class="col">
                  <h6 class="title">Sponsors</h6>
                </div>
                <div class="col-auto">
                </div>
              </div>
                <div class="col-100 no-padding-horizontal">
                  <!-- swiper Companies -->
                  <div class="swiper-container connectionwiper swiperSponsors">
                    <div class="swiper-wrapper">
            `
        sponsor.forEach(sp => {
            html += `            
                  <div class="swiper-slide">
                    <a href="/home-sponsor/${sp.id}/" class="card text-align-center">
                      <div class="card-content card-content-padding padding-half-sm skeleton-effect-wave">
                        <figure class="avatar avatar-default elevation-2 rounded-18 skeleton-block" style="height: 100px">
                        </figure>
                      </div>
                    </a>
                  </div>
                `
        })
        html += `        
                    </div>
                  </div>
                </div>
        `
        $('#home-speaker').append(html)
    }
    const renderSocialNetworks = () => {
        $('#social-networks').append(`
            <a class="social-networks-icon external" href="https://www.instagram.com/coneic_ucayali.oficial">
              <i class="bi bi-instagram"></i>
            </a>
            <a class="social-networks-icon external" href="https://www.facebook.com/juntossomosconeicucayali">
              <i class="bi bi-facebook"></i>
            </a>
            <a class="social-networks-icon external" href="https://www.linkedin.com/company/xxxi-coneic-ucayali-2024/about/">
              <i class="bi bi-linkedin"></i>
            </a>
            <a class="social-networks-icon external" href="https://www.youtube.com/@coneic_ucayali_2024">
              <i class="bi bi-youtube"></i>
            </a>
            <a class="social-networks-icon external tiktok" href="https://www.tiktok.com/@xxxi_coneic_ucayali_2024">
              <i class="bi bi-tiktok"></i>
            </a>
            `)
    }
    const renderAccountData = (user) => {
        $('#account-data').append(`
            <ul>
              <li><strong>Tipo de promoción </strong>: ${user.datos.promotion}</li>
              <li><strong>Tipo de cuenta</strong>: ${user.datos.condition}</li>
              <li><strong>Delegado</strong>: ${user.datos.edecan ? 'Si' : 'No'}</li>
              <li><strong>Etapa de inscripción</strong>: ${user.datos.stage}</li>
              <li><strong>Universidad</strong>: ${user.datos.university}</li>
            </ul>
            `)
    }
    const getMenu = () => {
        renderMenu(null)
    }
    const renderMenu = (user) => {
        $('#home-menu').append(`
            <a class="grid-item" href="/event-academic/">
              <i class="nav-icon bi bi-calendar-week-fill"></i>
              <p>Eventos</p>
            </a>
            <a class="grid-item" href="/olimpianeic-academic/">
              <i class="nav-icon bi bi-award-fill"></i>
              <p>Olimpianeic</p>
            </a>
            <a class="grid-item" href="/statistics/">
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clipboard-data-fill" viewBox="0 0 16 16">
                              <path d="M6.5 0A1.5 1.5 0 0 0  5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0zm3 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5z"/>
                              <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1A2.5 2.5 0 0 1 9.5 5h-3A2.5 2.5 0 0 1 4 2.5zM10 8a1 1 0 1 1 2 0v5a1 1 0 1 1-2 0zm-6 4a1 1 0 1 1 2 0v1a1 1 0 1 1-2 0zm4-3a1 1 0 0 1 1 1v3a1 1 0 1 1-2 0v-3a1 1 0 0 1 1-1"/>
                            </svg>
              <p>Estadísticas</p>
            </a>
            <a class="grid-item" href="/home-location/list/">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-buildings-fill" viewBox="0 0 16 16">
                  <path d="M15 .5a.5.5 0 0 0-.724-.447l-8 4A.5.5 0 0 0 6 4.5v3.14L.342 9.526A.5.5 0 0 0 0 10v5.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V14h1v1.5a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5zM2 11h1v1H2zm2 0h1v1H4zm-1 2v1H2v-1zm1 0h1v1H4zm9-10v1h-1V3zM8 5h1v1H8zm1 2v1H8V7zM8 9h1v1H8zm2 0h1v1h-1zm-1 2v1H8v-1zm1 0h1v1h-1zm3-2v1h-1V9zm-1 2h1v1h-1zm-2-4h1v1h-1zm3 0v1h-1V7zm-2-2v1h-1V5zm1 0h1v1h-1z"/>
                </svg>
              <p>Ubicaciones</p>
            </a>
            <a class="grid-item nav-link popup-open" ${user ? '' : 'data-popup=".popup-menu-benefit"'}>
              <i class="nav-icon bi bi-bookmark-check-fill"></i>
              <p>Beneficios</p>
            </a>
            <a class="grid-item" href="/home-university/list/">
              <i class="nav-icon bi bi-bank"></i>
              <p>Universidades</p>
            </a>
            `)
    }
    const renderCumulativeHours = () => {
        app.request({
            url: `${getRouteApi()}/congress/v1/api/participant/events-accumulation-hour`,
            method: 'GET',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                const accumulationHour = JSON.parse(data).data;
                $('#cumulative-hours').empty()
                $('#cumulative-hours').append(`
            <p>
            <strong>Horas acumuladas</strong>
            <br>
              <span class="highlight">
                <span class="number-box-hour">${accumulationHour.total_hours.charAt(0)}</span>
                <span class="number-box-hour">${accumulationHour.total_hours.charAt(1)}</span>
                <span class="text-box-hour">:</span>
                <span class="number-box-hour">${accumulationHour.total_minutes.charAt(0)}</span>
                <span class="number-box-hour">${accumulationHour.total_minutes.charAt(1)}</span>
            </span>
            </p>
        `)
            },
            error: function (xhr, status) {
                console.log('Error: ' + status);
            }
        });
    }
    const renderCumulativeHoursSkeleton = () => {
        $('#cumulative-hours').append(`
            <div class="skeleton-effect-wave skeleton-text">
            <p class="">
            <strong>Horas acumuladas</strong>
            <br>
              <span class="highlight">
                <span class="">--</span>
                <span class="">--</span>
                <span class="">:</span>
                <span class="">--</span>
                <span class="">--</span>
            </span>
            </p>
            </div>
        `)
    }
    const initHome = () => {
        console.log('initHome')
        // const sHomeContent = $('#home-content')
        // sHomeContent.empty()
        // sHomeContent.append(`
        // `)
        render()
        // getHome()
    }
    const isNow = (fechaString) => {
        const fechaDate = new Date(fechaString + "T00:00:00");
        console.log(fechaDate)
        const fechaActual = new Date();

        return fechaDate.getFullYear() === fechaActual.getFullYear() &&
            fechaDate.getMonth() === fechaActual.getMonth() &&
            fechaDate.getDate() === fechaActual.getDate()
    }
    const showBanner = () => {
        const user = JSON.parse(localStorage.getItem('user'))
        if (user.rol === 'PARTICIPANT') {
            if (isNow(user.dateForm)) {
                console.log('mostrar modal')
                app.popup.open('.popup-init')
            } else {
                console.log('no se muestra modal')
            }
        }
    }
    return {
        initHome,
        showBanner,
    }
}