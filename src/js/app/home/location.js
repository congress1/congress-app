import ListGenerator from "../listGenerator.js";

export default function hLocation(app) {
    const init = () => {
        console.log('init hLocation')
        let entity = 'location'
        let attributes = [
            {
                label: 'Nombre',
                value: 'name',
                type: 'Text',
                isShow: true,
                required: true
            },
            {
                label: 'Descripción',
                value: 'description',
                type: 'Text',
                isShow: true,
                required: true
            }
        ]
        const randomNumber = Math.floor(Math.random() * 1000000);

        const gList = new ListGenerator(entity, attributes, 'location-list', '', app, randomNumber, 'Lista de localidades')
        gList.isMt110()
        gList.initList()
    }
    return {
        init
    }
}