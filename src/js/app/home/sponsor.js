import getRouteApi from "../api.js";

const $ = Dom7;
export default function hSponsor(app) {
    const init = () => {

        const loadSpinner = () => {
            console.log("inicio spinner home/sponsor")
            app.dialog.preloader('cargando...');
        }

        const closeSpinner = () => {
            console.log("fin spinner home/sponsor")
            app.dialog.close();
        }
        loadSpinner()
        const sponsorId = app.views.main.router.currentRoute.params.sponsorId
        console.log('init h sponsor', sponsorId)

        app.request({
            url: `${getRouteApi()}/congress/v1/api/sponsor/${sponsorId}`,
            method: 'GET',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                console.log(JSON.parse(data))
                const sponsor = JSON.parse(data).data
                const color = getTypeColor(sponsor.type.name)
                //
                $('#sponsor-view').append(`
                <div class="row">
                    <div class="col-100 col-sm-6 medium-33 large-25">
                        <div class="card margin-bottom elevation-2 padding-half" style="border-radius: 10%;border: 4px solid ${color}; ">
                            <div class="card-content card-content-padding text-align-center elevation-2 rounded-15">
                                <div class="row align-items-center ">
                                    <div class="col-auto margin-left-auto margin-right-auto">
                                        <div class="card margin-bottom">
                                            <div class="card-content card-content-padding padding-half-sm">
                                                <figure class="avatar avatar-300 margin-left-auto margin-right-auto rounded-15">
                                                    <img src="${sponsor.url_image}" alt=""/>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h6 class="card-title no-margin-bottom c-white text-align-center">${sponsor.name}</h6>
                                <p class="text-small text-secondary small" style="color: ${color}!important;">Categoría: ${sponsor.type.name}</p>
                                <p>${sponsor.description}</p>
                            </div>
                            <div class="card-footer justify-content-center text-align-center border-0 text-align-center">
                                <a href="${sponsor.url_social_network}" class="c-white mx-2 external"><i class="bi bi-facebook"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                `)

                closeSpinner()
            },
            error: (xhr, status) => {
                console.log('Error: ' + status);
            }
        });
    }
    const fetchList = (select) => {
        app.request({
            url: `${getRouteApi()}/congress/v1/api/sponsor/list`,
            method: 'GET',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                console.log(JSON.parse(data))
                const list = JSON.parse(data).data
                select.empty()
                const listOro = list.filter(el => el.type.name === 'Oro')
                const listSilver = list.filter(el => el.type.name === 'Plata')
                const listBronze = list.filter(el => el.type.name === 'Bronce')
                const listColaborate = list.filter(el => el.type.name === 'Colaborador')
                const listOrganizador = list.filter(el => el.type.name === 'Organizador')
                const listEconomy = list.filter(el => el.type.name === 'Economy')
                console.log(listOro)
                console.log(listSilver)
                console.log(listBronze)
                renderListGold(select, listOro)
                renderListSilver(select, listSilver)
                renderListBronze(select, listBronze)
                renderListColaborate(select, listColaborate)
                renderListOrganizador(select, listOrganizador)
                renderListEconomy(select, listEconomy)
            },
            error: (xhr, status) => {
                console.log('Error: ' + status);
            }
        });
    }
    const listSponsor = () => {
        console.log('INIT LIST SPONSOR')
        const select = $('#home-sponsors')
        select.empty()
        renderListSkeleton(select, "categoria Oro")
        renderListSkeleton(select, "categoria Plata")
        renderListSkeleton(select, "categoria Bronce")
        renderListSkeleton(select, "Organizador")
        renderListSkeleton(select, "Economy")
        fetchList(select)
    }
    const renderListSkeleton = (select, title) => {
        let html = `<div class="row"><div class="col"><h3>${title}</h3></div></div><div class="row">`

        // Generar los items del esqueleto
        for (let i = 0; i < 4; i++) {
            html += `            
                <div class="col-50 col-sm-4 medium-25 skeleton-effect-wave">
                    <div class="card margin-bottom elevation-2 skeleton-text">
                        <div class="card-content card-content-padding text-align-center">
                            <div class="row align-items-center ">
                                <a href="/profile/" class="col-auto margin-left-auto margin-right-auto">
                                    <div class="card margin-bottom">
                                        <div class="card-content card-content-padding padding-half-sm">
                                            <figure class="avatar avatar-80 margin-left-auto margin-right-auto rounded-15">
                                                <div class="skeleton-block" style="width: 80px; height: 80px; border-radius: 10%"></div>
                                            </figure>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <p class="no-margin-bottom c-white text-align-center">John McMohan</p>
                            <p class="text-small c-white small">Chief Designer</p>
                        </div>
                    </div>
                </div>
            `;
        }

        html += '</div>'
        select.append(html)
    }

    function getTypeColor(type) {
        switch (type) {
            case 'Oro':
                return '#FFD700';
            case 'Plata':
                return '#C0C0C0';
            case 'Bronce':
                return '#CD7F32';
            case 'Colaborador':
                return '#000000';
            default:
                return '#FFFFFF'; // Retorno por defecto en caso de no matchear con ningún tipo.
        }
    }

    const renderListGold = (select, list) => {
        renderList(select, 'Oro', list, 'FFD700')
    }
    const renderListSilver = (select, list) => {
        console.log(list)
        renderList(select, 'Plata', list, 'C0C0C0')
    }
    const renderListBronze = (select, list) => {
        renderList(select, 'Bronce', list, 'CD7F32')
    }
    const renderListColaborate = (select, list) => {
        renderList(select, 'Colaboradores', list, '000000')
    }
    const renderListOrganizador = (select, list) => {
        renderList(select, 'Organizadores', list, '000000')
    }
    const renderListEconomy = (select, list) => {
        renderList(select, 'Economy', list, '000000')
    }
    const renderList = (select, title, list, color) => {
        let html = `<div class="row"><div class="col"><h3>${title}</h3></div></div><div class="row">`

        list.forEach((element) => {
            html += `            
                <div class="col-50 col-sm-4 medium-25">
                    <div class="card margin-bottom elevation-2">
                        <div class="card-content card-content-padding text-align-center" style=" border-radius: 10%;border: 4px solid #${color}; ">
                            <div class="row align-items-center ">
                                <a href="/home-sponsor/${element.id}/" class="col-auto margin-left-auto margin-right-auto">
                                    <div class="card margin-bottom">
                                        <div class="card-content card-content-padding padding-half-sm">
                                            <figure class="avatar avatar-80 margin-left-auto margin-right-auto rounded-15">
                                                <img src="${element.url_image}" alt=""/>
                                            </figure>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <p class="no-margin-bottom c-white text-align-center">${element.name}</p>
<!--                            <p class="text-small c-white small">Chief Designer</p>-->
                        </div>
                    </div>
                </div>
            `;
        })

        html += '</div>'
        select.append(html)
    }
    return {
        init,
        listSponsor
    }
}