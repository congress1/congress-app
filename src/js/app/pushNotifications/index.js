import {PushNotifications} from "@capacitor/push-notifications";
import {Capacitor} from "@capacitor/core";

import {FCM} from "@capacitor-community/fcm";

export default function pushNotifications(app) {
    const topicGeneral = 'topic_general'
    const topicAdmin = 'topic_admin'
    const subscriptionTopicGeneral = () => {
        // Subscribe to an FCM topic
        FCM.subscribeTo({topic: topicGeneral})
            .then(r => console.log(`subscribed to topic`))
            .catch(err => alert(JSON.stringify(err)));
        console.log('FCM finish subscribeTo')
    }
    const unsubscribeFromTopicGeneral = () => {
        // Subscribe to an FCM topic
        FCM.subscribeTo({topic: topicGeneral})
            .then(r => console.log(`unsubscribed from topic`))
            .catch(err => alert(JSON.stringify(err)));
        console.log('FCM finish subscribeTo')
    }
    const subscriptionTopicAdmin = () => {
        // Subscribe to an FCM topic
        FCM.subscribeTo({topic: topicAdmin})
            .then(r => console.log(`subscribed to topic`))
            .catch(err => alert(JSON.stringify(err)));
        console.log('FCM finish subscribeTo')
    }
    const unsubscribeFromTopicAdmin = () => {
        // Subscribe to an FCM topic
        FCM.subscribeTo({topic: topicAdmin})
            .then(r => console.log(`unsubscribed from topic`))
            .catch(err => alert(JSON.stringify(err)));
        console.log('FCM finish subscribeTo')
    }
    const addListeners = async () => {
        await PushNotifications.addListener('registration', token => {
            console.info('Registration token: ', token.value);
            console.log('My Token: ' + token.value);
        });

        await PushNotifications.addListener('registrationError', err => {
            console.error('Registration error: ', err.error);
            console.error('Error on device register ', err);
        });

        await PushNotifications.addListener('pushNotificationReceived', notification => {
            console.log('Push notification received: ', notification);
            // alert('Notificación recibida: ' + JSON.stringify(notification));
        });

        await PushNotifications.addListener('pushNotificationActionPerformed', notification => {
            console.log('Push notification action performed', notification.actionId, notification.inputValue);
            // alert('Notificación realizada: ' + JSON.stringify(notification));
        });

    }

    const registerNotifications = async () => {
        let permStatus = await PushNotifications.checkPermissions();

        if (permStatus.receive === 'prompt') {
            permStatus = await PushNotifications.requestPermissions();
        }

        if (permStatus.receive !== 'granted') {
            throw new Error('User denied permissions!');
        }
        const user = JSON.parse(localStorage.getItem('user'))
        if (user) {
            subscriptionTopicGeneral()
            if (user.rol === 'ADMIN') {
                subscriptionTopicAdmin()
            } else {

            }
        } else {
            alert('no existe usuario')
        }
        FCM.isAutoInitEnabled().then(r => {
            console.log('FCM Auto init is ' + (r.enabled ? 'enabled' : 'disabled'));
        });
    }

    const getDeliveredNotifications = async () => {
        const notificationList = await PushNotifications.getDeliveredNotifications();
        console.log('delivered notifications', notificationList);
    }
    const initAndroid = async () => {

        if (Capacitor.getPlatform() === 'android' || Capacitor.getPlatform() === 'ios') {
            try {
                await registerNotifications();
                await addListeners();
            } catch (error) {
                console.error('Error during push notification setup:', error);
            }
        }
    }
    const unsubscribeTopics = (rol) => {
        if (Capacitor.getPlatform() === 'android' || Capacitor.getPlatform() === 'ios') {
            unsubscribeFromTopicGeneral()
            if (rol === 'ADMIN') {
                unsubscribeFromTopicAdmin()
            } else {

            }
        } else {
            console.log('NO ES ANDROID NO SE PUEDE DESUSCRIBIR')
        }
    }
    return {
        initAndroid,
        unsubscribeTopics
    }
}