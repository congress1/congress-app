import getRouteApi from "./api.js";

const $ = Dom7;

// var $ = jQuery.noConflict();
class FormGenerator {
    id
    formAux = {}
    tableCache = {};
    uriCustomSubmit = null
    entityUnique = null
    listSelected = []
    methodCreate = 'POST'
    listSelectedTemp = []
    listSelectedIds = []
    constructor(entity, attributes, containerId, popupClass, app, randomNumber) {
        this.$ = $;
        this.entity = entity;
        this.entityUnique = entity + randomNumber;
        this.attributes = attributes;
        this.containerId = containerId;
        this.popupClass = popupClass;
        this.app = app;
        this.id = null
    }

    setId(id) {
        this.id = id
        this.initForm()
    }

    setPopupClass(popupClass) {
        this.popupClass = popupClass
    }

    setUriCustomSubmit(uriCustomSubmit) {
        this.uriCustomSubmit = uriCustomSubmit
    }

    setMethodCreate(methodCreate) {
        this.methodCreate = methodCreate
    }

    generateInputField(attribute) {
        const required = attribute.required ? "required" : "";
        if (attribute.type.toLowerCase() === 'color') {
            return `<div class="col-100 medium-50 large-33 margin-bottom-half margin-top">
                <div class="list form-list no-margin">
                    <ul>
                      <li>
                        <div class="item-content item-input">
                          <div class="item-media  align-self-center">
                            <i class="icon demo-list-icon" id="${this.entityUnique}-${attribute.value}-value"></i>
                          </div>
                          <div class="item-inner">
                            <div class="item-input-wrap">
                              <input type="text" placeholder="Color" readonly="readonly" id="${this.entityUnique}-${attribute.value}" />
                              <div id="error-${this.entityUnique}-${attribute.value}" class="item-input-error-message d-block"></div>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                </div>
            </div>`;
        }
        return `<div class="col-100 medium-50 large-33 margin-bottom-half margin-top">
                <div class="list form-list no-margin">
                    <ul>
                        <li class="item-content item-input">
                            <div class="item-inner">
                                <div class="item-title item-floating-label">${attribute.label}</div>
                                <div class="item-input-wrap">
                                    <input type="${attribute.type.toLowerCase()}" id="${this.entityUnique}-${attribute.value}" ${required}/>
                                    <div id="error-${this.entityUnique}-${attribute.value}" class="item-input-error-message d-block"></div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>`;
    }

    generateInputFieldTime(attribute) {
        const required = attribute.required ? "required" : "";

        return `<div class="col-100 medium-50 large-33 margin-bottom-half">
                <div class="list form-list no-margin">
                    <ul>
                        <li class="item-content item-input">
                            <div class="item-inner">
                                <div class="item-title item-floating-label">${attribute.label}</div>
                                <div class="item-input-wrap">
                                    <input type="text" id="${this.entityUnique}-${attribute.value}" ${required} readonly="readonly"/>
                                    <div id="error-${this.entityUnique}-${attribute.value}" class="item-input-error-message d-block"></div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>`;
    }

    generateSelectField(attribute) {
        const required = attribute.required ? "required" : "";
        // const options = attribute.options.map(option => `<option>${option}</option>`).join('');
        const options = attribute.options.map(option => `<option value="${option.value}">${option.label}</option>`).join('');
        return `<div class="col-100 medium-50 large-33 margin-bottom-half">
                <div class="list form-list no-margin">
                    <ul>
                        <li class="item-content item-input">
                            <div class="item-inner">
                                <div class="item-title item-floating-label">${attribute.label}</div>
                                <div class="item-input-wrap">
                                    <div>
                                        <select id="${this.entityUnique}-${attribute.value}" ${required}>
                                            ${options}
                                        </select>
                                        <span>x</span>
                                    </div>
                                    <div id="error-${this.entityUnique}-${attribute.value}" class="item-input-error-message d-block"></div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>`;
    }

    getTable(uri) {
        return new Promise((resolve, reject) => {
            this.app.request({
                url: `${getRouteApi()}/congress/v1/api/${uri}`,
                method: 'GET',
                contentType: 'application/json',
                success: (data, status, xhr) => {
                    const result = JSON.parse(data).data;
                    console.log(result);
                    resolve(result);
                },
                error: (xhr, status) => {
                    console.log('Error: ' + status);
                    resolve([]);
                }
            });
        });
    }

    generateTableSelectFieldV2(attribute) {
        const required = attribute.required ? "required" : "";
        const data = this.tableCache[attribute.uri];
        const rows = data.map(option => `
                <tr class="${this.entityUnique}-${attribute.value}-tr">
                    <td class="">${option.name}</td>
                    <td class="label-cell ">
                        <input type="checkbox" class="${this.entityUnique}-${attribute.value}-checkbox" name="demo-checkbox" value="${option[attribute.keyTable ?? 'id']}"/>
                    </td>
                </tr>
            `).join('');

        return `<div class="col-100 medium-50 large-33 margin-bottom-half">
                <div class="list form-list no-margin">
                    <ul>
                        <li class="">
                        
                        <div>
                        <button class="button mt-2 button-raised button-fill color-theme" id="${this.entityUnique}-${attribute.value}-add">
                          Agregar ${attribute.label}
                        </button>                        
                        <div id="error-${this.entityUnique}-${attribute.value}" class="item-input-error-message d-block"></div>
                        <div class="d-block card data-table p-2 text-align-center" style="display: none;" 
                                id="${this.entityUnique}-${attribute.value}-table">
                            <table id="${this.entityUnique}-${attribute.value}-selects" class="">
                                <thead>
                                    <tr>
                                        <th class="numeric-cell c-white" style="font-size: 16px">#</th>
                                        <th class="c-white" style="font-size: 16px">${attribute.label}</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        </div>
                        <div class="popup ${this.entityUnique}-${attribute.value}-popup-add">
                            <div class="page">
                                <div class="navbar">
                                  <div class="navbar-bg"></div>
                                  <div class="navbar-inner">
                                    <div class="title" id="popup-title">Agregar</div>
                                    <div class="right">
                                        <a href="#" class="link popup-close" 
                                            id="${this.entityUnique}-${attribute.value}-popup-add-close">
                                            Cerrar
                                        </a>
                                    </div>
                                  </div>
                                </div>
                                <div class="page-content">                                
                                    <div class="data-table">   
                                    <div>
                                        <form data-search-container=".search-list" data-search-in=".item-title" class="searchbar searchbar-init">
                                          <div class="searchbar-inner">
                                            <div class="searchbar-input-wrap">
                                              <input type="search" placeholder="Buscar"/>
                                              <i class="searchbar-icon"></i>
                                              <span class="input-clear-button"></span>
                                            </div>
                                            <span class="searchbar-disable-button if-not-aurora">Cancel</span>
                                          </div>
                                        </form>
                                    </div>                                  
                                    <table>
                                        <tbody id="${this.entityUnique}-${attribute.value}-body">
                                            ${rows}
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </li>
                    </ul>
                </div>
            </div>`;
    }

    generateTableSelectField(attribute) {
        const required = attribute.required ? "required" : "";
        let data = this.tableCache[attribute.uri];
        if (attribute.filter) {
            data = attribute.filter(data)
        }
        const options = data?.map(option => `<option value="${option[attribute.keyTable ?? 'id']}">${option.name}</option>`).join('');
        if (!attribute.isMultiple) {
            return `<div class="col-100 medium-50 large-33 margin-bottom-half">
                <div class="list form-list no-margin">
                    <ul>
                        <li class="item-content item-input"  id="${this.entityUnique}-${attribute.value}-li">
                            <div class="item-inner">
                                <div class="item-title item-floating-label">${attribute.label}</div>
                                <div class="item-input-wrap">
                                    <div class="d-flex">
                                        <select id="${this.entityUnique}-${attribute.value}" ${required}>
                                            ${options}
                                        </select>
                                        <div class="clear-select" id="${this.entityUnique}-${attribute.value}-select">
                                            <span>x</span>
                                        </div>
                                    </div>
                                    <div id="error-${this.entityUnique}-${attribute.value}" class="item-input-error-message d-block"></div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>`;
        } else {
            return `<div class="col-100 medium-50 large-33 margin-bottom-half">
                        <div class="list form-list no-margin">
                            <ul>
                                <li >
                                   <a class="item-link smart-select smart-select-init" data-open-in="popover">
                                        <select id="${this.entityUnique}-${attribute.value}" ${required} multiple>
                                            ${options}
                                        </select>                            
                                        <div id="error-${this.entityUnique}-${attribute.value}" class="item-input-error-message d-block"></div>
                                        <div class="item-content p-0">
                                          <div class="item-inner">
                                            <div class="item-title">${attribute.label}</div>
                                          </div>
                                        </div>
                                      </a>
                                </li>
                            </ul>
                        </div>
                    </div>`;
        }
    }

    generateBooleanField(attribute) {
        const required = attribute.required ? "required" : "";
        return `<div class="col-100 medium-50 large-33 margin-bottom-half">
            <div class="list form-list no-margin">
                <ul>
                    <li class="item-content item-input">
                    <div class="row">
                            <div class="col-auto">
                                <label class="toggle toggle-init color-green">
                                    <input type="checkbox" id="${this.entityUnique}-${attribute.value}" ${required} value="0"/>
                                    <span class="toggle-icon"></span>
                                </label>
                            </div>
                            <div class="col">
                                <h5 class="no-margin-bottom">${attribute.label}</h5>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>`;
    }

    generateAttributeField(attribute) {
        switch (attribute.type) {
            case 'Text':
            case 'Number':
            case 'Date':
            case 'Color':
                return this.generateInputField(attribute);
            case 'Time':
                return this.generateInputFieldTime(attribute);
            case 'Enum':
                return this.generateSelectField(attribute);
            case 'Boolean':
                return this.generateBooleanField(attribute);
            case 'Table':
                return this.generateTableSelectField(attribute);
            case 'TableList':
                return this.generateTableSelectFieldV2(attribute);
            default:
                return '';
        }
    }

    generateAttributeConditional(attribute) {
        const conditionals = attribute.conditionals
        console.log("valor1 :: ", attribute.value)
        if (conditionals) {
            console.log("conditionals :: ", conditionals);
            conditionals.forEach(conditional => {
                let valueConditional = this.$(`#${this.entityUnique}-${conditional.value}`).val()
                console.log("valor de conditional :: ", conditional.value, " igual a true? :: ", valueConditional === "true")
                if (valueConditional === "true") {
                    valueConditional = true
                } else if (valueConditional === "false") {
                    valueConditional = false
                }
                console.log("condicional :: ", conditional.conditional, " igual a equals :: ", conditional.conditional === 'equals')
                if (conditional.conditional === 'equals') {
                    if (conditional.type === 'show') {
                        console.log('valueConditional', valueConditional)
                        console.log('conditional.equalTo', conditional.equalTo)
                        if (valueConditional === conditional.equalTo) {
                            attribute.showConditional = 'Show';
                        } else {
                            attribute.showConditional = 'Hidden';
                        }
                    }
                }
            })
        }
        return attribute
    }

    generateShowHiddenConditional(attribute) {
        const conditionals = attribute.conditionals
        if (conditionals) {
            if (attribute.showConditional === 'Show') {
                this.$(`#${this.entityUnique}-${attribute.value}`).parent().parent().parent().parent().parent().parent().show()
            } else {
                this.$(`#${this.entityUnique}-${attribute.value}`).parent().parent().parent().parent().parent().parent().hide()
            }
        }
    }

    resolvePath(path, obj) {
        return path.split('.').reduce((prev, curr) => {
            return (prev ? prev[curr] : undefined);
        }, obj);
    }

    setValue(attribute) {

        const value = this.resolvePath(attribute.values ? attribute.values.valueEdit : attribute.value, this.formAux) ?? attribute.default ?? null;

        switch (attribute.type) {
            case 'Text':
            case 'Number':
            case 'Date':
            case 'Enum':
            case 'Time':
                if (value) {
                    this.$(`#${this.entityUnique}-${attribute.value}`).parent().parent().parent().addClass('item-input-with-value');
                    console.log('set value', value)
                    console.log('etiqueta', `#${this.entityUnique}-${attribute.value}`)
                    this.$(`#${this.entityUnique}-${attribute.value}`).val(value)
                } else {
                    console.log('no entro pipipi tag', `#${this.entityUnique}-${attribute.value}`)
                    this.$(`#${this.entityUnique}-${attribute.value}`).val(null)
                }
                break
            case 'Color':
                if (value) {
                    console.log('set value', value)
                    console.log('etiqueta', `#${this.entityUnique}-${attribute.value}`)
                    this.$(`#${this.entityUnique}-${attribute.value}`).val(value)
                } else {
                    console.log('no entro pipipi tag', `#${this.entityUnique}-${attribute.value}`)
                    this.$(`#${this.entityUnique}-${attribute.value}`).val(null)
                }
                console.log('set color value', value)
                this.app.colorPicker.create({
                    inputEl: `#${this.entityUnique}-${attribute.value}`,
                    targetEl: `#${this.entityUnique}-${attribute.value}-value`,
                    targetElSetBackgroundColor: true,
                    modules: ['rgb-sliders'],
                    openIn: 'popover',
                    sliderValue: true,
                    sliderLabel: true,
                    value: {
                        hex: value ?? '#0000ff',
                    },
                });
                break
            case 'Boolean':
                console.log('Boolean value table', value, attribute, this.formAux)
                if (value) {
                    this.$(`#${this.entityUnique}-${attribute.value}`).parent().parent().parent().addClass('item-input-with-value');
                    console.log('set value', attribute, value)
                    this.$(`#${this.entityUnique}-${attribute.value}`).prop('checked', value);
                    this.$(`#${this.entityUnique}-${attribute.value}`).val(value)
                } else {
                    this.$(`#${this.entityUnique}-${attribute.value}`).val(false)
                }
                break
            case 'Hidden':
                if (value) {
                    this.formAux[attribute.value] = value
                    console.log('this.formAux', this.formAux)
                }
                break
            case 'TableList':
            case 'Table':
                console.log('value table', value, attribute.value)
                if (value) {
                    this.$(`#${this.entityUnique}-${attribute.value}`).parent().parent().parent().addClass('item-input-with-value');
                    this.$(`#${this.entityUnique}-${attribute.value}`).val(value)
                } else {
                    this.$(`#${this.entityUnique}-${attribute.value}`).val(null)
                }
                break
            default:
        }

    }

    generateHalfHourBlocks = () => {
        const timeBlocks = [];
        for (let i = 0; i < 24; ++i) {
            // generate a string for each hour and the half hour mark
            const hourString = i.toString().padStart(2, '0');
            timeBlocks.push(hourString + ":00");
            timeBlocks.push(hourString + ":15");
            timeBlocks.push(hourString + ":30");
        }
        return timeBlocks;
    }

    bindFieldValidation(attribute) {
        if (attribute.type === 'Table') {
            this.$(`#${this.entityUnique}-${attribute.value}-select`).hide()
            this.$(`#${this.entityUnique}-${attribute.value}-select`).on('click', (e) => {
                console.log(e, 'limpiar')
                this.$(`#${this.entityUnique}-${attribute.value}`).val(null)
                console.log(this.$(`#${this.entityUnique}-${attribute.value}`).parent().parent().parent())
                this.$(`#${this.entityUnique}-${attribute.value}-li`).removeClass('item-input-with-value')
                this.$(`#${this.entityUnique}-${attribute.value}-li`).addClass('item-input')
                // this.$(`#${this.entityUnique}-${attribute.value}`).removeClass('input-with-value')
                this.$(`#${this.entityUnique}-${attribute.value}-select`).hide()
            })
        }
        if (attribute.type === 'Time') {
            this.app.picker.create({
                inputEl: `#${this.entityUnique}-${attribute.value}`,
                cols: [
                    {
                        textAlign: 'center',
                        values: this.generateHalfHourBlocks()
                    }
                ]
            });
        }
        if (attribute.type === 'TableList') {
            //todo evento post creacion de fields
            this.$(`#${this.entityUnique}-${attribute.value}-add`).on('click', (e) => {
                console.log('click', e, `#${this.entityUnique}-${attribute.value}-popup-add`)
                this.app.popup.open(`.${this.entityUnique}-${attribute.value}-popup-add`);
            })
            // var selectedTemp = [];
            // var selectedIds = [];
            this.listSelectedTemp[`${this.entityUnique}-${attribute.value}`] = [];
            this.listSelectedIds[`${this.entityUnique}-${attribute.value}`] = [];
            var isMultipleSelection = attribute.isMultiple; // Puedes cambiar este valor según tus requerimientos
            this.$('input[type="search"]').on('input', (event) => {
                let searchVal = $(event.target).val().toLowerCase();
                console.log('searchVal', searchVal)
                this.$(`#${this.entityUnique}-${attribute.value}-body .${this.entityUnique}-${attribute.value}-tr`).each((index, element) => {
                    let tdElement = this.$(index).find('td:first-child');
                    if (tdElement.length > 0) {
                        let cellText = tdElement.text();
                        if (cellText === null) {
                            console.error("No text in cell:", tdElement);
                        } else {
                            cellText = cellText.toLowerCase();
                            if (!cellText.includes(searchVal)) {
                                // El texto de búsqueda no coincide, oculta la fila
                                this.$(index).hide();
                            } else {
                                // El texto de búsqueda coincide, muestra la fila
                                this.$(index).show();
                            }
                        }
                    }
                });
            });
            this.$(`.${this.entityUnique}-${attribute.value}-checkbox`).on('change', (event) => {
                console.log('Cambio registrado');
                this.$(`#error-${this.entityUnique}-${attribute.value}`).text('')
                var checkbox = $(event.target);
                var value = checkbox.val();
                console.log('value', value)
                var name = checkbox.parent().parent().find('td').text();// Recupera el nombre de la celda adyacente
                if (!isMultipleSelection) {
                    // Si no es selección múltiple, quita la marca a todos los otros checkboxes
                    this.$(`.${this.entityUnique}-${attribute.value}-checkbox`).each(function () {
                        if (this !== checkbox[0]) {
                            this.checked = false;
                        }
                    });
                    this.listSelectedTemp[`${this.entityUnique}-${attribute.value}`] = [];
                    this.listSelectedIds[`${this.entityUnique}-${attribute.value}`] = [];
                }

                if (checkbox.is(':checked')) {
                    // Si el checkbox está seleccionado, agrega el valor al array
                    console.log('selectedTemp.length', this.listSelectedTemp[`${this.entityUnique}-${attribute.value}`].length)
                    console.log('selectedIds.length', this.listSelectedIds[`${this.entityUnique}-${attribute.value}`].length)
                    console.log('attribute.maximum', attribute.maximum)
                    if (this.listSelectedTemp[`${this.entityUnique}-${attribute.value}`].length >= (attribute.maximum ?? 1)
                        && this.listSelectedIds[`${this.entityUnique}-${attribute.value}`].length >= (attribute.maximum ?? 1)) {
                        checkbox.prop('checked', false);
                        this.app.dialog.alert('Excede el limite máximo', 'Error');
                        return
                    } else {
                        this.listSelectedTemp[`${this.entityUnique}-${attribute.value}`].push({
                            value: value,
                            name: name
                        });
                        this.listSelectedIds[`${this.entityUnique}-${attribute.value}`].push(value);
                        console.log('selectedTemp', this.listSelectedTemp[`${this.entityUnique}-${attribute.value}`])
                        console.log('selectedIds', this.listSelectedIds[`${this.entityUnique}-${attribute.value}`])
                    }
                } else {
                    // Si el checkbox no está seleccionado, remueve el valor del array
                    this.listSelectedTemp[`${this.entityUnique}-${attribute.value}`] = this.listSelectedTemp[`${this.entityUnique}-${attribute.value}`].filter(item => item.value !== value);
                    this.listSelectedIds[`${this.entityUnique}-${attribute.value}`] = this.listSelectedIds[`${this.entityUnique}-${attribute.value}`].filter(item => item !== value);
                }

                // Reconstruye toda la lista
                this.renderFieldTable(attribute, this.listSelectedTemp[`${this.entityUnique}-${attribute.value}`],
                    this.listSelectedIds[`${this.entityUnique}-${attribute.value}`])
            });
            // Event handler para las filas, con función flecha
            this.$(`tr.${this.entityUnique}-${attribute.value}-tr`).on('click', (event) => {
                if (!$(event.target).is(`.${this.entityUnique}-${attribute.value}-checkbox`)) {

                    // Encuentra el checkbox en la fila
                    var checkbox = $(event.currentTarget).find(`input.${this.entityUnique}-${attribute.value}-checkbox`);

                    // Cambia el estado del checkbox
                    checkbox.prop('checked', !checkbox.prop('checked')).change();
                }
            });

        }
        this.$(`#${this.entityUnique}-${attribute.value}`).on('input blur', (event) => {
            console.log("eventTarget :: ", event.target)
            let checkCurrent = event.target.checked;
            let checkvalue = event.target.value;
            console.log("checkValue", checkvalue);
            console.log("attributeValue:: ", attribute.value);

            if (attribute.value === "documentNumber" || attribute.value === "dni") {
                checkvalue = checkvalue.replace(/\D/g, '');
                if (checkvalue.length > 8) {
                    checkvalue = checkvalue.substring(0, 8);
                }
                event.target.value = checkvalue;
            } else if (attribute.value === "cellphone" || attribute.value === "cellPhone" || attribute.value === "whatsappNumber") {
                checkvalue = checkvalue.replace(/[^0-9+]/g, '');
                if (checkvalue.length > 12) {
                    checkvalue = checkvalue.substring(0, 12);
                }
                event.target.value = checkvalue;
            } else if (attribute.value === "latitude" || attribute.value === "longitude") {
                const regex = /^-?\d{0,3}(\.\d{0,9})?$/;
                if (regex.test(checkvalue)) {
                    event.target.value = checkvalue;
                } else {
                    // Si no es válido, elimina el último carácter ingresado
                    checkvalue = checkvalue.slice(0, -1);
                    event.target.value = checkvalue;
                }
            }

            let fieldValue;
            if (attribute.type === 'Boolean') {
                fieldValue = this.$(`#${this.entityUnique}-${attribute.value}`).val(checkCurrent);
            } else {
                fieldValue = this.$(`#${this.entityUnique}-${attribute.value}`).val(checkvalue);
            }
            console.log('change2', fieldValue)
            if (fieldValue) {
                this.$(`#${this.entityUnique}-${attribute.value}-select`).show()
            } else {
                this.$(`#${this.entityUnique}-${attribute.value}-select`).hide()
            }
            // Validación adicional para el correo electrónico
            const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
            const urlRegex = /^(https?:\/\/)?([\da-z.-]+)\.([a-z.]{2,6})([\/\w .-]*)*\/?$/;
            if (attribute.value === "email" && !emailRegex.test(checkvalue)) {
                $(`#error-${this.entityUnique}-${attribute.value}`).text('Por favor, ingrese un correo electrónico válido.');
                $(`#${this.entityUnique}-${attribute.value}`).addClass('input-invalid');
            } /*else if ((attribute.value === "urlBase" || attribute.value === "urlSocialNetwork") && !urlRegex.test(checkvalue)) {
                $(`#error-${this.entityUnique}-${attribute.value}`).text('Por favor, ingrese una URL válida.');
                $(`#${this.entityUnique}-${attribute.value}`).addClass('input-invalid');
            }*/ else if (!(attribute.required && !fieldValue)) {
                $(`#error-${this.entityUnique}-${attribute.value}`).text('');
                $(`#${this.entityUnique}-${attribute.value}`).removeClass('input-invalid');
            } else {
                this.setErrorId(attribute.value, `El campo ${attribute.label} es requerido.`);
            }

            const isGroup = $(`#${this.entityUnique}-isGroup`).val() === 'true';
            if (isGroup) {
                this.handleIsGroupTrue();
            }

            console.log("type :: ", attribute.type)
            if (attribute.type !== 'Boolean') {
                this.attributes.forEach((attribute, index) => {
                    this.attributes[index] = this.generateAttributeConditional(this.attributes[index]);
                    console.log("!== boolean", this.attributes[index])
                    this.generateShowHiddenConditional(this.attributes[index]);
                });
            }
        });
    }

    // Método para validar cuando isGroup es true
    handleIsGroupTrue() {

        const minimum = parseInt($(`#${this.entityUnique}-minimum`).val(), 10);
        const maximum = parseInt($(`#${this.entityUnique}-maximum`).val(), 10);

        if (isNaN(minimum) || minimum <= 1) {
            $(`#error-${this.entityUnique}-minimum`).text('El valor mínimo debe ser mayor a 1.');
            $(`#${this.entityUnique}-minimum`).addClass('input-invalid');
        } else {
            $(`#error-${this.entityUnique}-minimum`).text('');
            $(`#${this.entityUnique}-minimum`).removeClass('input-invalid');
        }

        if (isNaN(maximum) || maximum < minimum) {
            $(`#error-${this.entityUnique}-maximum`).text('El valor máximo debe ser mayor o igual al valor mínimo.');
            $(`#${this.entityUnique}-maximum`).addClass('input-invalid');
        } else {
            $(`#error-${this.entityUnique}-maximum`).text('');
            $(`#${this.entityUnique}-maximum`).removeClass('input-invalid');
        }
    }

    // Método para validar cuando isGroup es false
     handleIsGroupFalse() {
        $(`#${this.entityUnique}-minimum`).val(1).removeClass('input-invalid');
        $(`#${this.entityUnique}-maximum`).val(1).removeClass('input-invalid');
        $(`#error-${this.entityUnique}-minimum`).text('');
        $(`#error-${this.entityUnique}-maximum`).text('');
    }

    setValidationBackend(attribute, message) {
        this.setErrorId(attribute, message);
    }

    setErrorId(attribute, message) {
        $(`#error-${this.entityUnique}-${attribute}`).text(message);
        $(`#${this.entityUnique}-${attribute}`).addClass('input-invalid')
    }

    clearForm() {
        console.log("clear form")
        this.formAux = {}
        this.id = null
        this.setDefaultValueAttribute()
    }

    submit(data) {
        console.log("submit1 uri --> ", `${getRouteApi()}/congress/v1/api/${this.entity}/${this.id}`);
        this.$(`#loading-${this.entityUnique}`).show();
        this.$(`#button-${this.entityUnique}`).attr('disabled', true);
        if (this.id) {
            this.app.request({
                url: `${getRouteApi()}/congress/v1/api/${this.entity}/${this.id}`,
                method: 'PUT',
                contentType: 'application/json',
                data: data,
                success: (data, status, xhr) => {
                    this.app.popup.close(`.${this.popupClass}`);
                    console.log('data', data)
                    this.app.emit('reloadList' + this.entityUnique, data);
                    // alert(data.message)
                    this.app.dialog.alert(JSON.parse(data).message, 'Éxito');
                    this.buttonDefault();
                    this.clearForm()
                    this.closeSpinner()
                },
                error: (xhr, status) => {
                    console.log('Error: ' + status);
                    this.catchLogic(xhr, status)
                    this.clearForm()

                }
            });
        } else {
            console.log("submit2 uri --> ", this.uriCustomSubmit ? `${getRouteApi()}/congress/v1/api/${this.uriCustomSubmit}` : `${getRouteApi()}/congress/v1/api/${this.entity}`);
            this.app.request({
                url: this.uriCustomSubmit ? `${getRouteApi()}/congress/v1/api/${this.uriCustomSubmit}` : `${getRouteApi()}/congress/v1/api/${this.entity}`,
                method: this.methodCreate,
                contentType: 'application/json',
                data: data,
                success: (data, status, xhr) => {
                    if (this.popupClass) {
                        this.app.popup.close(`.${this.popupClass}`);
                    }
                    console.log('data', data)
                    this.app.dialog.alert(JSON.parse(data).message, 'Éxito');
                    this.app.emit('reloadList' + this.entityUnique, data);
                    this.buttonDefault();
                    this.clearForm()
                    this.closeSpinner()
                },
                error: (xhr, status) => {
                    console.log('Error: ' + status);
                    this.catchLogic(xhr, status)
                    this.clearForm()
                },
            });
        }
    }

    catchLogic(xhr, status) {
        if (status >= 400 && status <= 499) {
            const response = JSON.parse(xhr.responseText).errors;
            if (response.length > 0) {
                console.log("error: ", response)
                console.log("error0: ", response[0])
                this.app.dialog.alert(response[0], 'Error');
            } else if (typeof response === 'object') {
                try {
                    console.log('La respuesta es un objeto');
                    console.log(response)
                    const firstErrorValue = Object.values(response)[0];
                    console.log(firstErrorValue);
                    this.app.dialog.alert(firstErrorValue, 'Error');
                    Object.keys(response).forEach((key) => this.setValidationBackend(key, response[key]))
                } catch (err) {
                    this.app.dialog.alert('Ocurrio un error', 'Error');
                }
            }
            // this.app.dialog.alert(JSON.parse(xhr.responseText).errors[0], 'Error');
            // document.getElementsByClassName('dialog-title')[0].style.color = 'red';
            this.closeSpinner()
            this.buttonDefault();
        }
    }

    buttonDefault() {
        console.log('buttonDefault')
        this.$(`#loading-${this.entity}`).hide();
        this.$(`#button-${this.entity}`).removeAttr('disabled');
    }

    deleteEntity() {
        console.log('entity :: ', this.entity, ' id :: ', this.id)
        this.app.request({
            url: `${getRouteApi()}/congress/v1/api/${this.entity}/${this.id}`,
            method: 'DELETE',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                if (this.popupClass) {
                    this.app.popup.close(`.${this.popupClass}`);
                }
                this.$(`#button-delete`).removeAttr('disabled');
                console.log('data', data)
                this.app.dialog.alert(JSON.parse(data).message, 'Éxito');
                this.app.emit('reloadList' + this.entityUnique, data);
                this.clearForm()
                this.closeSpinner()
            },
            error: (xhr, status) => {
                console.log('Error: ' + status);
                this.catchLogic(xhr, status)
                this.closeSpinner()
            }
        });
    }

    bindSubmitButton() {
        this.$(`#button-${this.entity}`).on('click', (e) => {
            e.preventDefault();
            let allFieldsValid = true;
            const formData = {};
            this.attributes.forEach(attribute => {
                let fieldValue = this.$(`#${this.entityUnique}-${attribute.value}`).val();
                formData[attribute.value] = !fieldValue ? null : fieldValue;
                if (attribute.type === 'Hidden') {
                    formData[attribute.value] = attribute.default;
                }
                if (attribute.type === 'TableList') {
                    formData[attribute.value] = this.listSelected[`${this.entityUnique}-${attribute.value}`];
                    fieldValue = this.listSelected[`${this.entityUnique}-${attribute.value}`]
                }
                if (attribute.type !== 'Hidden') {
                    if (attribute.type === 'TableList') {
                        if (!((fieldValue ?? []).length >= attribute.minimum)) {
                            allFieldsValid = false;
                            this.$(`#error-${this.entityUnique}-${attribute.value}`).text(`No cumple con cantidad mínima ${attribute.minimum}`);
                        } else {
                            this.$(`#error-${this.entityUnique}-${attribute.value}`).text(''); // Limpiar mensaje
                        }
                    } else {
                        if (attribute.required && !fieldValue) {
                            allFieldsValid = false;
                            this.$(`#error-${this.entityUnique}-${attribute.value}`).text(`El campo ${attribute.label} es requerido.`);
                        } else {
                            this.$(`#error-${this.entityUnique}-${attribute.value}`).text(''); // Limpiar mensaje
                        }

                        // Validación específica para el correo electrónico
                        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                        // const urlRegex = /^(https?:\/\/)?([\da-z.-]+)\.([a-z.]{2,6})([\/\w .-]*)*\/?$/;
                        if (attribute.value === "email" && !emailRegex.test(fieldValue)) {
                            allFieldsValid = false;
                            this.$(`#error-${this.entityUnique}-${attribute.value}`).text('Por favor, ingrese un correo electrónico válido.');
                        }

                        if (attribute.type === "Text" && attribute.url && !fieldValue.includes('http', 'http')) {
                            console.log("valor de URL dentro : ", attribute.url)
                            allFieldsValid = false;
                            this.$(`#error-${this.entityUnique}-${attribute.value}`).text('Por favor, ingrese una URL válida.');
                        }

                        if (attribute.min !== undefined && attribute.max !== undefined && fieldValue !== undefined) {
                            if ((attribute.min <= fieldValue.length && fieldValue.length <= attribute.max)) {
                                this.$(`#error-${this.entityUnique}-${attribute.value}`).text(''); // Limpiar mensaje
                            } else {
                                allFieldsValid = false;
                                this.$(`#error-${this.entityUnique}-${attribute.value}`).text(`El campo ${attribute.label} debe tener minimo ${attribute.min} y máximo ${attribute.max}.`);
                            }
                        }
                    }
                }
            });
            console.log(formData)
            if (allFieldsValid) {
                console.log('Todos los campos son válidos');
                this.loadSpinner()
                this.submit(formData)
            }

        });
    }

    async preloadTables() {
        const tableAttributes = this.attributes.filter(attribute => attribute.type === 'Table' || attribute.type === 'TableList');
        const tableRequests = tableAttributes.map(attribute => this.getTable(attribute.uri));
        const tableData = await Promise.all(tableRequests);

        // Populate tableCache
        tableAttributes.forEach((attribute, index) => {
            this.tableCache[attribute.uri] = tableData[index];
        });
    }

    showImage() {
        console.log("dentro de show image :: ", this.formAux);

        if ((this.containerId.includes("containerGCSponsorForm") || this.containerId.includes("containerGCSpeakerForm") || this.containerId.includes("containerGCOrganizerForm")) && this.id) {

            let htmlBase = `
            <div class="inscription-section-actions justify-content-center">
                <div class="inscription-section-child-icons" id="${this.containerId}-file">
                    <i class="bi bi-file-earmark-arrow-up" ></i>
                </div>
            </div>
            <input type="file" accept="image/*"  id="${this.containerId}-file-input" style="display: none">
            `;
            this.$(`#${this.containerId}`).append(htmlBase);
            //si tiene imagen se muestra en formulario
            let image = this.formAux?.url_image;
            if (image) {
                console.log("existe imagen en :: ", this.entity)
                let html = `
                    <div class="inscription-image-preview-container mt-15" id="${this.containerId}-container-preview">
                    <div id="${this.containerId}-preview-action" class="button-x"></div>
                        <div id="${this.containerId}-load-image">
                           <img src="${image}" alt="" class="inscription-image-preview">
                        </div>
                    </div>
                    `
                this.$(`#${this.containerId}`).append(html);
                $('.inscription-section-actions').hide();
                const sPreviewAction = `#${this.containerId}-preview-action`
                $(sPreviewAction).append(`<i class="bi bi-x"></i>`)
                $(sPreviewAction).on('click', () => {
                    $(`#${this.containerId}-load-image`).empty()
                    const iipc = `#${this.containerId}-container-preview`
                    $(iipc).empty()
                    $(iipc).append(`
                    <div id="${this.containerId}-preview-action" class="button-x"></div>
                    <div id="${this.containerId}-preview"></div>
                    `
                    )
                    $(iipc).hide()
                    $('.inscription-section-actions').show()
                })

            } else {
                console.log("no existe imagen en :: ", this.entity)
                const html = `
                      <div class="inscription-image-preview-container" id="${this.containerId}-container-preview">
                        <div id="${this.containerId}-preview-action" class="button-x"></div>
                            <div id="${this.containerId}-preview"></div>
                      </div>
                    `
                $(`#${this.containerId}`).append(html);
                $(`#${this.containerId}-container-preview`).hide()
            }
            return '';
        }
        return '';
    }

    handleOnChange = (e, entityId, photoInput, elementId) => {
        let file = e.target.files[0];
        let preview = document.getElementById(`${this.containerId}-preview`);
        let reader = new FileReader();
        console.log("cambio de imagen")
        reader.onloadend = () => {
            console.log("dentro de onloadend")
            if (file) {
                console.log("existe file")
                const iipc = `#${this.containerId}-container-preview`
                $(iipc).show()
                let image = new Image();
                image.classList.add('skeleton-effect-image');
                image.src = reader.result;
                image.classList.add('inscription-image-preview');
                preview.innerHTML = '';
                preview.append(image);
                $('.inscription-section-actions').hide()
                this.submitAction(entityId, elementId, image)
                const sPreviewAction = `#${this.containerId}-preview-action`
                $(sPreviewAction).append(`
                                <i class="bi bi-x"></i>
                            `)
                $(sPreviewAction).on('click', () => {
                    $(`#${this.containerId}-load-image`).empty()
                    const iipc = `#${this.containerId}-container-preview`
                    $(iipc).empty()
                    $(iipc).append(`
                                <div id="${this.containerId}-preview-action" class="button-x"></div>
                                <div id="${this.containerId}-preview"></div>`
                    )
                    $(iipc).hide()
                    $('.inscription-section-actions').show()
                })

                photoInput.value = '';
            }
        }

        if (file) {
            reader.readAsDataURL(file);
        }
    }

    submitAction = (entityId, elementId = 'photo-input', image) => {
        const photoInput = document.getElementById(`${this.containerId}-${elementId}`);
        if (photoInput.files.length > 0) {
            const photoFile = photoInput.files[0];
            this.uploadImage(entityId, photoFile, image);
        } else {
            console.log("No photo was taken");
        }
    }

    uploadImage = (entityId, photoFile, image) => {
        let formData = new FormData();
        formData.append('file', photoFile);

        // Muestra el cargador
        try {
            this.app.request({
                url: `${getRouteApi()}/congress/v1/api/${this.entity}/upload-image/${entityId}`,
                method: 'PUT',
                data: formData,
                xhrFields: {
                    onprogress: function (e) {
                        if (e.lengthComputable) {
                            console.log(e.loaded / e.total * 100 + '%');
                        }
                    }
                },
                success: (data, status, xhr) => {
                    image.classList.remove('skeleton-effect-image')
                    this.app.dialog.alert(JSON.parse(data).message, 'Éxito');

                },
                error: (xhr, status) => {
                    //alert(xhr)
                    image.classList.remove('skeleton-effect-image')
                    console.log('Error: ' + status);
                }
            });
        } catch (error) {
            console.log(`Ha ocurrido un error durante el servicio de cargar imagen :: ${error}`);
        }
    }

    loadPicker() {
        // Comprobamos si el elemento existe
        let colorPickerElement = document.querySelector('.color-picker');

        if (colorPickerElement) {

        } else {
            console.warn("No se encontró el elemento .color-picker");
        }
    }
    loadForm() {
        console.log("evento: ", this.containerId)
        this.$(`#${this.containerId}`).empty();
        const textButton = this.id ? 'Actualizar' : 'Guardar'
        const buttonDelete = this.id ?
            `
            <div class="col-50">
              <a id="button-${this.entity}-remove" class="button button-large button-raised button-fill color-theme-red">Eliminar</a>
            </div>
            `
            : ''
        let formHtml = '<div class="row margin-bottom">';
        this.attributes.forEach((attribute, index) => {
            this.attributes[index] = this.generateAttributeConditional(this.attributes[index]);
        });
        this.attributes.forEach(attribute => {
            formHtml += this.generateAttributeField(attribute);
        });
        formHtml += '</div>';

        formHtml += `
            ${this.showImage()}
            <div class="col-90 margin-top-auto margin-left-auto margin-right-auto padding-vertical">
                <div class="row justify-content-center">
                    <div class="${this.id ? 'col-50' : 'col-80' } text-align-right">
                      <a id="button-${this.entity}" class="button button-large button-raised button-fill color-theme">${textButton}</a>
                    </div>
                 ${buttonDelete}
                </div> 
            </div>   
        `;
        this.$(`#${this.containerId}`).append(formHtml);
        this.loadPicker()
        this.attributes.forEach(attribute => this.bindFieldValidation(attribute));
        this.bindSubmitButton();

        this.$(`#loading-${this.entity}`).hide();

        this.setDefaultValueAttribute()

        this.attributes.forEach((attribute) => {
            this.generateShowHiddenConditional(attribute);
        });
        this.$(`#button-${this.entity}-remove`).click(() => {
            this.app.dialog.confirm('¿Está seguro que desea eliminar?', 'Mensaje', () => {
                this.loadSpinner();
                console.log('eliminar con id :: ', this.id)
                this.deleteEntity()
            });
        });

        $(`#${this.containerId}-file`).on('click', () => {
            console.log("dentro de change el id de la entidad es :: ", this.formAux?.id, " entidad:: ", this.entity)
            let photoInput = document.getElementById(`${this.containerId}-file-input`);
            $(photoInput).off('change');
            $(photoInput).on('change', (e) => this.handleOnChange(e, this.formAux?.id, photoInput, 'file-input'));
            photoInput.click();
        })
    this.closeSpinner();
    }

    loadSpinner() {
        console.log("inicio spinner formGenerator")
        this.app.dialog.preloader('cargando...');
    }

    closeSpinner() {
        console.log("fin spinner formGenerator")
        this.app.dialog.close();
    }

    loadEntity() {
        console.log('obtener desde un api la data')
        this.loadSpinner()
        this.app.request({
            url: `${getRouteApi()}/congress/v1/api/${this.entity}/${this.id}`,
            method: 'GET',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                console.log(data)
                this.formAux = JSON.parse(data).data
                this.loadForm()
                this.closeSpinner();
            },
            error: (xhr, status) => {
                console.log('Error: ' + status);
                this.closeSpinner();
            }
        });
    }

    async initForm() {
        this.formAux = {}
        if (this.id) {
            console.log("Entro con id en initForm con evento:: ", this.entity)
            this.loadEntity()
        } else {
            await this.preloadTables();
            this.loadForm()
        }

    }

    renderFieldTable(attribute, selectedTemp = [], selectedIds = []) {
        console.log('renderFieldTable', selectedTemp, selectedIds)
        let listContent = '';
        for (let i = 0; i < selectedTemp.length; i++) {
            listContent += `
                    <tr id="item-${selectedTemp[i].value}">
                        <td class="numeric-cell">${i + 1}</td>
                        <td class="">${selectedTemp[i].name}</td>
                    </tr>
                `;
        }

        this.$(`#${this.entityUnique}-${attribute.value}-selects tbody`).html(listContent);
        this.listSelected[`${this.entityUnique}-${attribute.value}`] = selectedIds
        // Muestra u oculta la tabla dependiendo de si hay elementos seleccionados
        if (selectedTemp.length === 0) {
            this.$(`#${this.entityUnique}-${attribute.value}-table`).hide();
        } else {
            this.$(`#${this.entityUnique}-${attribute.value}-table`).show();
        }
    }
    setDefaultValueAttribute() {
        console.log("data desde set Value:: ", this.formAux)
        this.attributes.forEach(attribute => {
            console.log("atributes padre :: ", attribute)
            //se setea el valor
            this.setValue(attribute);
            //para los de tipo boolean se creara la funcion que seteara el valor
            console.log("atribute debajo de padre :: ", attribute.type)
            if (attribute.type === 'Color') {
            }
            if (attribute.type === 'TableList') {
                this.renderFieldTable(attribute)

                const checkboxes = document.querySelectorAll(`.${this.entityUnique}-${attribute.value}-checkbox`);
                checkboxes.forEach(checkbox => {
                    checkbox.checked = false;
                });
                this.listSelectedTemp[`${this.entityUnique}-${attribute.value}`] = [];
                this.listSelectedIds[`${this.entityUnique}-${attribute.value}`] = [];
            }
            if (attribute.type === 'Boolean') {
                let isChecked = this.$(`#${this.entityUnique}-${attribute.value}`).val();
                console.log("dentro de Boolean: ", `#${this.entityUnique}-${attribute.value}`, "valor2 :: ", this.$(`#${this.entityUnique}-${attribute.value}`).val())
                console.log("valor2 :: ", this.$(`#${this.entityUnique}-${attribute.value}`))
                if (isChecked) {
                    this.attributes.forEach((attribute, index) => {
                        this.attributes[index] = this.generateAttributeConditional(this.attributes[index]);
                        console.log(this.attributes[index])
                        this.generateShowHiddenConditional(this.attributes[index]);
                    });
                }
                this.$(`#${this.entityUnique}-${attribute.value}`).change((event) => {
                    console.log('change', event.target.checked);
                    if (event.target.checked) {
                        $(event.target).val(true);
                    } else {
                        $(event.target).val(false);
                    }
                    if (attribute.value === "isGroup") {
                        if (event.target.checked) {
                            this.handleIsGroupTrue();
                        } else {
                            this.handleIsGroupFalse();
                        }
                    }

                    this.attributes.forEach((attribute, index) => {
                        this.attributes[index] = this.generateAttributeConditional(this.attributes[index]);
                        console.log(this.attributes[index])
                        this.generateShowHiddenConditional(this.attributes[index]);
                    });
                });
            }
        });
    }
}

export default FormGenerator;