import getRouteApi from "./api.js";
import {ApiUtils} from "../utils/apiUtils";

const $ = Dom7;

class ListGenerator {
    allData = []
    data = [
        {
            "name": "Bryan Mc Giver",
            "dni": "71450637",
            "gender": "Male"
        },
        {
            "name": "Diego",
            "dni": "71450655",
            "gender": "Male"
        }
        // aquí puedes tener más objetos con datos
    ];
    paginate = {
        currentPage: 1,
        totalElements: 0,
        totalPages: 0,
    }
    isMargin = true
    search = ''
    deleteInShow = false
    disableEdit = false
    mt110 = false
    urlDelete = null
    id = "id"
    constructor(entity, attributes, containerId, popupClass, app, randomNumber, title = 'Ingrese titulo', buttonCreate = true, haveNavbar = false) {
        this.$ = $;
        this.entity = entity;
        this.randomNumber = randomNumber;
        this.attributes = attributes;
        this.containerId = containerId;
        this.popupClass = popupClass;
        this.app = app;
        this.title = title;
        this.buttonCreate = buttonCreate;
        this.haveNavbar = haveNavbar
    }
    
    setId(val) {
        this[this.id] = val
    }

    setUrlDelete(val) {
        this.urlDelete = val
    }

    noneMargin() {
        this.isMargin = false
    }

    marginTopNavbar100() {
        this.mtNavbar100 = true
    }

    isDeleteInShow() {
        this.deleteInShow = true
    }

    isMt110() {
        this.mt110 = true
    }

    isDisableEdit() {
        this.disableEdit = true
    }

    openEditForm(id) {
        if (id !== null) {
            let dataItem = this.allData.find(f => f[this.id] === id);
            console.log(dataItem)
            this.app.emit('eventEdit' + this.entity + this.randomNumber, dataItem[this.id]);
        } else {
            this.app.emit('eventEdit' + this.entity + this.randomNumber, null);
        }
        this.app.popup.open(`.${this.popupClass}`);
    }

    resolvePath(path, obj) {
        return path.split('.').reduce((prev, curr) => {
            return (prev ? prev[curr] : undefined);
        }, obj);
    }

    generateListSkeleton(skeletonItems = 10) {
        const skeletonData = {
            item1: ".................................",
            item2: "..........................",
            item3: "..........................",
        };
        const $skeletonContainer = this.$(`#${this.containerId}-list-skeleton`);
        $skeletonContainer.empty()
        $skeletonContainer.empty().addClass('container-glist');

        if (this.isMargin) {
            $skeletonContainer.addClass('mt-50');
        }

        // Generar los items del esqueleto
        for (let i = 0; i < skeletonItems; i++) {
            $skeletonContainer.append(this.generateSkeletonItem(skeletonData, i));
        }
    }

    generateSkeletonItem(skeletonData, index) {
        return `<div id="item-${index}" class="card margin-bottom-half skeleton-text skeleton-effect-wave">
                <div class="card-content card-content-padding">
                    <div class="row">
                        <div class="col-auto">
                            <div>Item 1: ${skeletonData.item1}</div>
                            <div>Item 2: ${skeletonData.item2}</div>
                            <div>Item 3: ${skeletonData.item3}</div>
                        </div>
                    </div>
                </div>
            </div>`;
    }

    renderOpenButton() {
        const idSelect = `${this.containerId}-open-button`
        let html = ''
        if (this.popupClass) {
            html += `<button class="floating-button" id="openButton">+</button>`;
        }
        $('#' + idSelect).append(html)
    }

    renderButtonSeeMore() {
        const idSelect = `${this.containerId}-see-more`
        let html = `
            <div class="footer-fixed">
                <div style="width: 150px">
                    <button class="button button-white popup-open" id="seeMore">
                        <span class="preloader" id="loadingSeeMore"></span>
                        Ver más
                    </button>
                </div>
            </div>`
        this.$('#' + idSelect).append(html)
    }

    emptyButtonSeeMore() {
        const idSelect = `${this.containerId}-see-more`
        this.$('#' + idSelect).empty()

    }

    clearList() {
        this.$(`#${this.containerId}-list`).empty();
    }

    generateListEmpty() {
        const listHtml = '<p class="text-align-center">No hay nada que mostrar</p>'
        const selectContainerId = `#${this.containerId}-list`
        const $container = this.$(selectContainerId);
        $container.empty();
        $container.append(listHtml);
    }

    generateList(list) {
        console.log('INIT GENERATE LIST')
        let listHtml = ``;
        listHtml += '<div class=" container-glist">';

        // if (this.allData.length === 0) {
        //     listHtml += '<p>No hay nada que mostrar</p>';
        // }
        list.forEach((dataItem, index) => {
            let listItemHtml = '';

            this.attributes.forEach(attribute => {
                const value = this.resolvePath(attribute.values ? attribute.values.valueList : attribute.value, dataItem)
                if (attribute.isShow) {

                    if (attribute.type === 'Enum') {
                        console.log(attribute.options)
                        let color = ''
                        const resolveColor = attribute.options.find(opt => opt.value === value)
                        if (resolveColor) {
                            color = resolveColor.color
                        }
                        console.log('resolveColor', color)
                        listItemHtml += `<div class="item item-title">
                                        <span class="badge ${color}">
                                            ${attribute.label}: ${value ? value : '-'} 
                                        </span>
                                        
                                    </div>`;
                    } else {
                        listItemHtml += `<div class="fix-text-overflow">${attribute.label}: ${value ? value : ''}</div>`;
                    }
                }
            });
            const htmlButtonDelete = this.deleteInShow ? `<div class="close-btn x-list" id="${this.containerId}-item-${dataItem[this.id]}-delete">
                                  <i class="bi bi-x"></i>
                              </div>` : ''
            listHtml += `<div id="${this.containerId}-item-${dataItem[this.id]}" class="card margin-bottom-half">
                        <div class="card-content card-content-padding">
                            <div class="row">
                                <div class="col-auto">
                                    ${listItemHtml}
                                </div>
                                <div class="col-auto">
                                ${htmlButtonDelete}
                                </div>
                            </div>
                        </div>
                    </div>`;
        });

        listHtml +=
            '</div>';
        const selectContainerId = `#${this.containerId}-list`
        const $container = this.$(selectContainerId);
        $container.append(listHtml);

        // Ahora detectamos el evento click en cada li, por su ID
        list.forEach((dataItem, index) => {
            if (!this.disableEdit) {
                $(`#${this.containerId}-item-${dataItem[this.id]}`).on('click', (e) => {
                    // Llamamos a la función openEditForm con el índice del elemento
                    this.openEditForm(dataItem[this.id]);
                })
            }
            if (this.deleteInShow) {
                $(`#${this.containerId}-item-${dataItem[this.id]}-delete`).on('click', (e) => {
                    // Llamamos a la función openEditForm con el índice del elemento
                    console.log('click delete')
                    //todo agregar logica
                    this.app.dialog.confirm('¿Está seguro que desea eliminar?', 'Mensaje', () => {
                        console.log('eliminar con id :: ', dataItem, this.entity)
                        this.deleteEntity(dataItem[this.id])
                    });
                })
            }
        });

        console.log('FINISH GENERATE LIST', selectContainerId)

    }

    logicOpenButton() {
        $('#openButton').on('click', (e) => {
            // Llamamos a la función openEditForm con el índice del elemento
            this.openEditForm(null);
        })
    }

    logicSeeMore() {
        $('#loadingSeeMore').hide();
        console.log("[DECLARE CLICK]")
        $('#seeMore').on('click', event => {
            console.log('ver más')
            console.log(this.containerId)
            $('#loadingSeeMore').show();
            $('#seeMore').attr('disabled', true);
            if (this.paginate.currentPage < this.paginate.totalPages) {
                this.paginate.currentPage = this.paginate.currentPage + 1
                this.fetchData()
            }
            console.log(this.paginate.currentPage)
        })
    }

    clearAllData() {
        this.allData = []
    }

    doneTyping() {
        let searchVal = $(`#${this.containerId}-search`).val().toLowerCase();
        console.log('search', searchVal);
        this.search = searchVal
        this.fetchData(true)
    }

    logicSearch() {
        let typingTimer;                // Timer identifier
        const doneTypingInterval = 500;  // Time in ms. (1 second)

        this.$(`#${this.containerId}-search`).on('input', (event) => {
            clearTimeout(typingTimer);  // Clear the previous timer
            typingTimer = setTimeout(() => {
                this.doneTyping();
            }, doneTypingInterval);
        });
    }

    deleteEntity(id) {
        console.log('entity :: ', this.entity, ' id :: ', id)
        ApiUtils.loadSpinner(this.app)
        this.app.request({
            url: `${getRouteApi()}/congress/v1/api/${this.urlDelete ?? this.entity}/${id}`,
            method: 'DELETE',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                console.log('data', data)
                this.app.dialog.alert(JSON.parse(data).message, 'Éxito');
                this.clearList()
                this.fetchData()
            },
            error: (xhr, status) => {
                console.log('Error: ' + status);
                ApiUtils.catchLogicListGenerator(xhr, status, this.app)
                ApiUtils.closeSpinner(this.app)
            }
        });
    }
    fetchData(isSearch = false) {
        if (isSearch) {
            this.paginate.currentPage = 1
        }
        this.$(`#${this.containerId}-list-skeleton`).show()
        this.$(`#${this.containerId}-list`).hide()
        $('#loadingSeeMore').show();
        $('#seeMore').attr('disabled', true);
        this.app.request({
            url: `${getRouteApi()}/congress/v1/api/${this.entity}?page=${this.paginate.currentPage}&search=${this.search}`,
            method: 'GET',
            contentType: 'application/json',
            // headers: {
            //     'Authorization': 'Bearer ' + localStorage.getItem('token') // reemplaza token con tu token
            // },
            success: (data, status, xhr) => {
                this.$(`#${this.containerId}-list-skeleton`).hide()
                this.$(`#${this.containerId}-list`).show()
                this.data = JSON.parse(data).data; // Parsea la respuesta a un objeto JSON
                if (isSearch) {
                    this.allData = this.data
                    this.paginate = JSON.parse(data).paginate
                    this.clearList()
                    if (this.data.length === 0) {
                        this.generateListEmpty()
                    }
                } else {
                    this.data = JSON.parse(data).data; // Parsea la respuesta a un objeto JSON
                    this.allData = this.allData.concat(this.data)
                    this.paginate = JSON.parse(data).paginate

                    if (this.allData.length === 0) {
                        this.generateListEmpty()
                    }
                }
                this.$(`#${this.containerId}-count-list`).empty()
                this.$(`#${this.containerId}-count-list`).append(`<p>Total: ${this.paginate.totalElements}</p>`)
                this.generateList(this.data)
                this.generateListSkeleton(this.data.length)
                const lastPage = Math.round(this.paginate.totalElements / 10)
                if (this.paginate.totalElements > this.allData.length && this.paginate.currentPage <= lastPage && this.allData.length >= 10) {
                    console.log('ver mas')
                    this.renderButtonSeeMore()
                    this.logicSeeMore()
                } else {
                    this.emptyButtonSeeMore()
                }
                $('#seeMore').removeAttr('disabled');
                $('#loadingSeeMore').hide();
                ApiUtils.closeSpinner(this.app)
            },
            error: function (xhr, status) {
                console.log('Error: ' + status);
                $('#seeMore').removeAttr('disabled');
                $('#loadingSeeMore').hide();
                ApiUtils.closeSpinner(this.app)
            }
        });
    }

    initList() {

        let htmlButtonCreate = ''
        const htmlSearch = `
            <div style="margin-bottom: 5px">
                <form data-search-container=".search-list" data-search-in=".item-title" class="searchbar searchbar-init" style="background-color: transparent">
                  <div class="searchbar-inner">
                    <div class="searchbar-input-wrap">
                      <input type="search" placeholder="Buscar" id="${this.containerId}-search"/>
                      <i class="searchbar-icon"></i>
                      <span class="input-clear-button"></span>
                    </div>
<!--                    <span class="searchbar-disable-button if-not-aurora">Cancel</span>-->
                  </div>
                </form>
            </div>
        `
        // if (this.buttonCreate) {
        //     htmlButtonCreate = '<button class="floating-button">+</button>'
        // }
        if (this.haveNavbar) {

        }
        let htmlTitle = ''
        let classContainerList = this.mt110 ? 'mt-110' : 'mt-100'
        if (this.title) {
            htmlTitle = `                
                <div class="header-fixed ${this.haveNavbar ? this.mtNavbar100 ? 'mt-navbar-100' : 'mt-navbar' : ''}">
                <div style="display: flex; justify-content: space-between; align-items: center">
                    <h4>${this.title}
                    <h5 id="${this.containerId}-count-list"></h5>
                </div>
                <hr>`
        } else {
            htmlTitle = `                
                <div class="header-fixed-only-search ${this.haveNavbar ? this.mtNavbar100 ? 'mt-navbar-100' : 'mt-navbar' : ''}">`
            classContainerList = 'mt-70'
        }
        const html = `
                ${htmlTitle}
                ${htmlButtonCreate}
                ${htmlSearch}
            </div>
            <div id="${this.containerId}-list-skeleton" class="${this.haveNavbar ? 'mt-navbar-list' : classContainerList}" ></div>
            <div id="${this.containerId}-list" class="${this.haveNavbar ? 'mt-navbar-list' : classContainerList}" ></div>
            <div id="${this.containerId}-see-more"></div>
            <div id="${this.containerId}-open-button"></div>
            `
        this.$(`#${this.containerId}`).append(html);

        this.logicSearch()

        this.generateListSkeleton()
        if (this.buttonCreate) {
            this.renderOpenButton()
        }
        this.logicOpenButton()
        this.fetchData()
    }

}

export default ListGenerator;