import GenerateCrud from "./generateCrud.js";

export default function user(app) {
    let entity = 'user'
    let attribute = [
        {
            label: 'Nick',
            value: 'username',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Email',
            value: 'email',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Rol',
            value: 'rol',
            type: 'Table',
            keyTable: 'type',
            uri: 'role/list',
            filter: function (list = []) {
                return list.filter(f => f.name !== 'Administrador' && f.name !== 'Participante')
            },
            values: {
                valueList: 'rol.name',
                valueEdit: 'rol.id',
            },
            isShow: true,
            required: true
        },
        {
            label: 'Contraseña',
            value: 'password',
            type: 'Text',
            isShow: false,
            required: true
        },
    ]
    const generateCrud = new GenerateCrud(entity, attribute, 'containerGCUser', app, 'Lista de usuarios');

    const initGenerateCrud = () => {
        console.log('init form');
        return generateCrud.initGenerateCrud("popup-form", "Nuevo formulario")
    }
    const initForm = () => {
        console.log('init form');
        return generateCrud.initForm()
    }
    const generateList = () => {
        console.log('init list');
        return generateCrud.initList()
    }
    return {
        initGenerateCrud,
        initForm,
        generateList
    }


}