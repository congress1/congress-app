import getRouteApi from "./api.js";
import pushNotifications from "./pushNotifications/index.js";


const $ = Dom7;
// var jwt = require('jsonwebtoken');
export default function authenticate(app) {
    const {initAndroid} = pushNotifications(app)
    let loadingLogin = false
    const isLoggedIn = () => {
        return localStorage.getItem('token');
    }
    const redirectToLogin = () => {
        if (isLoggedIn()) {
            console.log('not redirectToLogin')
            app.views.main.router.navigate('/home/', {clearPreviousHistory: true});
        } else {
            console.log('redirectToLogin')
            app.views.main.router.navigate('/login/', {clearPreviousHistory: true});
        }
    }
    // const decodeJwt = (token) => {
    //     var base64Url = token.split('.')[1];
    //     var base64 = base64Url.replace('-', '+').replace('_', '/'); // Reemplazar caracteres no válidos
    //     return JSON.parse(window.atob(base64)); // Convertir de Base64 a string y parsear a JSON
    // }

    const decodeJwt = (token) => {
        try {

            const base64Url = token.split('.')[1];

            let base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
            switch (base64.length % 4) {
                case 2:
                    base64 += '==';
                    break;
                case 3:
                    base64 += '=';
                    break;
            }

            const jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
            }).join(''));

            return JSON.parse(jsonPayload);
        } catch (e) {
            console.error('Invalid JWT', e);
            return null;
        }
    };

    const login = () => {
        console.log('go login')
        const {isValid, formLogin} = logicForm()
        if (!isValid) {
            return
        }

        loadingLogin = true
        $('#loadingLogin').show();
        $('#buttonLogin').attr('disabled', true);
        console.log('formLogin', formLogin)
        app.request({
            url: `${getRouteApi()}/congress/v1/api/auth/login`,
            method: 'POST',
            contentType: 'application/json',
            data: formLogin,
            success: function (data, status, xhr) {
                localStorage.setItem("token", JSON.parse(data).data)
                // Decodificar el token
                var decodedToken = decodeJwt(data);

                console.log('decodedToken', decodedToken);
                localStorage.setItem("user", JSON.stringify(decodedToken))
                loadingLogin = false
                $('#buttonLogin').attr('disabled', false);
                $('#loadingLogin').hide();
                initAndroid()
                app.views.main.router.navigate('/home/', {clearPreviousHistory: true});
            },
            error: function (xhr, status) {
                const response = JSON.parse(xhr.responseText).errors;
                if (status === 503) {
                    app.views.main.router.navigate(`/confirmCode/${response[0]}/`, {clearPreviousHistory: true});
                } else {
                    if (status === 401) {
                        console.log('Error: ' + status);
                        console.log(JSON.parse(xhr.responseText).errors)
                        app.dialog.alert(response[0], 'Error');
                    }
                }
                $('#buttonLogin').removeAttr('disabled');
                $('#loadingLogin').hide();
            }
        });

        // setTimeout(function () {
        //     loadingLogin = false
        //     $('#buttonLogin').attr('disabled', false);
        //     $('#loadingLogin').hide();
        //     app.views.main.router.navigate('/home/');
        // }, 1000);

    }
    const logicForm = () => {

        var isValid = true;

        // Validación para el campo de usuario
        var usuario = $("#login-email").val();
        if (usuario === "") {
            $("#login-email").addClass('is-invalid');
            $("#login-email-error").show();
            $("#login-email-error").text('Por favor, ingresa tu usuario.');
            isValid = false;
        } else {
            $("#login-email").removeClass('is-invalid');
            $("#login-email-error").text('');
            $("#login-email-error").hide();
        }

        // Validación para el campo de contraseña
        var password = $("#login-password").val();
        if (password.length <= 4) {
            $("#login-password").addClass('is-invalid');
            $("#login-password-error").show();
            $("#login-password-error").text('La contraseña debe tener al menos 8 caracteres.');
            isValid = false;
        } else {
            $("#login-password").removeClass('is-invalid');
            $("#login-password-error").text('');
            $("#login-password-error").hide();
        }

        // Bloquea la sumisión si el formulario no es válido
        const formLogin = {
            "username": usuario, "password": password
        }
        return {isValid, formLogin}
    }
    const initLogin = () => {
        const show = false
        const sTogglePasswordIcon = $("#toggle-password-icon")
        sTogglePasswordIcon.on('click', function () {
            $(this).toggleClass("bi-eye-fill bi-eye-slash-fill");
            let inputType = $("#login-password").attr("type");
            if (inputType === "password") {
                $("#login-password").attr("type", "text");
            } else {
                $("#login-password").attr("type", "password");
            }
        })
    }
    return {
        redirectToLogin,
        login,
        loadingLogin,
        logicForm,
        initLogin
    }
}