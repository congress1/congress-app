export default function initMap() {
    var location = {lat: -25.363, lng: 131.044};  // Reemplaza estas coordenadas

    var map = new google.maps.Map(document.getElementById('map-container'), {
        zoom: 4,
        center: location
    });

    var marker = new google.maps.Marker({
        position: location,
        map: map,
        title: 'Click to zoom'
    });

    map.addListener('center_changed', function () {
        // Después de 3 segundos se moverá de nuevo  al marcador
        window.setTimeout(function () {
            map.panTo(marker.getPosition());
        }, 3000);
    });

    marker.addListener('click', function () {
        window.location.href = 'https://www.google.com/maps/search/?api=1&query=' + location.lat + ',' + location.lng;
    });
}