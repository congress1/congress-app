import getRouteApi from "../api.js";
import {ApiUtils} from "../../utils/apiUtils";

const $ = Dom7;
export default function inscription(app) {
    let dataList = []
    const idComponent = "inscription"
    const idComponentDetail = "inscription-view"

    const loadSpinner = () => {
        console.log("inicio spinner inscription/index")
        app.dialog.preloader('cargando...');
    }

    const closeSpinner = () => {
        console.log("fin spinner inscription/index")
        app.dialog.close();
    }

    const createComponentSkeleton = () => {
        const sIdComponent = `#${idComponent}`
        $(sIdComponent).empty()
        $(sIdComponent).append(
            `<div style="margin-bottom: 10px" class="">
                <form data-search-container=".search-list" data-search-in=".item-title" class="searchbar searchbar-init">
                  <div class="searchbar-inner">
                    <div class="searchbar-input-wrap">
                      <input type="search" placeholder="Buscar"/>
                      <i class="searchbar-icon"></i>
                      <span class="input-clear-button"></span>
                    </div>
                    <span class="searchbar-disable-button if-not-aurora">Cancel</span>
                  </div>
                </form>
            </div>`)
        const dataListAux = [
            {
                event_name: "................................",
                event_date: "................................",
                name: "................................",
                participants: "................................",
                description: "................................"
            },
            {
                event_name: "................................",
                event_date: "................................",
                name: "................................",
                participants: "................................",
                description: "................................"
            },
            {
                event_name: "................................",
                event_date: "................................",
                name: "................................",
                participants: "................................",
                description: "................................"
            },
            {
                event_name: "................................",
                event_date: "................................",
                name: "................................",
                participants: "................................",
                description: "................................"
            }
        ]
        dataListAux.forEach((item, index) => {
            // Create new card element
            let cardElement = `<div id="itemCard-${index}" class="card margin-bottom-half skeleton-text skeleton-effect-wave">
                                <div class="card-content card-content-padding">
                                    <div class="row">
                                        <div class="col align-self-center ">
                                             <p class="small margin-bottom-half"><div class="fw-medium mr-4">Evento: ${item.event_name}</div>
                                            </p>
                                            <p>
                                                Fecha de evento: ${item.event_date}
                                            </p>
                                            <p>
                                                Nombre de equipo: ${item.name}
                                            </p>
                                        </div>
                                        <div class="col-auto">
                                        </div>
                                    </div>
                                </div>
                            </div>`;
            $(`#${idComponent}`).append(cardElement);
        });
    }
    const createComponent = () => {
        const sIdComponent = `#${idComponent}`
        const sIdComponentData = `#${idComponent}-data`
        $(sIdComponent).empty()
        $(sIdComponent).append(
            `
            <div style="margin-bottom: 10px">
                <form data-search-container=".search-list" data-search-in=".item-title" class="searchbar searchbar-init">
                  <div class="searchbar-inner">
                    <div class="searchbar-input-wrap">
                      <input type="search" placeholder="Buscar"/>
                      <i class="searchbar-icon"></i>
                      <span class="input-clear-button"></span>
                    </div>
                    <span class="searchbar-disable-button if-not-aurora">Cancel</span>
                  </div>
                </form>
            </div>
            <div id="${idComponent}-data"></div>`)

        $('input[type="search"]').on('input', (event) => {
            let searchVal = $(event.target).val().toLowerCase();
            console.log('search')
            $(`#${idComponent}-data .file`).each((index) => {
                let tdElement = $(index).find('.title-team');
                if (tdElement.length > 0) {
                    let cellText = tdElement.text();
                    if (cellText === null) {
                        console.error("No text in cell:", tdElement);
                    } else {
                        cellText = cellText.toLowerCase();
                        if (!cellText.includes(searchVal)) {
                            // El texto de búsqueda no coincide, oculta la fila
                            $(index).hide();
                        } else {
                            // El texto de búsqueda coincide, muestra la fila
                            $(index).show();
                        }
                    }
                }
            })
        })
        dataList.forEach((item, index) => {
            // Create new card element
            let cardElement = `<div id="itemCard-${index}" class="card margin-bottom-half file">
                                <div class="card-content card-content-padding">
                                    <div class="row">
                                        <div class="col align-self-center ">
                                             <p class="small margin-bottom-half"><div class="fw-medium mr-4">Evento: ${item.event.name}</div>
                                            </p>
                                            <p>
                                                Fecha de evento: ${item.event.date} ${item.event.start_time} 
                                            </p>
                                            <p class="title-team">
                                                ${item.event.is_group ? 'Equipo: ' : 'Participante: '} ${item.name}
                                            </p>
<!--                                            <p>-->
<!--                                                Participantes: -->
<!--                                            </p>-->
                                            <p>
<!--                                                ${item.description}-->
                                                <span class="badge text-limit ${(item.payment === 'Pago pendiente' || item.payment === undefined) ? 'color-red' : 'color-green'}">${item.payment ?? 'Pago pendiente'}</span>
                                            </p>
                                        </div>
                                        <div class="col-auto">
                                        </div>
                                    </div>
                                </div>
                            </div>`;
            $(sIdComponentData).append(cardElement);
            $(`#itemCard-${index}`).click(() => {
                // if (item.payment === 'Pago pendiente') {
                loadSpinner()
                app.views.main.router.navigate(`/inscription-view/${item.id}/`);
                // }
            })
        });
    }
    const fetchData = () => {
        createComponentSkeleton()
        app.request({
            url: `${getRouteApi()}/congress/v1/api/participant/inscription-participants`,
            method: 'GET',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                dataList = JSON.parse(data).data
                createComponent()
            },
            error: (xhr, status) => {
                console.log('Error: ' + status);
            }
        });
    }
    const fetchRow = (inscriptionId) => {
        app.request({
            url: `${getRouteApi()}/congress/v1/api/participant/inscription-participants/${inscriptionId}`,
            method: 'GET',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                const inscription = JSON.parse(data).data
                console.log(dataList)
                renderDetail(inscription, inscriptionId)
                closeSpinner()
            },
            error: (xhr, status) => {
                console.log('Error: ' + status);
            }
        });
    }
    const confirmPayment = (inscriptionId) => {
        app.request({
            url: `${getRouteApi()}/congress/v1/api/payment`,
            method: 'PUT',
            data: {
                inscriptionId
            },
            contentType: 'application/json',
            success: (data, status, xhr) => {
                console.log(JSON.parse(data))
                app.dialog.alert(JSON.parse(data).message, 'Éxito');
                $(`#${idComponentDetail}-confirmed`).hide()
                app.views.main.router.navigate(`/inscription/`);
            },
            error: (xhr, status) => {
                catchLogicEvent(xhr, status);
                //app.dialog.alert(JSON.parse(xhr.responseText).errors.general, 'Error');
                console.log('Error: ' + status);
            }
        });
    }

    const catchLogicEvent = (xhr, status) => {
        if (status >= 400 && status <= 499) {
            const response = JSON.parse(xhr.responseText).errors;
            if (response.length > 0) {
                app.dialog.alert(response[0], 'Error');
            } else if (typeof response === 'object') {
                console.log('La respuesta es un objeto');
                console.log(response)
                const firstErrorValue = Object.values(response)[0];
                console.log(firstErrorValue);
                app.dialog.alert(firstErrorValue, 'Error');
            }
        }
    }

    const submitAction = (inscriptionId, elementId = 'photo-input', image) => {
        const photoInput = document.getElementById(`${idComponentDetail}-${elementId}`);
        if (photoInput.files.length > 0) {
            const photoFile = photoInput.files[0];
            submitPayment(inscriptionId, photoFile, image);
        } else {
            console.log("No photo was taken");
        }
    }
    const renderDetail = (inscription, inscriptionId) => {
        let listParticipants = `
        <ul> 
        `
        inscription.participants.forEach(participant => {
            listParticipants += `<li><span class="${participant.attendance ? 'badge text-limit color-orange' : 'badge text-limit'}">${participant.name}</span></li>`
        })
        listParticipants += '</ul>'
        let html = `
            
          <div class="card no-safe-areas" style="margin-bottom: 15px">
<!--            <div class="card-header">Card</div>-->
            <div class="card-content card-content-padding">
                <p>Evento: ${inscription.event.name}</p>
                <p>Fecha: ${inscription.event.date} ${inscription.event.start_time}</p>
                <p>Lugar: ${inscription.event.location.name} </p>
                <p>Tipo: ${inscription.event.category_description} </p>
                <p>Modalidad: ${inscription.event.is_group ? 'Grupal' : 'Individual'} </p>
                <hr>
                <p>Nombre de equipo: ${inscription.name}</p>                
                <p>Cantidad de participantes: ${inscription.participants.length}</p>     
                 ${listParticipants}
                 <p>Nota: Los participantes que asistieron al evento tienen el nombre resaltado</p>
                <div class="text-align-center">
                    <span class="badge p-9 ${(inscription.payment === 'Pago pendiente' || inscription.payment === undefined) ? 'color-red' : 'color-green'}">${inscription.payment ?? 'Pago pendiente'}</span>
                </div>
            </div>
          </div>
        
          <div class="inscription-section-actions">
            <div class="inscription-section-child-icons" id="${idComponentDetail}-camera">
                <i class="bi bi-camera"></i>
            </div>
            <div class="inscription-section-child-icons" id="${idComponentDetail}-file">
                <i class="bi bi-file-earmark-arrow-up" ></i>
            </div>
          </div>
          
        <input type="file" accept="image/*" capture="camera" id="${idComponentDetail}-photo-input" style="display: none">
        <input type="file" accept="image/*"  id="${idComponentDetail}-file-input" style="display: none">
        `
        if (!inscription.uri_image) {
            html += `
        
        <div class="inscription-image-preview-container">
            <div id="${idComponentDetail}-preview-action"></div>
                <div id="${idComponentDetail}-preview"></div>
            
        </div>
<!--        <button class="button button-fill button-raised color-theme" id="${idComponentDetail}-camera">Abrir Cámara</button>-->
        <button class="button button-fill button-raised color-theme" id="${idComponentDetail}-payment">Subir evidencia</button>
    `
            const sPayment = `#${idComponentDetail}-payment`
            $(`#${idComponentDetail}`).append(html);
            $(sPayment).hide();

            $('.inscription-image-preview-container').hide()
        } else {
            const role = JSON.parse(localStorage.getItem('user')).rol
            const htmlPA = (inscription.payment !== 'Pago válido' && role !== 'ADMIN') ?
                `<div id="${idComponentDetail}-preview-action"></div>` : ''

            html += `
            <div class="inscription-image-preview-container">
                ${htmlPA}
                <div id="load-image">
                    <img src="${inscription.uri_image}" alt="" class="inscription-image-preview">
                </div>
            </div>
            `
            if (role === 'ADMIN' && inscription.payment !== 'Pago válido') {
                html += `<button class="button button-fill button-raised color-theme mb-10"
                        id="${idComponentDetail}-confirmed">Confirmar pago</button>`
            }
            $(`#${idComponentDetail}`).append(html);
            $('.inscription-section-actions').hide()
            const sPreviewAction = `#${idComponentDetail}-preview-action`
            $(sPreviewAction).append(`
                                <i class="bi bi-x"></i>
                            `)
            $(`#${idComponentDetail}-confirmed`).on('click', () => {
                console.log('aceptar pago')
                confirmPayment(inscriptionId)
            })
            $(sPreviewAction).on('click', () => {
                // $('.inscription-image-preview').empty()
                $('#load-image').empty()
                const iipc = '.inscription-image-preview-container'
                $(iipc).empty()
                $(iipc).append(`
                    <div id="${idComponentDetail}-preview-action"></div>
                    <div id="${idComponentDetail}-preview"></div>
                    `
                )
                $(iipc).hide()
                $('.inscription-section-actions').show()
            })
        }

        $(document).off('click', `#${idComponentDetail}-camera`).on('click', `#${idComponentDetail}-camera`, () => {
            console.log('click - 1')
            let photoInput = document.getElementById(`${idComponentDetail}-photo-input`);

            photoInput.onchange = (e) => handleOnChange(e, inscriptionId, photoInput, 'photo-input')

            photoInput.click();
        })
        $(document).on('click', `#${idComponentDetail}-file`, () => {
            let photoInput = document.getElementById(`${idComponentDetail}-file-input`);

            photoInput.onchange = (e) => handleOnChange(e, inscriptionId, photoInput, 'file-input')

            photoInput.click();
        })

    }
    const handleOnChange = (e, inscriptionId, photoInput, elementId) => {
        let file = e.target.files[0];
        let preview = document.getElementById(`${idComponentDetail}-preview`);
        let reader = new FileReader();

        reader.onloadend = function () {
            if (file) { // aquí verificamos si se seleccionó un archivo
                const iipc = '.inscription-image-preview-container'
                $(iipc).show()
                let image = new Image();
                // image.style.filter = "grayscale(100%)";
                image.classList.add('skeleton-effect-image');
                image.src = reader.result;
                image.classList.add('inscription-image-preview');
                // image.width = 100; // actualizar al tamaño de vista previa requerida
                preview.innerHTML = '';
                preview.append(image);
                $('.inscription-section-actions').hide()
                submitAction(inscriptionId, elementId, image)

                const sPreviewAction = `#${idComponentDetail}-preview-action`
                $(sPreviewAction).append(`
                                <i class="bi bi-x"></i>
                            `)
                $(sPreviewAction).on('click', () => {
                    // $('.inscription-image-preview').empty()
                    $('#load-image').empty()
                    const iipc = '.inscription-image-preview-container'
                    $(iipc).empty()
                    $(iipc).append(`
                                <div id="${idComponentDetail}-preview-action"></div>
                                <div id="${idComponentDetail}-preview"></div>`
                    )
                    $(iipc).hide()
                    $('.inscription-section-actions').show()
                })
                photoInput.value = '';
            }
        }

        if (file) {
            reader.readAsDataURL(file);
        }
    }
    const detail = (inscription) => {
        const inscriptionId = app.views.main.router.currentRoute.params.inscriptionId
        const eventId = app.views.main.router.currentRoute.params.eventId
        fetchRow(inscriptionId)
        $('#button-back').on('click', () => {
            if (eventId) {
                app.views.main.router.navigate(`/event/assistance-participant/${eventId}/payment/`);
            } else {
                app.views.main.router.navigate(`/inscription/`);
            }
        })

    }
    const submitPayment = (inscriptionId, photoFile, image) => {
        let formData = new FormData();
        formData.append('file', photoFile);

        // Muestra el cargador
        try {
            app.request({
                url: `${getRouteApi()}/congress/v1/api/inscription/upload-voucher/${inscriptionId}`,
                method: 'PUT',
                data: formData,
                xhrFields: {
                    onprogress: function (e) {
                        if (e.lengthComputable) {
                            console.log(e.loaded / e.total * 100 + '%');
                        }
                    }
                },
                success: (data, status, xhr) => {
                    image.classList.remove('skeleton-effect-image')
                    app.dialog.alert(JSON.parse(data).message, 'Éxito');
                    // app.views.main.router.navigate(`/inscription/`);
                },
                error: (xhr, status) => {
                    alert(xhr)
                    image.classList.remove('skeleton-effect-image')
                    console.log('Error: ' + status);
                }
            });
        } catch (error) {
            console.log(`Ha ocurrido un error durante el envío del pago: ${error}`);
        }
    }
    const init = () => {
        if (ApiUtils.isAdmin() || ApiUtils.isSupportSales() || ApiUtils.isPARTICIPANTS()) {
            fetchData()
        } else {
            const sIdComponent = `#${idComponent}`
            $(sIdComponent).empty()
            $(sIdComponent).append(`<div style="margin-bottom: 10px"><h3 class="text-align-center">No tiene permiso</h3> </div>`)
        }
    }
    return {
        init,
        detail
    }
}