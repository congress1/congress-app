import ListGenerator from "../listGenerator.js";

export default function iParticipant(app) {
    const loadSpinner = () => {
        console.log("inicio spinner inscription/index")
        app.dialog.preloader('cargando...');
    }
    const init = () => {
        const type = app.views.main.router.currentRoute.params.type
        let attributes = []
        if (type === 'free') {

            attributes = [
                {
                    label: 'Nombre',
                    value: 'name',
                    type: 'Text',
                    isShow: true,
                    required: true
                },
                {
                    label: 'Universidad',
                    value: 'university',
                    values: {
                        valueList: type === 'free' ? 'university' : 'university.name',
                    },
                    type: 'Text',
                    isShow: true,
                    required: true
                },
            ]
        } else {

            attributes = [
                {
                    label: 'Nombre',
                    value: 'name',
                    type: 'Text',
                    isShow: true,
                    required: true
                },
                {
                    label: 'Universidad',
                    value: 'university',
                    values: {
                        valueList: type === 'free' ? 'university' : 'university.name',
                    },
                    type: 'Text',
                    isShow: true,
                    required: true
                },
                {
                    label: 'Pago',
                    value: 'payment',
                    type: 'Enum',
                    options: [
                        {
                            label: 'Pago pendiente',
                            value: 'Pago pendiente',
                            color: 'color-red'
                        },
                        {
                            label: 'Pago válido',
                            value: 'Pago válido',
                            color: 'color-green'
                        }
                    ],
                    isShow: true,
                    required: true
                },
                {
                    label: 'Asistencias',
                    value: 'count_attendance',
                    type: 'Text',
                    isShow: true,
                    required: true
                },
            ]
        }

        const eventId = app.views.main.router.currentRoute.params.eventId
        const randomNumber = Math.floor(Math.random() * 1000000);
        const entity = type === 'free' ? 'event/list-participants-event/' + eventId : 'event/list-inscription-event/' + eventId
        console.log('eventId :: ', eventId)
        console.log('entity :: ', entity)
        app.on('eventEdit' + entity + randomNumber, (e) => {
            console.log('eventEdit', e)
            if(e) {
                loadSpinner()
                if (type === 'payment') {
                    app.views.main.router.navigate(`/inscription-view/${e}/${eventId}/`);
                }
            }
        })
        const gList = new ListGenerator(entity, attributes, 'participant-container', '', app, randomNumber, type === 'free' ? 'Lista de participantes' : 'Lista de inscritos', false, true)
        gList.isDisableEdit()
        gList.isDeleteInShow()
        if (type === 'free') {
            gList.setUrlDelete("inscription/attendance")
            gList.setId("id_enrollment")
        } else {
            gList.setUrlDelete("inscription/olimpianeic")
        }
        gList.initList()
    }
    return {
        init
    }
}