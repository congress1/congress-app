import FormGenerator from "../formGenerator.js";
import getRouteApi from "../api.js";

const $ = Dom7;

export default function eRegistration(app) {
    const registration = () => {
        const eventId = app.views.main.router.currentRoute.params.eventId
        console.log(app.views.main.router)
        // const dni = JSON.parse(localStorage.getItem('user')).datos.dni
        const dni = null
        console.log(dni)


        app.request({
            url: `${getRouteApi()}/congress/v1/api/event/${eventId}`,
            method: 'GET',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                const dataL = JSON.parse(data)
                const event = dataL.data
                let attributes = []
                if (event.is_group) {
                    attributes = [
                        {
                            label: 'Evento',
                            value: 'eventId',
                            type: 'Hidden',
                            default: eventId,
                            isShow: true,
                            required: true
                        },
                        {
                            label: 'Nombre',
                            value: 'name',
                            type: 'Text',
                            isShow: true,
                            required: true
                        },
                        {
                            label: 'Team',
                            value: 'participants',
                            values: {
                                valueList: 'condition.name',
                                valueEdit: 'condition.id'
                            },
                            type: 'TableList',
                            isMultiple: event.is_group ?? false,
                            maximum: event.maximum ?? 1,
                            minimum: event.minimum ?? 1,
                            uri: 'participant/university-participants',
                            // uri: 'participant/events-participants/'+dni,
                            isShow: true,
                            required: true
                        },
                    ]
                } else {
                    attributes = [
                        {
                            label: 'Evento',
                            value: 'eventId',
                            type: 'Hidden',
                            default: eventId,
                            isShow: true,
                            required: true
                        },
                        {
                            label: 'Team',
                            value: 'participants',
                            values: {
                                valueList: 'condition.name',
                                valueEdit: 'condition.id'
                            },
                            type: 'TableList',
                            isMultiple: event.is_group ?? false,
                            maximum: event.maximum ?? 1,
                            minimum: event.minimum ?? 1,
                            uri: 'participant/university-participants',
                            // uri: 'participant/events-participants/'+dni,
                            isShow: true,
                            required: true
                        },
                    ]
                }

                const htmlFinish = `
            <div class="row mt-10">
                <div class="col">
                    <h3>Registro finalizado</h3>
                    <p>No se olvide estar atento al día del evento</p>
                </div>
            </div>
`

                app.on('reloadList', (data) => {
                    console.log('redirigir a la pantalla anterior')
                    const sContentRegistration = $('#contentRegistration')
                    sContentRegistration.empty()
                    sContentRegistration.append(htmlFinish)
                });
                const randomNumber = Math.floor(Math.random() * 1000000);
                const formGenerator = new FormGenerator('inscription-enroll', attributes, 'containerRegistration', '', app, randomNumber)
                formGenerator.initForm()
                formGenerator.setUriCustomSubmit('inscription/enroll')
            },
            error: (xhr, status) => {
                this.app.dialog.alert(JSON.parse(xhr.responseText).errors[0], 'Error');
            }
        });
    }
    return {
        registration
    }
}