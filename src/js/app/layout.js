import {ApiUtils} from "../utils/apiUtils";

const $ = Dom7;

export default function layout(app) {
    let user = {}
    const navbarAdmin = (uri) => {
        const $generateNavbar = $('#generate-navbar');
        $generateNavbar.empty();

        const navItems = [
            {href: ["/home/"], icon: "bi bi-house-fill", text: "Inicio"},
            {
                href: [
                    "/event-academic/",
                    "/event-cultural-partner/"
                ], icon: "bi bi-calendar-week-fill", text: "Eventos"
            },
            {
                href: [
                    "/olimpianeic-academic/",
                    "/olimpianeic-cultural-partner/",
                    "/olimpianeic-deport/"
                ],
                icon: "bi bi-award-fill",
                text: "Olimpianeic"
            },
            {
                href: [
                    "/inscription/",
                ], icon: "bi bi-bookmark-check-fill", text: "Inscripciones"
            },
        ];

        let html = '<div class="container"><ul class="nav nav-pills nav-justified">';
        const halfLength = Math.ceil(navItems.length / 2);

        for (let i = 0; i < halfLength; i++) {
            const item = navItems[i];
            const isActive = item.href.includes(uri);
            html += `
            <li class="nav-item">
                <a class="nav-link ${isActive ? 'active' : ''}" href="${item.href[0]}">
                    <span>
                        <i class="nav-icon ${item.icon}"></i>
                        <span class="nav-text">${item.text}</span>
                    </span>
                </a>
            </li>`;
        }

        for (let i = halfLength; i < navItems.length; i++) {
            const item = navItems[i];
            const isActive = item.href.includes(uri);
            html += `
            <li class="nav-item">
                <a class="nav-link ${isActive ? 'active' : ''}" href="${item.href[0]}">
                    <span>
                        <i class="nav-icon ${item.icon}"></i>
                        <span class="nav-text">${item.text}</span>
                    </span>
                </a>
            </li>`;
        }

        html += '</ul></div>';

        $generateNavbar.append(html);
    }
    const navbarUser = (uri) => {
        const $generateNavbar = $('#generate-navbar');
        $generateNavbar.empty();

        const navItems = [
            {href: ["/home/"], icon: "bi bi-house-fill", text: "Inicio"},
            {
                href: [
                    "/event-academic/",
                    "/event-cultural-partner/"
                ], icon: "bi bi-calendar-week-fill", text: "Eventos"
            },
            {
                href: [
                    "/olimpianeic-academic/",
                    "/olimpianeic-cultural-partner/",
                    "/olimpianeic-deport/"
                ],
                icon: "bi bi-award-fill",
                text: "Olimpianeic"
            },
            {
                href: [
                    "/inscription/",
                ], icon: "bi bi-bookmark-check-fill", text: "Inscripciones"
            },
        ];

        let html = '<div class="container"><ul class="nav nav-pills nav-justified">';
        const halfLength = Math.ceil(navItems.length / 2);

        for (let i = 0; i < halfLength; i++) {
            const item = navItems[i];
            const isActive = item.href.includes(uri);
            html += `
            <li class="nav-item">
                <a class="nav-link ${isActive ? 'active' : ''}" href="${item.href[0]}">
                    <span>
                        <i class="nav-icon ${item.icon}"></i>
                        <span class="nav-text">${item.text}</span>
                    </span>
                </a>
            </li>`;
        }

        html += `
        <li class="nav-item centerbutton">
            <button type="button" class="nav-link popup-open" data-popup=".popup-menu">
                <span class="theme-radial-gradient">
                    <i class="bi bi-grid size-22"></i>
                </span>
            </button>
        </li>`;

        for (let i = halfLength; i < navItems.length; i++) {
            const item = navItems[i];
            const isActive = item.href.includes(uri);
            html += `
            <li class="nav-item">
                <a class="nav-link ${isActive ? 'active' : ''}" href="${item.href[0]}">
                    <span>
                        <i class="nav-icon ${item.icon}"></i>
                        <span class="nav-text">${item.text}</span>
                    </span>
                </a>
            </li>`;
        }

        html += '</ul></div>';

        $generateNavbar.append(html);

    }
    const generateMenuItem = (href, iconSvg, text, id = null) => {
        const idComponent = id ? `id="${id}"` : ''
        return `
        <li>
            <a href="${href}" class="panel-close" ${idComponent}>
                <div class="avatar avatar-40 rounded icon">
                    <i class="${iconSvg}"></i>
                </div>
                ${text}
            </a>
        </li>
    `
    };
    const optionsAdmin = () => {
        const $generateMenu = $('#generate-menu')
        $generateMenu.empty()

        const user = JSON.parse(localStorage.getItem('user'))
        const rol = user.rol
        //ADMIN, SUPPORT_SALES, SCANNERS, PARTICIPANT
        const menuItems = [
            {href: "/panel/sponsor/", icon: 'bi bi-amd', text: "Patrocinadores", roles: ["ADMIN"]},
            {href: "/panel/organizer/", icon: 'bi bi-people-fill', text: "Organizadores", roles: ["ADMIN"]},
            {href: "/panel/university/", icon: 'bi bi-bank', text: "Universidades", roles: ["ADMIN"]},
            {href: "/panel/participant/", icon: 'bi bi-backpack2-fill', text: "Participantes", roles: ["ADMIN"]},
            {href: "/panel/speaker/", icon: 'bi bi-file-earmark-person-fill', text: "Ponentes", roles: ["ADMIN"]},
            {href: "/panel/location/", icon: 'bi bi-buildings-fill', text: "Ubicaciones", roles: ["ADMIN"]},
            {href: "/panel/question/", icon: 'bi bi-buildings-fill', text: "Preguntas", roles: ["ADMIN"]},
            {href: "/panel/user/", icon: 'bi bi-person-fill', text: "Usuarios", roles: ["ADMIN"]},
            {href: "/statistics/", icon: 'bi-clipboard-data-fill', text: "Estadísticas", roles: ["ADMIN"]},
            {href: "/calendar/", icon: 'bi-calendar-day-fill', text: "Calendario", roles: ["ADMIN"]},
            {href: "/home-questions/", icon: 'bi-chat-left-text-fill', text: "FAQ", roles: ["ADMIN"]},
            {
                href: "/configuration/",
                icon: 'bi-gear-fill',
                text: "Configuración",
                roles: ["ADMIN", "SUPPORT_SALES", "SCANNERS"]
            },
            {
                href: "#",
                icon: 'bi-arrow-down-left-square-fill',
                text: "Salir",
                id: "logoutAction",
                roles: ["ADMIN", "SUPPORT_SALES", "SCANNERS"]
            }
        ];
        const html = `
            <div class="list links-list accordion-list no-margin-top">
                <ul>
                    ${menuItems.filter(item => item.roles ? item.roles.includes(rol) : false)
            .map(item => generateMenuItem(item.href, item.icon, item.text, item.id)).join('')}
                </ul>
            </div>
        `;
        $generateMenu.append(html)
    }
    const optionsUser = () => {
        const $generateMenu = $('#generate-menu')
        $generateMenu.empty()
        const html = `
        <div class="list links-list accordion-list no-margin-top">
            <ul>
                <li>
                    <a href="/configuration/" class="panel-close">
                        <div class="avatar avatar-40 rounded icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-gear-fill" viewBox="0 0 16 16">
                              <path d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872zM8 10.93a2.929 2.929 0 1 1 0-5.86 2.929 2.929 0 0 1 0 5.858z"/>
                            </svg>
                        </div> Configuración
                        <span class="badge color-orange margin-left-half">new</span>
                    </a>
                </li>                
                <li>
                    <a href="/calendar/" class="panel-close">
                        <div class="avatar avatar-40 rounded icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar-day-fill" viewBox="0 0 16 16">
                              <path d="M4 .5a.5.5 0 0 0-1 0V1H2a2 2 0 0 0-2 2v1h16V3a2 2 0 0 0-2-2h-1V.5a.5.5 0 0 0-1 0V1H4zM16 14a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V5h16zm-4.785-6.145a.428.428 0 1 0 0-.855.426.426 0 0 0-.43.43c0 .238.192.425.43.425m.336.563h-.672v4.105h.672zm-6.867 4.105v-2.3h2.261v-.61H4.684V7.801h2.464v-.61H4v5.332zm3.296 0h.676V9.98c0-.554.227-1.007.953-1.007.125 0 .258.004.329.015v-.613a2 2 0 0 0-.254-.02c-.582 0-.891.32-1.012.567h-.02v-.504H7.98z"/>
                            </svg>
                        </div>
                         Calendario
                    </a>
                </li>               
                <li>
                    <a href="/home-questions/" class="panel-close">
                        <div class="avatar avatar-40 rounded icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chat-left-text-fill" viewBox="0 0 16 16">
                              <path d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H4.414a1 1 0 0 0-.707.293L.854 15.146A.5.5 0 0 1 0 14.793zm3.5 1a.5.5 0 0 0 0 1h9a.5.5 0 0 0 0-1zm0 2.5a.5.5 0 0 0 0 1h9a.5.5 0 0 0 0-1zm0 2.5a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1z"/>
                            </svg>
                        </div>
                         FAQ
                    </a>
                </li>               
                <li>
                    <a href="/home-organizers/" class="panel-close">
                        <div class="avatar avatar-40 rounded icon">
                          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-people-fill" viewBox="0 0 16 16">
                             <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6m-5.784 6A2.24 2.24 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.3 6.3 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1zM4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5"/>
                           </svg>
                        </div>
                         Organizadores
                    </a>
                </li>
                <li>
                    <a class="panel-close" id="logoutAction">
                        <div class="avatar avatar-40 rounded icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-down-left-square-fill" viewBox="0 0 16 16">
                          <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2zm8.096-10.803L6 9.293V6.525a.5.5 0 0 0-1 0V10.5a.5.5 0 0 0 .5.5h3.975a.5.5 0 0 0 0-1H6.707l4.096-4.096a.5.5 0 1 0-.707-.707"/>
                        </svg>
                        </div> Salir
                    </a >
                </li>
            </ul>
        </div>
        `
        $generateMenu.append(html)

    }
    const generateComponentQR = () => {
        console.log('generateComponentQR')
        const $cQR = $('#generate-cQR')
        $cQR.empty()
        const idQrGenerate = "area-para-qr-" + Date.now() + Math.floor(Math.random() * 1000);
        const selectIdQrGenerate = $(`#${idQrGenerate}`)
        console.log('idQrGenerate', idQrGenerate)
        const html = `
        <div class="popup popup-menu elevation-2 popup-swipe-to-close bg-color-2 c-white">
           <div class="row h-100 justify-content-center align-item-center">
                <div class="col-100 align-self-center">
                  <h1 class="margin-bottom text-align-center"><span class=" fw-light">Muestra tu</span><br />QR</h1>
                  <div class="text-align-center">
                    <div class="qr-content">
                      <div id="${idQrGenerate}" class="qr-generate"></div>
                    </div>
                    <p class="small margin-bottom">Para poder ingresar a los distintos<br />eventos</p>
                  </div>
                </div>
              </div>
            </div>
        <div class="popup popup-init">
            <img src="../img/app/stages/Banner.png" alt="" style="width: 100%" id="banner">   
        </div>
        `
        $cQR.append(html)
        $('#banner').on('click', () => {

            const user = JSON.parse(localStorage.getItem('user'))
            window.open(user.urlForm, '_blank');
        })
        setTimeout(() => generateQR(idQrGenerate), 100)

    }

    const generateComponentBenefit = () => {
        console.log('generateComponentBenefit')
        const $cQR = $('#generate-cBenefit')
        $cQR.empty()
        const idBenefitGenerate = "area-para-benefit-" + Date.now() + Math.floor(Math.random() * 1000);
        console.log('idBenefitGenerate', idBenefitGenerate)
        const html = `
        <div class="popup popup-menu-benefit elevation-2 popup-swipe-to-close bg-color-2 c-white" id="modal-benefit">
           <div class="row h-100 justify-content-center align-item-center">
                <div class="col-100 align-self-center">
                  <h1 class="margin-bottom text-align-center"><span class=" fw-light">Beneficio</span></h1>
                  <div class="text-align-center">
                  <div class="row margin-vertical text-align-center" id="benefit-content">
                  </div>
                </div>
              </div>
            </div>
        `
        $cQR.append(html)

    }
    const generateQR = (id) => {
        console.log('generate QR');
        var dniUsuario = user.datos.dni; // Reemplaza esto con el DNI real del usuario
        console.log("dni user:: ", dniUsuario)
        // Comprueba si el elemento con el ID generado existe
        var $qrAreaElement = $(`#${id}`);

        if ($qrAreaElement.length) {
            console.log($qrAreaElement)
            const qrcode = new QRCode($qrAreaElement[0], {
                text: dniUsuario,
                width: 128,
                height: 128,
                colorDark: "#000000",
                colorLight: "#ffffff",
            });

            const user = JSON.parse(localStorage.getItem('user'))
            const role = user.rol
            if (role !== 'ADMIN') {
                closePopup()
            }
        } else {
            console.log("El elemento " + id + " no se encuentra en el documento.");
        }
    }
    const closePopup = () => {
        // $('.centerbutton .nav-link').on('click', function () {
        //     $(this).toggleClass('active')
        // })
        const swipeToClosePopup = app.popup.create({
            el: '.popup-swipe-to-close',
            swipeToClose: true,
        });
        swipeToClosePopup.on('close', function (popup) {
            $('.centerbutton .nav-link').removeClass('active')
        });
    }
    const generateOptions = (uri) => {
        user = JSON.parse(localStorage.getItem('user'))
        let stage = user?.datos?.stage ? `img/app/stages/${user.datos.stage}.png` : 'img/user1.jpg';
        console.log("stage current profile:: ", stage)
        $('#img_stage').attr('src', stage);
        const role = user.rol
        const sidebarUser = $("#sidebar-user")
        if (ApiUtils.rolesAdmins.includes(role)) {
            optionsAdmin()
            navbarAdmin(uri)
            sidebarUser.empty()
            const html = `
                <h5>${user.user}</h5>
                <p class="text-secondary-50 size-12">${role}</p>
            `
            sidebarUser.append(html)
        } else {
            generateComponentQR()
            generateComponentBenefit()
            optionsUser()
            navbarUser(uri)
            sidebarUser.empty()
            const html = `
                <h5>${user.user}</h5>
                <p class="text-secondary-50 size-12 text-uppercase">${user.datos.promotion}</p>
            `
            sidebarUser.append(html)
        }
    }
    return {
        generateOptions,
        closePopup
    }
}