import GenerateCrud from "./generateCrud.js";

export default function participant(app) {
    let entity = 'participant'
    let attribute = [
        {
            label: 'Nombre',
            value: 'name',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Correo Electrónico',
            value: 'email',
            values: {
                valueList: 'user.email',
                valueEdit: 'user.email'
            },
            type: 'Text',
            isShow: true,
            required: true
        },
        // {
        //     label: 'Departamento',
        //     value: 'department',
        //     type: 'Text',
        //     isShow: false,
        //     required: true
        // },
        // {
        //     label: 'Provincia',
        //     value: 'province',
        //     type: 'Text',
        //     isShow: true,
        //     required: true
        // },
        // {
        //     label: 'Distrito',
        //     value: 'district',
        //     type: 'Text',
        //     isShow: true,
        //     required: true
        // },
        {
            label: 'DNI',
            value: 'dni',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Celular',
            value: 'cellphone',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Código corporativo',
            value: 'corporativeCode',
            valueAttribute: 'corporative_code',
            values: {
                valueList: 'corporative_code',
                valueEdit: 'corporative_code'
            },
            type: 'Text',
            isShow: true,
            required: false
        },
        {
            label: 'Condición',
            value: 'conditionCode',
            values: {
                valueList: 'condition.name',
                valueEdit: 'condition.id'
            },
            type: 'Table',
            uri: 'setting/list-internal-code/TIPA000',
            isShow: true,
            required: true
        },
        {
            label: 'Promoción',
            value: 'promotionCode',
            values: {
                valueList: 'promotion.name',
                valueEdit: 'promotion.id'
            },
            type: 'Table',
            uri: 'setting/list-internal-code/TIPR000',
            isShow: true,
            required: true
        },
        {
            label: 'Universidad',
            value: 'universityId',
            values: {
                valueList: 'university.name',
                valueEdit: 'university.id'
            },
            type: 'Table',
            uri: 'university/list-all',
            isShow: true,
            required: true
        },
        {
            label: 'Tipo de etapas',
            value: 'stageCode',
            values: {
                valueList: 'stage.name',
                valueEdit: 'stage.id'
            },
            type: 'Table',
            uri: 'setting/list-internal-code/PAET000',
            isShow: true,
            required: true
        },
        {
            label: 'Es delegado',
            value: 'delegate',
            default: true,
            type: 'Boolean',
            isShow: false,
            required: false
        },
    ]
    const generateCrud = new GenerateCrud(entity, attribute, 'containerGCParticipant', app, 'Lista de participantes');

    const initGenerateCrud = () => {
        console.log('init form');
        return generateCrud.initGenerateCrud("popup-form", "Nuevo formulario")
    }
    const initForm = () => {
        console.log('init form');
        return generateCrud.initForm()
    }
    const generateList = () => {
        console.log('init list');
        return generateCrud.initList()
    }
    return {
        initGenerateCrud,
        initForm,
        generateList
    }


}