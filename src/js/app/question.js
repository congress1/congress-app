import GenerateCrud from "./generateCrud.js";

export default function question(app) {
    let entity = 'question'
    let attribute = [
        {
            label: 'Pregunta',
            value: 'question',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Respuesta',
            value: 'answer',
            type: 'Text',
            isShow: true,
            required: true
        },
        // {
        //     label: 'Descripción',
        //     value: 'description',
        //     type: 'Text',
        //     isShow: true,
        //     required: true
        // },
        // {
        //     label: 'Latitud',
        //     value: 'latitude',
        //     type: 'Text',
        //     isShow: false,
        //     required: true
        // },
        // {
        //     label: 'Longitud',
        //     value: 'longitude',
        //     type: 'Text',
        //     isShow: false,
        //     required: true
        // },
        // {
        //     label: 'locaUrlimage',
        //     value: 'urlImage',
        //     type: 'Hidden',
        //     default: 'default',
        //     isShow: false,
        //     required: false
        // }
    ]
    const generateCrud = new GenerateCrud(entity, attribute, 'containerGCQuestion', app, 'Lista de preguntas');

    const initGenerateCrud = () => {
        console.log('init form');
        return generateCrud.initGenerateCrud("popup-form", "Nuevo formulario")
    }
    const initForm = () => {
        console.log('init form');
        return generateCrud.initForm()
    }
    const generateList = () => {
        console.log('init list');
        return generateCrud.initList()
    }
    return {
        initGenerateCrud,
        initForm,
        generateList
    }


}