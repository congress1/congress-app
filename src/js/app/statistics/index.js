import getRouteApi from "./../api.js";
import {ApiUtils} from "../../utils/apiUtils";

const $ = Dom7;
export default function statistics(app) {

    const init = () => {
        console.log('statistics init')

        const replaceValuesCard1 = (response) => {

            const responseCard = response && response.length > 0 ? response[0] : null;
            const acronym = responseCard ? responseCard.acronym : "-";
            const countParticipant = responseCard ? responseCard.count_participant : "-";
            console.log("responseCard1:: ", responseCard);

            $("#uinscritos").text(acronym)
            $("#ninscritos").text(countParticipant)

        }

        const replaceValuesCard2 = (response) => {

            const responseCard = response && response.length > 0 ? response[0] : null;
            const name = responseCard ? responseCard.name : "-";
            console.log("bestEvent:: ", name);

            $("#emejor").text(name)

        }

        const replaceValuesCard3 = (response) => {

            const responseCard = response && response.length > 0 ? response[0] : null;
            const name = responseCard ? responseCard.name : "-";
            console.log("Olimpianeic:: ", name);

            $("#omejor").text(name)

        }

        const fetchEventsFreePayment =  (type) => {
            app.request({
                url: `${getRouteApi()}/congress/v1/api/statistics/event-count-participants/${type}`,
                method: 'GET',
                contentType: 'application/json',
                success: (data, status, xhr) => {
                    console.log(JSON.parse(data))
                    const dataEvent = JSON.parse(data).data
                    if(type === 'TICT001') {
                        eventAreaChart(dataEvent)
                        replaceValuesCard2(dataEvent)
                    } else {
                        olimpianeicDoughnutChart(dataEvent)
                        replaceValuesCard3(dataEvent)
                    }
                    ApiUtils.closeSpinner(app)
                },
                error: (xhr, status) => {
                    console.log('Error: ' + status);
                    if(type === 'TICT001') {
                        eventAreaChart([])
                        replaceValuesCard2([])
                    } else {
                        olimpianeicDoughnutChart([])
                        replaceValuesCard3([])
                    }
                    ApiUtils.catchLogicListGenerator(xhr, status, app)
                    ApiUtils.closeSpinner(app)
                }
            });
        }

        const fetchCountParticipants =  () => {
            app.request({
                url: `${getRouteApi()}/congress/v1/api/statistics/count-participants`,
                method: 'GET',
                contentType: 'application/json',
                success: (data, status, xhr) => {
                    console.log(JSON.parse(data))
                    const dataCount = JSON.parse(data).data
                    $("#cant_participant").text(dataCount)
                    ApiUtils.closeSpinner(app)
                },
                error: (xhr, status) => {
                    console.log('Error: ' + status);
                    ApiUtils.catchLogicListGenerator(xhr, status, app)
                    ApiUtils.closeSpinner(app)
                }
            });
        }

        const fetchDataRankings =  () => {
            app.request({
                url: `${getRouteApi()}/congress/v1/api/event-result/university-rankings`,
                method: 'GET',
                contentType: 'application/json',
                success: (data, status, xhr) => {
                    console.log(JSON.parse(data))
                    const dataStatistics = JSON.parse(data).data
                    firstChart(dataStatistics);
                    ApiUtils.closeSpinner(app)
                },
                error: (xhr, status) => {
                    console.log('Error: ' + status);
                    firstChart([])
                    ApiUtils.catchLogicListGenerator(xhr, status, app)
                    ApiUtils.closeSpinner(app)
                }
            });
        }

        const fetchInscritosPorUniversidad =  () => {
            app.request({
                url: `${getRouteApi()}/congress/v1/api/statistics/university-count-participants`,
                method: 'GET',
                contentType: 'application/json',
                success: (data, status, xhr) => {
                    console.log(JSON.parse(data))
                    const dataInscritos = JSON.parse(data).data
                    firstPolarAreaChart(dataInscritos)
                    replaceValuesCard1(dataInscritos)
                    ApiUtils.closeSpinner(app)
                },
                error: (xhr, status) => {
                    console.log('Error: ' + status);
                    firstPolarAreaChart([])
                    ApiUtils.catchLogicListGenerator(xhr, status, app)
                    ApiUtils.closeSpinner(app)
                }
            });
        }


        const firstChart =  (response) => {
            const topTenUniversities = response.slice(0, 6);
            const univNames = topTenUniversities.map(university => university.acronym);
            const scores = topTenUniversities.map(university => university.score);
            const barchart = $('#areachart')[0].getContext('2d');

            const colors = [
                '#36A2EB', '#FFCD56', '#4BC0C0', '#FF6384', '#FF3333',
                '#FF9F40', '#36A2EB', '#8C33FF', '#A1FF33', '#33FF8C'
            ];

            if (topTenUniversities.length === 0) {
                new Chart(barchart, {
                    type: 'bar',
                    data: {
                        labels: [''],
                        datasets: [{
                            label: 'No hay información',
                            data: [0],
                            backgroundColor: 'rgba(0, 0, 0, 0)',
                            borderWidth: 0,
                        }]
                    },
                    options: {
                        plugins: {
                            legend: {
                                display: false,
                            },
                            title: {
                                display: true,
                                text: 'No hay información',
                                font: {
                                    size: 18
                                },
                                padding: {
                                    top: 10,
                                    bottom: 30
                                }
                            }
                        },
                        scales: {
                            y: {
                                ticks: {
                                    display: true
                                },
                                drawBorder: false,
                                label: true,
                                grid: {
                                    display: true,
                                    zeroLineColor: 'rgba(219,219,219,0.3)',
                                    drawBorder: false,
                                    lineWidth: 1,
                                    zeroLineWidth: 2
                                }
                            },
                            x: {
                                grid: {
                                    display: false,
                                },
                                ticks: {
                                    display: false
                                }
                            }
                        }
                    }
                });
            } else {
                const mybarcartConfig = {
                    type: 'bar',
                    data: {
                        labels: univNames,
                        datasets: [{
                            label: '# puntaje',
                            data: scores,
                            backgroundColor: colors,
                            borderWidth: 0,
                            borderRadius: 10,
                            borderSkipped: false,
                            barThickness: 10,
                        }]
                    },
                    options: {
                        plugins: {
                            legend: {
                                display: false,
                            },
                            title: {
                                display: false,
                                text: 'Ranking de Universidades',
                                font: {
                                    size: 18
                                },
                                padding: {
                                    top: 10,
                                    bottom: 30
                                }
                            }
                        },
                        scales: {
                            y: {
                                ticks: {
                                    display: true
                                },
                                drawBorder: false,
                                label: true,
                                grid: {
                                    display: true,
                                    zeroLineColor: 'rgba(219,219,219,0.3)',
                                    drawBorder: false,
                                    lineWidth: 1,
                                    zeroLineWidth: 2
                                }
                            },
                            x: {
                                grid: {
                                    display: false,
                                },
                                barPercentage: 0.5,
                                categoryPercentage: 0.5
                            }
                        }
                    }
                };
                let myBarChart = new Chart(barchart, mybarcartConfig);
            }
        }

        const firstPolarAreaChart = (response) => {
            const topTenUniversities = response.slice(0, 6);
            const univNames = topTenUniversities.map(university => university.acronym);
            const scores = topTenUniversities.map(university => university.count_participant);
            const polarChartContext = $('#polarareachart')[0].getContext('2d');

            const colors = [
                '#FF6384', '#4BC0C0', '#FFCD56', '#36A2EB', '#A1FF33',
                '#FF8C33', '#36A2EB', '#8C33FF', '#FF5733', '#33FF8C'
            ];

            if (topTenUniversities.length === 0) {
                // Dibujar un gráfico vacío con un mensaje
                new Chart(polarChartContext, {
                    type: 'polarArea',
                    data: {
                        labels: [''],
                        datasets: [{
                            label: 'No hay información',
                            data: [0],
                            backgroundColor: ['rgba(0, 0, 0, 0)'], // Transparente
                            borderWidth: 0,
                        }]
                    },
                    options: {
                        plugins: {
                            legend: {
                                display: false,
                            },
                            title: {
                                display: true,
                                text: 'No hay información',
                                font: {
                                    size: 18
                                },
                                padding: {
                                    top: 10,
                                    bottom: 30
                                }
                            }
                        }
                    }
                });
            } else {
                const polarAreaChartConfig = {
                    type: 'polarArea',
                    data: {
                        labels: univNames,
                        datasets: [{
                            label: '# puntaje',
                            data: scores,
                            backgroundColor: colors,
                            borderWidth: 1,
                        }]
                    },
                    options: {
                        plugins: {
                            legend: {
                                display: true,
                            },
                            title: {
                                display: false,
                                text: 'Ranking de Universidades',
                                font: {
                                    size: 18
                                },
                                padding: {
                                    top: 10,
                                    bottom: 30
                                }
                            }
                        }
                    }
                };

                let myPolarAreaChart = new Chart(polarChartContext, polarAreaChartConfig);
            }
        }

        const eventAreaChart = (response) => {
            const topTenUniversities = response.slice(0, 6);
            const univNames = topTenUniversities.map(university => university.name);
            const scores = topTenUniversities.map(university => university.count_participant);
            const eventChartContext = $('#eventareachart')[0].getContext('2d');

            const colors = [
                'rgba(255, 99, 132, 0.2)', 'rgba(75, 192, 192, 0.2)', 'rgba(255, 205, 86, 0.2)',
                'rgba(201, 203, 207, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(255, 159, 64, 0.2)',
                'rgba(153, 102, 255, 0.2)', 'rgba(255, 206, 86, 0.2)', 'rgba(75, 192, 192, 0.2)',
                'rgba(54, 162, 235, 0.2)'
            ];

            const borderColors = [
                'rgb(255, 99, 132)', 'rgb(75, 192, 192)', 'rgb(255, 205, 86)', 'rgb(201, 203, 207)',
                'rgb(54, 162, 235)', 'rgb(255, 159, 64)', 'rgb(153, 102, 255)', 'rgb(255, 206, 86)',
                'rgb(75, 192, 192)', 'rgb(54, 162, 235)'
            ];

            if (topTenUniversities.length === 0) {
                new Chart(eventChartContext, {
                    type: 'bar',
                    data: {
                        labels: [''],
                        datasets: [{
                            label: 'No hay información',
                            data: [0],
                            backgroundColor: ['rgba(0, 0, 0, 0)'],
                            borderWidth: 0,
                        }]
                    },
                    options: {
                        plugins: {
                            legend: {
                                display: false,
                            },
                            title: {
                                display: true,
                                text: 'No hay información',
                                font: {
                                    size: 18
                                },
                                padding: {
                                    top: 10,
                                    bottom: 30
                                }
                            }
                        }
                    }
                });
            } else {
                const eventAreaChartConfig = {
                    type: 'bar',
                    data: {
                        labels: univNames,
                        datasets: [{
                            label: '# puntaje',
                            data: scores,
                            backgroundColor: colors,
                            borderColor: borderColors,
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            y: {
                                beginAtZero: true
                            }
                        },
                        plugins: {
                            legend: {
                                display: false,
                            },
                            title: {
                                display: false,
                                text: 'Ranking de Universidades',
                                font: {
                                    size: 18
                                },
                                padding: {
                                    top: 10,
                                    bottom: 30
                                }
                            }
                        }
                    }
                };

                let eventAreaChart = new Chart(eventChartContext, eventAreaChartConfig);
            }
        }

        const olimpianeicDoughnutChart = (response) => {
            const topTenUniversities = response.slice(0, 6);
            const univNames = topTenUniversities.map(university => university.name);
            const scores = topTenUniversities.map(university => university.count_participant);
            const doughnutChartCanvas = $('#olimpianeicchart')[0].getContext('2d');

            const colors = [
                'rgb(255, 99, 132)',
                'rgb(54, 162, 235)',
                'rgb(255, 205, 86)',
                'rgb(75, 192, 192)',
                'rgb(153, 102, 255)',
                'rgb(201, 203, 207)'
            ];

            if (topTenUniversities.length === 0) {
                new Chart(doughnutChartCanvas, {
                    type: 'doughnut',
                    data: {
                        labels: ['No hay información'],
                        datasets: [{
                            label: 'No hay información',
                            data: [1],
                            backgroundColor: ['rgba(0, 0, 0, 0.2)'],
                            borderColor: ['rgba(0, 0, 0, 0.1)'],
                            hoverOffset: 4
                        }]
                    },
                    options: {
                        responsive: true,
                        plugins: {
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: false,
                                text: 'University Scores'
                            }
                        }
                    }
                });
            } else {
                const data = {
                    labels: univNames,
                    datasets: [{
                        label: 'University Scores',
                        data: scores,
                        backgroundColor: colors,
                        hoverOffset: 4
                    }]
                };

                const config = {
                    type: 'doughnut',
                    data: data,
                    options: {
                        responsive: true,
                        plugins: {
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: false,
                                text: 'University Scores'
                            }
                        },
                    },
                };

                new Chart(doughnutChartCanvas, config);
            }
        }


        /* ----- */

        ApiUtils.loadSpinner(app)
        fetchDataRankings();
        fetchInscritosPorUniversidad();
        fetchCountParticipants();
        fetchEventsFreePayment('TICT001');
        fetchEventsFreePayment('TICT002');

        /* ----- */


    }
    return {
        init
    }
}