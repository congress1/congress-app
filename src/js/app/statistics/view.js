import getRouteApi from "../api";
import {ApiUtils} from "../../utils/apiUtils";

const $ = Dom7;

export default function statisticsView(app) {
    const viewInscriptionUniversity = () => {
        ApiUtils.loadSpinner(app)
        app.request({
            url: `${getRouteApi()}/congress/v1/api/statistics/university-count-participants`,
            method: 'GET',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                console.log(JSON.parse(data))
                const dataInscritos = JSON.parse(data).data
                renderInscriptionUniversity(dataInscritos)
                ApiUtils.closeSpinner(app)
            },
            error: (xhr, status) => {
                console.log('Error: ' + status)
                ApiUtils.catchLogicListGenerator(xhr, status, app)
                ApiUtils.closeSpinner(app)
            }
        });
    }
    const viewRankUniversity = () => {
        ApiUtils.loadSpinner(app)
        app.request({
            url: `${getRouteApi()}/congress/v1/api/event-result/university-rankings`,
            method: 'GET',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                console.log(JSON.parse(data))
                const dataStatistics = JSON.parse(data).data
                renderRankPosition(dataStatistics)
                ApiUtils.closeSpinner(app)
            },
            error: (xhr, status) => {
                console.log('Error: ' + status);
                ApiUtils.catchLogicListGenerator(xhr, status, app)
                ApiUtils.closeSpinner(app)
            }
        });
    }
    const viewEventWithMostIncome = () => {
        const select = $('#statistics-event-with-most-income-view-content')
        eventCountParticipant('TICT001', select)
    }
    const viewOlimpianeicWithMostIncome = () => {
        const select = $('#statistics-olimpianeic-with-most-income-view-content')
        eventCountParticipant('TICT002', select)
    }
    const eventCountParticipant = (type, selectID) => {
        ApiUtils.loadSpinner(app)
        app.request({
            url: `${getRouteApi()}/congress/v1/api/statistics/event-count-participants/${type}`,
            method: 'GET',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                console.log(JSON.parse(data))
                const dataEvent = JSON.parse(data).data
                renderEventCountParticipant(dataEvent, selectID, type)
                ApiUtils.closeSpinner(app)
            },
            error: (xhr, status) => {
                console.log('Error: ' + status);
                ApiUtils.catchLogicListGenerator(xhr, status, app)
                ApiUtils.closeSpinner(app)
            }
        });
    }
    const renderEventCountParticipant = (dataList, select, type) => {
        dataList.forEach((item, index) => {
            // Create new card element
            let cardElement = `<div id="itemCard-${index}" class="card margin-bottom-half file">
                                <div class="card-content card-content-padding">
                                    <div class="row">
                                        <div class="col align-self-center ">
                                             <p class="small margin-bottom-half"><div class="fw-medium mr-4">${type === 'TICT001' ? 'Evento' : 'Olimpianeic'}: ${item.name}</div>
                                            </p>
                                            <p>
                                                Participantes: ${item.count_participant} 
                                            </p>
                                        </div>
                                        <div class="col-auto">
                                        </div>
                                    </div>
                                </div>
                            </div>`;
            $(select).append(cardElement);
        });
    }
    const renderInscriptionUniversity = (dataList) => {
        const select = $('#statistics-inscription-university-view-select')
        dataList.forEach((item, index) => {
            // Create new card element
            let cardElement = `<div id="itemCard-${index}" class="card margin-bottom-half file">
                                <div class="card-content card-content-padding">
                                    <div class="row">
                                        <div class="col align-self-center ">
                                             <p class="small margin-bottom-half"><div class="fw-medium mr-4">Universidad: ${item.name}</div>
                                            </p>
                                            <p>
                                                Participantes: ${item.count_participant} 
                                            </p>
                                        </div>
                                        <div class="col-auto">
                                        </div>
                                    </div>
                                </div>
                            </div>`;
            $(select).append(cardElement);
        });
    }
    const renderRankPosition = (dataList) => {
        const select = $('#statistics-rank-university-view-content')
        dataList.forEach((item, index) => {
            // Create new card element
            let cardElement = `<div id="itemCard-${index}" class="card margin-bottom-half file">
                                <div class="card-content card-content-padding">
                                    <div class="row">
                                        <div class="col-auto align-self-center">
                                        <p>#${item.position} </p>
                                        </div>
                                        <div class="col align-self-center">
                                             <p class="small"><div class="fw-medium mr-4">Universidad: ${item.univ_name}</div>
                                            </p>
                                        </div>
                                        <div class="col-auto align-self-center" style="align-self: center">
                                        <span class="badge color-green">${item.score} Ptos</span>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
            $(select).append(cardElement);
        });
    }
    const renderEventWithMostIncome = (dataList) => {
        const select = $('#statistics-event-with-most-income-view-content')
        dataList.forEach((item, index) => {
            // Create new card element
            let cardElement = `<div id="itemCard-${index}" class="card margin-bottom-half file">
                                <div class="card-content card-content-padding">
                                    <div class="row">
                                        <div class="col-auto align-self-center">
                                        <p>#${item.position} </p>
                                        </div>
                                        <div class="col align-self-center">
                                             <p class="small"><div class="fw-medium mr-4">Universidad: ${item.univ_name}</div>
                                            </p>
                                        </div>
                                        <div class="col-auto align-self-center" style="align-self: center">
                                        <span class="badge color-green">${item.score} Ptos</span>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
            $(select).append(cardElement);
        });
    }
    return {
        viewInscriptionUniversity,
        viewRankUniversity,
        viewEventWithMostIncome,
        viewOlimpianeicWithMostIncome,
    }
}