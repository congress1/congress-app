import GenerateCrud from "./generateCrud.js";

export default function university(app) {
    let entity = 'university'
    let attribute = [
        {
            label: 'Nombre',
            value: 'name',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Acronimo',
            value: 'acronym',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Lugar',
            value: 'location',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Dni',
            value: 'dni',
            type: 'Number',
            isShow: true,
            required: true
        },
        {
            label: 'Genero',
            value: 'gender',
            type: 'Enum',
            options: ['Male', 'Female'], // opciones para el campo "Enum"
            isShow: false,
            required: true
        }
    ]
    const generateCrud = new GenerateCrud(entity, attribute, 'containerGC', app);

    const initGenerateCrud = () => {
        console.log('init form');
        return generateCrud.initGenerateCrud("popup-form", "Nuevo formulario")
    }
    const initForm = () => {
        console.log('init form');
        return generateCrud.initForm()
    }
    const generateList = () => {
        console.log('init list');
        return generateCrud.generateList()
    }
    return {
        initGenerateCrud,
        initForm,
        generateList
    }


}