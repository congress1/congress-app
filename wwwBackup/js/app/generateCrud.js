import FormGenerator from "./formGenerator.js";
import ListGenerator from "./listGenerator.js";

const $ = Dom7;

class GenerateCrud {
    popup;
    constructor(entity, attributes, containerId, app) {
        this.$ = $;
        this.entity = entity;
        this.randomNumber = Math.floor(Math.random() * 1000000);
        this.attributes = attributes;
        this.app = app;
        this.containerId = containerId;
        this.containerFormId = containerId + "Form";
        this.containerListId = containerId + "List"
        this.app.on('reloadList' + this.entity + this.randomNumber, (data) => {
            if (this.listGenerator) {
                this.listGenerator.clearAllData()
                this.listGenerator.fetchData()
            }

        });
        this.app.on('eventEdit' + this.entity + this.randomNumber, (id) => {
            this.eventEdit(id)
        });

    }
    eventEdit(id = null) {
        if (this.formGenerator) {
            this.formGenerator.setId(id)
            this.updateTitle()
        }
    }
    updateTitle() {
        this.$('#popup-title').text(this.resolveTitle());
    }
    resolveTitle() {
        if(this.formGenerator.id) {
            return 'Editar'
        }
        return 'Registrar'
    }
    closePopup() {
        this.eventEdit()
        this.app.popup.close(`.${this.popupClass}`);
    }
    initGenerateCrud(popupClass, title) {
        this.popupClass = popupClass
        this.listGenerator = new ListGenerator(this.entity, this.attributes, this.containerListId, this.popupClass, this.app, this.randomNumber)
        this.formGenerator = new FormGenerator(this.entity, this.attributes, this.containerFormId, this.popupClass, this.app, this.randomNumber)
        let initHtml = `
        <div id="${this.containerListId}"></div>
        <div class="popup ${popupClass}">
          <div class="page">
            <div class="navbar">
              <div class="navbar-bg"></div>
              <div class="navbar-inner">
                <div class="title" id="popup-title">${this.resolveTitle()}</div>
                <div class="right"><a href="#" class="link popup-close" id="popup-close">Cerrar</a></div>
              </div>
            </div>
            <div class="page-content">              
                <div id="${this.containerFormId}"></div>
            </div>
          </div>
        </div>
        `;
        var $contenedor = this.$(`#${this.containerId}`);
        $contenedor.append(initHtml);
        this.$('#popup-close').on('click', (e) => {
            this.closePopup()
        })

        // scroll(function() {
        //     console.log('scroll')
        //     //Comprueba si has desplazado a la parte inferior del contenedor
        //     if ($contenedor.scrollTop() + $contenedor.innerHeight() >= $contenedor[0].scrollHeight) {
        //         //Aquí debes poner tu código para obtener más datos de API
        //         console.log("Has alcanzado el final del contenedor!");
        //
        //         // Evita llamada múltiples veces a la api
        //         $contenedor.unbind('scroll');
        //
        //         // Tu Funcion de llamada a api, despues de el resultado, añade '.scroll' nuevamente
        //         // loadMoreData();
        //     }
        // });
    }

    initForm() {
        this.formGenerator.initForm();
    }

    initList() {
        this.listGenerator.initList();
    }
}

export default GenerateCrud;