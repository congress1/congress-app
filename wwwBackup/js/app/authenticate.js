import getRouteApi from "./api.js";

const $ = Dom7;
// var jwt = require('jsonwebtoken');
export default function authenticate(app) {
    let loadingLogin = false
    const isLoggedIn = () => {
        return localStorage.getItem('token');
    }
    const redirectToLogin = () => {
        if (isLoggedIn()) {
            console.log('not redirectToLogin')
            app.views.main.router.navigate('/home/', {clearPreviousHistory: true});
        } else {
            console.log('redirectToLogin')
            app.views.main.router.navigate('/login/', {clearPreviousHistory: true});
        }
    }
    const decodeJwt = (token) => {
        var base64Url = token.split('.')[1]; // Tomar sólo el payload
        var base64 = base64Url.replace('-', '+').replace('_', '/'); // Reemplazar caracteres no válidos
        return JSON.parse(window.atob(base64)); // Convertir de Base64 a string y parsear a JSON
    }
    const login = () => {
        console.log('go login')
        const {isValid, formLogin} = logicForm()
        if (!isValid) {
            return
        }

        loadingLogin = true
        $('#loadingLogin').show();
        $('#buttonLogin').attr('disabled', true);
        console.log('formLogin', formLogin)
        app.request({
            url: `${getRouteApi()}/congress/v1/api/auth/login`,
            method: 'POST',
            contentType: 'application/json',
            data: formLogin,
            success: function (data, status, xhr) {
                localStorage.setItem("token", JSON.parse(data).data)
                // Decodificar el token
                var decodedToken = decodeJwt(data);

                console.log('decodedToken', decodedToken);
                localStorage.setItem("user", JSON.stringify(decodedToken))
                loadingLogin = false
                $('#buttonLogin').attr('disabled', false);
                $('#loadingLogin').hide();
                app.views.main.router.navigate('/home/', {clearPreviousHistory: true});
            },
            error: function (xhr, status) {
                if (status === 503) {
                    app.views.main.router.navigate(`/confirmCode/${formLogin.username}/`, {clearPreviousHistory: true});
                } else {
                    if (status === 401) {
                        console.log('Error: ' + status);
                        console.log(JSON.parse(xhr.responseText).errors)
                    }
                }
                const response = JSON.parse(xhr.responseText).errors;
                app.dialog.alert(response[0], 'Error');
                $('#buttonLogin').removeAttr('disabled');
                $('#loadingLogin').hide();
            }
        });

        // setTimeout(function () {
        //     loadingLogin = false
        //     $('#buttonLogin').attr('disabled', false);
        //     $('#loadingLogin').hide();
        //     app.views.main.router.navigate('/home/');
        // }, 1000);

    }
    const logicForm = () => {

        var isValid = true;

        // Validación para el campo de usuario
        var usuario = $("#login-email").val();
        if (usuario === "") {
            $("#login-email").addClass('is-invalid');
            $("#login-email-error").show();
            $("#login-email-error").text('Por favor, ingresa tu usuario.');
            isValid = false;
        } else {
            $("#login-email").removeClass('is-invalid');
            $("#login-email-error").text('');
            $("#login-email-error").hide();
        }

        // Validación para el campo de contraseña
        var password = $("#login-password").val();
        if (password.length <= 4) {
            $("#login-password").addClass('is-invalid');
            $("#login-password-error").show();
            $("#login-password-error").text('La contraseña debe tener al menos 8 caracteres.');
            isValid = false;
        } else {
            $("#login-password").removeClass('is-invalid');
            $("#login-password-error").text('');
            $("#login-password-error").hide();
        }

        // Bloquea la sumisión si el formulario no es válido
        const formLogin = {
            "username": usuario, "password": password
        }
        return {isValid, formLogin}
    }
    return {
        redirectToLogin,
        login,
        loadingLogin,
        logicForm
    }
}