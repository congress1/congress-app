import GenerateCrud from "./generateCrud.js";

export default function speaker(app) {
    let entity = 'speaker'
    let attribute = [
        {
            label: 'Nombres',
            value: 'name',
            type: 'Text',
            isShow: true,
            required: true
        },
        // {
        //     label: 'Apellido',
        //     value: 'lastName',
        //     values: {
        //         valueList: 'last_name',
        //         valueEdit: 'last_name'
        //     },
        //     type: 'Text',
        //     isShow: true,
        //     required: true
        // },
        {
            label: 'Link Redes sociales',
            value: 'urlSocialNetwork',
            values: {
                valueList: 'url_social_network',
                valueEdit: 'url_social_network'
            },
            type: 'Text',
            isShow: false,
            required: true
        },
        {
            label: 'Descripción',
            value: 'description',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Nacionalidad',
            value: 'nationality',
            type: 'Text',
            isShow: true,
            required: true
        },
    ]
    const generateCrud = new GenerateCrud(entity, attribute, 'containerGCSpeaker', app);

    const initGenerateCrud = () => {
        console.log('init form');
        return generateCrud.initGenerateCrud("popup-form", "Nuevo formulario")
    }
    const initForm = () => {
        console.log('init form');
        return generateCrud.initForm()
    }
    const generateList = () => {
        console.log('init list');
        return generateCrud.initList()
    }
    return {
        initGenerateCrud,
        initForm,
        generateList
    }


}