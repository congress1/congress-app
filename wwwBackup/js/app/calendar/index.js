import EventGenerator from "./EventGenerator.js";
import getRouteApi from "../api.js";

const $ = Dom7;
export default function eCalendar(app) {
    let navbarId = null
    let navbarDate = null
    const loadSpinner = () => {
        console.log("inicio spinner formGenerator")
        app.dialog.preloader('cargando...');
    }

    const closeSpinner = () => {
        console.log("fin spinner formGenerator")
        app.dialog.close();
    }
    const daysRender = (days) => {
        const sCalendarNavbarDays = $('#calendar-navbar-days')
        sCalendarNavbarDays.empty()
        let html = '<div class="segmented segmented-strong">'
        days.forEach((day, index) => {
            if (!navbarDate) {
                navbarDate = day.fecha
            }
            if (day.fecha === navbarDate) {
                html += `<button class="button button-active" id="navbar-day-${index}">${day.label.charAt(0)}</button>`
                getCalendarDetail(navbarDate, day)
            } else {
                html += `<button class="button c-white" id="navbar-day-${index}">${day.label.charAt(0)}</button>`
            }
        })
        html += '<span class="segmented-highlight"></span></div>'

        sCalendarNavbarDays.append(html)

        days.forEach((day, index) => {
            $(`#navbar-day-${index}`).on('click', () => {
                console.log('click ', navbarDate)
                navbarDate = day.fecha
                daysRender(days)
                // getCalendarDetail(navbarDate, days)


                // console.log(day.events)
            })
        })
    }
    const getCalendarDetail = (navbarDate, day) => {
        loadSpinner()
        app.request({
            url: `${getRouteApi()}/congress/v1/api/event/calendar-detail?date=${navbarDate}`,
            method: 'GET',
            contentType: 'application/json',
            success: function (data, status, xhr) {
                const eventsDay = JSON.parse(data).data
                console.log(eventsDay)
                generate(eventsDay, day)
                closeSpinner()
            },
            error: function (xhr, status) {
                console.log('Error: ' + status);
                console.log(JSON.parse(xhr.responseText).errors)
                // const response = JSON.parse(xhr.responseText).errors;
                // app.dialog.alert(response[0], 'Error');
                closeSpinner()
            }
        });
    }
    const generate = (events, day) => {
        const dayTitle = day.label + ': ' + day.fecha
        console.log(day)
        const generator = new EventGenerator(events, 'calendar', dayTitle, app);
        generator.generar();
    }
    const navbarRender = (navbarList) => {
        const sCalendarNavbar = $('#calendar-navbar')
        sCalendarNavbar.empty()
        let html = '<div class="segmented segmented-strong">'
        navbarList.forEach((navbar, index) => {
            if (!navbarId) {
                navbarId = navbar.label
            }
            if (navbar.label === navbarId) {
                html += `<button class="button button-active" id="navbar-${index}">${navbar.label}</button>`
                console.log(navbar.dias)
                navbarDate = null
                daysRender(navbar.dias)
            } else {
                html += `<button class="button c-white" id="navbar-${index}">${navbar.label}</button>`
            }
        })
        html += '<span class="segmented-highlight"></span></div>'

        sCalendarNavbar.append(html)

        navbarList.forEach((navbar, index) => {
            $(`#navbar-${index}`).on('click', () => {
                console.log('semana -> ', navbar.label)
                navbarId = navbar.label
                navbarRender(navbarList)
            })
        })
    }
    const getCalendarHead = () => {

        app.request({
            url: `${getRouteApi()}/congress/v1/api/event/calendar-head`,
            method: 'GET',
            contentType: 'application/json',
            success: function (data, status, xhr) {
                const events = JSON.parse(data).data
                console.log(events)
                navbarRender(events)
                closeSpinner()
            },
            error: function (xhr, status) {
                console.log('Error: ' + status);
                console.log(JSON.parse(xhr.responseText).errors)
                const response = JSON.parse(xhr.responseText).errors;
                app.dialog.alert(response[0], 'Error');
                closeSpinner()
            }
        });
    }
    const navbarAction = () => {
        loadSpinner()
        getCalendarHead()
        // let navbarList = [
        //     {
        //         "label": "04-14 de Julio de 2024",
        //         "dias": [
        //             {
        //                 "label": "L",
        //                 "description": "",
        //                 "fecha": "2024-09-05",
        //                 "events": [
        //                     {
        //                         title: 'event1',
        //                         start: '12:30',
        //                         end: '14:00',
        //                         color: '#ffff00'
        //                     },
        //                     {
        //                         title: 'event2',
        //                         start: '11:30',
        //                         end: '15:00',
        //                         color: '#00ff00'
        //                     },
        //                     {
        //                         title: 'event3',
        //                         start: '03:30',
        //                         end: '04:00',
        //                         color: '#af1c2c'
        //                     },
        //                     {
        //                         title: 'event5',
        //                         start: '12:00',
        //                         end: '14:00',
        //                         color: '#00ff00'
        //                     },
        //                 ]
        //             },
        //             {
        //                 "label": "M",
        //                 "fecha": "2024-09-06",
        //                 "events": [
        //                     {
        //                         title: 'event1',
        //                         start: '10:30',
        //                         end: '12:00',
        //                         color: '#ffff00'
        //                     },
        //                     {
        //                         title: 'event2',
        //                         start: '13:30',
        //                         end: '16:30',
        //                         color: '#1d37a6'
        //                     },
        //                 ]
        //             }
        //         ]
        //     },
        //     {
        //         "label": "17-21 de Julio de 2024",
        //         "dias": [
        //             {
        //                 "label": "L",
        //                 "fecha": "2024-09-07",
        //                 "events": [
        //                     {
        //                         title: 'event1',
        //                         start: '10:30',
        //                         end: '12:00',
        //                         color: '#ffff00'
        //                     },
        //                     {
        //                         title: 'event2',
        //                         start: '13:30',
        //                         end: '16:30',
        //                         color: '#1d37a6'
        //                     },
        //                 ]
        //             },
        //             {
        //                 "label": "M",
        //                 "fecha": "2024-09-08",
        //                 "events": [
        //                     {
        //                         title: 'event1',
        //                         start: '10:30',
        //                         end: '12:00',
        //                         color: '#ffff00'
        //                     },
        //                     {
        //                         title: 'event2',
        //                         start: '13:30',
        //                         end: '16:30',
        //                         color: '#1d37a6'
        //                     },
        //                 ]
        //             }
        //         ]
        //     }
        // ]
        // navbarList.forEach(navbar => {
        //     navbar.active = navbar.label === navbarId
        // })
    }
    const init = () => {
        console.log('eCalendar init')
        // const events = [
        //     {
        //         title  : 'event1',
        //         start  : '12:30',
        //         end    : '14:00',
        //         color  : '#ffff00'
        //     },
        //     {
        //         title  : 'event2',
        //         start  : '11:30',
        //         end    : '15:00',
        //         color  : '#00ff00'
        //     },
        //     {
        //         title  : 'event3',
        //         start  : '03:30',
        //         end    : '04:00',
        //         color  : '#af1c2c'
        //     },
        //     {
        //         title  : 'event5',
        //         start  : '12:00',
        //         end    : '14:00',
        //         color  : '#00ff00'
        //     },
        // ];
        // const generator = new EventGenerator(events, 'calendar');
        // generator.generar();
        navbarAction()


    }
    return {
        init
    }
}

