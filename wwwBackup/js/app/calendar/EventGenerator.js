import getRouteApi from "../api.js";

const $ = Dom7;

export default class EventGenerator {
    idContentMap = 'map-container-e'

    constructor(events, idApp, day, app) {
        this.app = app
        this.day = day
        this.events = events;
        console.log('this.events', events)
        this.idApp = idApp;
        this.idComponent = 'container-detail'
        this.popupClass = this.idComponent + '-popup'
        this.idContentPopup = this.idComponent + '-content'
        this.idContentMap = 'event-' + this.idContentMap
        this.createModal('DETALLE DE EVENTO')
    }

    createModal(title) {
        const html = `
        <div class="popup ${this.popupClass}">
          <div class="page">
            <div class="navbar">
              <div class="navbar-bg"></div>
              <div class="navbar-inner">
                <div class="title" id="popup-title">${title}</div>
                <div class="right"><a href="#" class="link" id="popup-close">Cerrar</a></div>
              </div>
            </div>
            <div class="page-content">              
                <div id="${this.idContentPopup}"></div>
            </div>
          </div>
        </div>
        `
        $(`#${this.idComponent}`).append(html);

        $(`#popup-close`).click(() => {
            console.log('cerrar')
            this.app.popup.close(`.${this.popupClass}`)
        })
    }
    to12HourFormat(time24) {
        const [hours, minutes] = time24.split(':');
        const hours12 = (hours % 12) || 12;
        const period = hours >= 12 ? 'pm' : 'am';
        return hours12 + ':' + minutes + period;
    }

    generarHorasDelDia() {
        const horas = [];
        for (let i = 0; i <= 23; i++) {
            for (let minuto of ['00', '30']) {
                const hora = {
                    value: (i < 10 ? "0" + i : i.toString()) + ':' + minuto,
                    display: this.to12HourFormat((i < 10 ? "0" + i : i.toString()) + ':' + minuto)
                };
                horas.push(hora);
            }
        }
        return horas;
    }

    getEventsForHour(hour) {
        return this.events.filter(event => hour.value >= event.start && hour.value < event.end);
    }

    loadSpinner() {
        console.log("inicio spinner gEvent")
        this.app.dialog.preloader('cargando...');
    }

    closeSpinner() {
        console.log("fin spinner gEvent")
        this.app.dialog.close();
    }

    getEventStatus(date, end_date, start_time, end_time) {

        let current = new Date();

        let endTime = new Date(`${end_date}T${end_time}`);
        let startTime = new Date(`${date}T${start_time}`);
        // console.log('eval -> ', startTime, current, endTime)
        if (startTime <= current && current <= endTime) {
            return 'Evento iniciado';
        } else {
            if (current >= endTime) {
                return 'Evento finalizado';
            } else {
                return 'Evento Programado'
            }
        }
    }

    initMap = (lat, lng) => {
        console.log('initMap')
        const location = {lat, lng};
        const cMap = $(`#${this.idContentMap}`)
        let map = new google.maps.Map(cMap[cMap.length - 1], {
            zoom: 15,
            center: location
        });
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            title: 'Click to zoom'
        });

        map.addListener('center_changed', function () {
            // Después de 3 segundos se moverá de nuevo  al marcador
            window.setTimeout(function () {
                map.panTo(marker.getPosition());
            }, 3000);
        });

        marker.addListener('click', function () {
            window.location.href = 'https://www.google.com/maps/search/?api=1&query=' + location.lat + ',' + location.lng;
        });
    }

    createContentPopup = (item) => {
        console.log("item value desde method create :: ", item)
        const cContentPopup = $(`#${this.idContentPopup}`)
        const user = JSON.parse(localStorage.getItem('user'))
        cContentPopup.empty();
        const dataTypeEventGroup = `
                <div class="col-50 text-align-center margin-bottom">
                    <p class="small ">
                        <p class="fw-medium">Mínimo de participantes:</p>
                        <span class="text-muted">${item.minimum}</span>
                    </p>
                    </div>
                <div class="col-50 text-align-center margin-bottom">
                    <p class="small ">
                        <p class="fw-medium">Máximo de participantes:</p>
                        <span class="text-muted">${item.maximum}</span>
                    </p>
                </div>
                <div class="col-50 text-align-center margin-bottom">
                    <p class="small ">
                        <p class="fw-medium">Cupos disponibles:</p>
                        <span class="text-muted">${item.available_seats}</span>
                    </p>
                </div>
        `;
        const dataEventPayment = `
            ${(item.is_group ?? false) === true ? dataTypeEventGroup : ''}
            <div class="col-${(item.is_group ?? false) === true ? '50' : '100'} text-align-center margin-bottom">    
                <p class="small ">
                    <p class="fw-medium">Tipo de evento:</p>
                    <span class="text-muted">${(item.is_group ?? false) === true ? 'Grupal' : 'Individual'}</span>
                </p> 
            </div>
            `;
        const speakerContent = `
                        <p class="small">
                            <p class="fw-medium">Ponente:</p>
                            <span class="text-muted">${item?.speaker?.name}</span>
                        </p>
            `;
        let html = `
            <div>
            <div>
                <h3 class="text-align-center">${item.name}</h3>
            </div>            
            <div class="content-popup-academic ${user.rol === 'ADMIN' ? 'fixed-footer-event-160' : 'fixed-footer-event'}">
                <p class="small ">
                    <p class="fw-medium">Descripción:</p>
                    <span class="text-muted">${item.description}</span>
                </p>
                <p class="small " style="display: ${this.hasRegistration ? 'block' : 'none'};">
                    <p class="fw-medium" style="display: ${this.hasRegistration ? 'block' : 'none'};">Bases:</p>
                    <a href="${item.url_base}" class="external" style="display: ${this.hasRegistration ? 'block' : 'none'};">Clic para ver las bases</a>
                </p> 
                ${item?.speaker?.name ? speakerContent : ''}
                <p class="small ">
                    <p class="fw-medium">Lugar:</p>
                    <span class="text-muted">${item.location.name}</span>
                    <div id="${this.idContentMap}" style="width: 100%; height: 400px;"></div>
                </p>
                <div class="row margin-bottom">
                    <div class="col-50 text-align-center margin-bottom">
                         <p>
                            ${this.getEventStatus(item.date, item.end_date, item.start_time, item.end_time) === 'Evento finalizado' ?
            '<span class="badge color-red">' + this.getEventStatus(item.date, item.end_date, item.start_time, item.end_time) + '</span>'
            : '<span class="badge color-green">' + this.getEventStatus(item.date, item.end_date, item.start_time, item.end_time) + '</span>'}
                         </p>
                    </div>
                    <div class="col-50 text-align-center margin-bottom">
                         <p>
                            <span class="badge color-deeporange">Asistentes: ${item.number_attendees}</span>
                         </p>
                    </div>
                    <div class="col-50 text-align-center margin-bottom">
                        <p class="small ">
                            <p class="fw-medium">Fecha Inicio:</p>
                            <span class="text-muted">${item.date}</span>
                        </p>
                    </div>
                    <div class="col-50 text-align-center margin-bottom">
                        <p class="small ">
                            <p class="fw-medium">Fecha Fin:</p>
                            <span class="text-muted">${item.end_date}</span>
                        </p>
                    </div>
                    <div class="col-50 text-align-center margin-bottom">
                        <p class="small ">
                            <p class="fw-medium">Hora Inicio:</p>
                            <span class="text-muted">${item.start_time}</span>
                        </p>
                    </div> 
                    <div class="col-50 text-align-center margin-bottom">
                         <p class="small ">
                            <p class="fw-medium">Hora Fin:</p>
                            <span class="text-muted">${item.end_time}</span>
                        </p>
                    </div> 
                    ${this.hasRegistration ? dataEventPayment : ''}           
                </div>
            </div>
        `


        if (user.rol === 'ADMIN') {
            if (this.getEventStatus(item.date, item.end_date, item.start_time, item.end_time) !== 'Evento finalizado') {
                html += `
            <div class="event-detail-footer">
                <!--<button class="button button-fill button-raised color-theme" id="${this.linkAssistance}">Asistentes</button>-->
                 <div class="row justify-content-center">
                    <div class="col-90 medium-50 large-40 text-align-center margin-top-auto">
                    <a id="${this.linkAssistance}" class="button button-fill button-large button-raised color-theme">Asistentes</a>
                </div> 
            </div> 
                <!--<button class="button button-fill button-raised color-theme mt-2" id="${this.linkAssistance}-edit">Editar</button>-->
                <div class="col-90 margin-top-auto margin-left-auto margin-right-auto padding-vertical">
                    <div class="row ">
                      <div class="col-50 text-align-right">
                        <a id="${this.linkAssistance}-edit" class="button button-large button-raised button-fill color-theme">Editar</a>
                      </div>
                      <div class="col-50">
                        <a id="${this.linkAssistance}-remove" class="button button-large button-raised button-fill color-theme-red">Eliminar</a>
                      </div>
                    </div>
                </div>
              
            </div>`;
            } else {
                html += `
            <div class="row justify-content-center event-detail-footer">
                <!--<button class="button button-fill button-raised color-theme" id="${this.linkAssistance}-summary">Resumen de evento</button>-->
                <div class="col-90 medium-50 large-40 text-align-center margin-top-auto padding-vertical">
                    <a id="${this.linkAssistance}-summary" class="button button-fill button-large button-raised color-theme">Resumen de evento</a>
                </div> 
            </div>`;
            }
        } else if (this.hasRegistration) {
            if (user.datos.edecan && this.getEventStatus(item.date, item.end_date, item.start_time, item.end_time) !== 'Evento finalizado') {
                html += `
                            <!--<div class="row justify-content-center event-detail-footer">
                                    <button class="button button-fill button-raised color-theme" id="${this.linkRegistration}">
                                        Inscribirse
                                    </button>
                            </div> -->
                    <div class="row justify-content-center event-detail-footer">
                        <div class="col-90 medium-50 large-40 text-align-center margin-top-auto padding-vertical">
                            <a id="${this.linkRegistration}" class="button button-fill button-large button-raised color-theme">Inscribirse</a>
                        </div> 
                    </div>
                    `;
            } else if (this.getEventStatus(item.date, item.end_date, item.start_time, item.end_time) !== 'Evento finalizado') {
                html += `
            <div class="event-detail-footer">
                <!--<button class="button button-fill button-raised color-theme" id="${this.linkAssistance}-summary">Resumen de evento</button>-->
                <div class="col-90 medium-50 large-40 text-align-center margin-top-auto padding-vertical">
                <a id="${this.linkAssistance}-summary" class="button button-fill button-large button-raised color-theme">Resumen de evento</a>
                </div> 
            </div>`;
            }
        }
        html += `</div>`;
        cContentPopup.append(html);
        this.initMap(parseFloat(item.location.latitude), parseFloat(item.location.longitude));

        $(`#${this.linkAssistance}`).click(() => {
            console.log('ir a evento')
            this.app.popup.close(`.${this.popupClass}`);
            const router = this.app.views.main.router;
            router.navigate(`/event/assistance/${item.id}/${this.hasRegistration ? 'payment' : 'free'}/`);
        })
        $(`#${this.linkAssistance}-edit`).click(() => {
            console.log('ir a editar :: ', item.id)
            this.app.popup.close(`.${this.popupClass}`);
            this.openFormulario(item.id)
        })
        $(`#${this.linkAssistance}-remove`).click(() => {
            console.log('eliminar :: ', item.id)
            this.app.dialog.confirm('¿Está seguro de eliminar este evento?', 'Mensaje', () => {
                this.deleteEvent(item.id);
            });
        })
        $(`#${this.linkAssistance}-summary`).click(() => {
            console.log('ir a resumen de evento')
            this.app.popup.close(`.${this.popupClass}`);
            const router = this.app.views.main.router;
            router.navigate(`/event/summary/${item.id}/${this.hasRegistration ? 'payment' : 'free'}/`);
        })

        if (this.hasRegistration) {
            $(`#${this.linkRegistration}`).click(() => {
                console.log('ir a registrarse')
                this.app.popup.close(`.${this.popupClass}`);
                const router = this.app.views.main.router;
                router.navigate(`/event/registration/${item.id}/`);
            })
        }
    }
    getDataEvent = (eventId) => {
        console.log("entrando a request")
        this.app.request({
            url: `${getRouteApi()}/congress/v1/api/event/${eventId}`,
            method: 'GET',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                const dataL = JSON.parse(data)
                console.log("data req: ", dataL)
                this.dataEvent = dataL.data
                this.createContentPopup(this.dataEvent)
                this.closeSpinner();
            },
            error: (xhr, status) => {
                console.log('Error: ' + status);
                this.catchLogicEvent(xhr, status);
                this.closeSpinner();
            }
        });
    }

    catchLogicEvent(xhr, status) {
        if (status >= 400 && status <= 499) {
            const response = JSON.parse(xhr.responseText).errors;
            if (response.length > 0) {
                this.app.dialog.alert(response[0], 'Error');
            } else if (typeof response === 'object') {
                console.log('La respuesta es un objeto');
                console.log(response)
                const firstErrorValue = Object.values(response)[0];
                console.log(firstErrorValue);
                this.app.dialog.alert(firstErrorValue, 'Error');
            }
        }
    }
    generar() {
        const idApp = $('#' + this.idApp);
        idApp.empty();
        const horas = this.generarHorasDelDia();
        const horasConEventos = horas.map(hora => ({...hora, events: this.getEventsForHour(hora)}));

        let maxEventosSimultaneos = Math.max(...horasConEventos.map(h => h.events.length));
        // aquí puedes continuar con el resto de tu lógica para generar tu tabla HTML
        let renderHours = ''

        for (let i = 0; i < horasConEventos.length; i++) {
            const hora = horasConEventos[i];

            let rowHtml = `
            <tr>
                <td class="time-column ${hora.display.includes(':00') ? '' : 'half-hour-mark'}"><div class="container-hour">${hora.display.includes(':00') ? hora.display : ''}</div></td>`;

            for (let j = 0; j < maxEventosSimultaneos; j++) {
                if (hora.events[j]) {
                    // An event for this slot

                    if (i === 0 || !horasConEventos[i - 1].events.includes(hora.events[j])) {
                        // This is the first slot of this event
                        const eventLength = horasConEventos.slice(i).filter(h => h.events.includes(hora.events[j])).length;
                        rowHtml += `<td rowspan="${eventLength}" class="event-column event-cell" style="background-color: ${hora.events[j].color};" 
                            id="calendar-detail-${hora.events[j].id}">${hora.events[j].type}: ${hora.events[j].title}</td>`;
                    }

                } else {
                    // No event for this slot, but slots to fill up to maxEventosSimultaneos
                    rowHtml += `<td class="event-column ${hora.display.includes(':00') ? '' : 'half-hour-mark'}"></td>`;
                }
            }

            rowHtml += '</tr>';

            renderHours += rowHtml;
        }

        let columnHeaders = `<td class="column text-center" colspan="${maxEventosSimultaneos}">${this.day}</td>`

        const html = `
        <table class="header table-calendar">
            <thead>
                <tr>
                    <td class="time-header column"></td>
                    ${columnHeaders}
                </tr>
            </thead>
        </table>
        <div class='container-with-scroll'>
              <table class="table-calendar">
                  ${renderHours}
              </table>
      </div
    `;

        idApp.append(html);
        this.events.forEach(event => {
            $(`#calendar-detail-${event.id}`).on("click", () => {
                console.log('click')
                this.app.popup.open(`.${this.popupClass}`);
                this.getDataEvent(event.id)
            })
        })
    }
}