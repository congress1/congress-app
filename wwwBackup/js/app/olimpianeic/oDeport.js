import GEvent from "../event/gEvent.js";

export default function oDeport(app) {
    let attributes = [
        {
            label: 'Nombre',
            value: 'name',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Descripción',
            value: 'description',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Hora de inicio (09:00)',
            value: 'startTime',
            type: 'Time',
            isShow: true,
            required: true
        },
        {
            label: 'Hora de fin (09:00)',
            value: 'endTime',
            type: 'Time',
            isShow: true,
            required: true
        },
        {
            label: 'Color Label',
            value: 'labelColor',
            default: '#FFA500',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'speakerId',
            value: 'speakerId',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'locationId',
            value: 'locationId',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'typeCode',
            value: 'typeCode',
            default: 'TIEV001',
            type: 'Hidden',
            isShow: true,
            required: true
        },
        {
            label: 'themeCode',
            value: 'themeCode',
            default: 'TIET002',
            type: 'Hidden',
            isShow: true,
            required: true
        },
        {
            label: 'categoryCode',
            value: 'categoryCode',
            default: 'TICT001',
            type: 'Hidden',
            isShow: true,
            required: true
        },
        {
            label: 'classCode',
            value: 'classCode',
            default: 'TICL001',
            type: 'Hidden',
            isShow: true,
            required: true
        },
    ]
    const gEvent = new GEvent(app, 'ol-deport', 'TICT002/TIEV003', attributes, true)
    const init = () => {
        gEvent.init()
    }
    return {
        init
    }
}