import GEvent from "../event/gEvent.js";

export default function oAcademic(app) {
    let attributes = [
        {
            label: 'Nombre',
            value: 'name',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Descripción',
            value: 'description',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Bases',
            value: 'urlBase',
            type: 'Text',
            values: {
                valueEdit: 'url_base'
            },
            isShow: true,
            required: true
        },
        {
            label: 'Fecha Inicio',
            value: 'date',
            type: 'Date',
            isShow: true,
            default: new Date(),
            required: true
        },
        {
            label: 'Fecha Fin',
            value: 'endDate',
            type: 'Date',
            values: {
                valueEdit: 'end_date'
            },
            isShow: true,
            default: new Date(),
            required: true
        },
        {
            label: 'Hora de inicio (09:00)',
            value: 'startTime',
            values: {
                valueEdit: 'start_time'
            },
            type: 'Time',
            isShow: true,
            required: true
        },
        {
            label: 'Hora de fin (09:00)',
            values: {
                valueEdit: 'end_time'
            },
            value: 'endTime',
            type: 'Time',
            isShow: true,
            required: true
        },
        {
            label: 'Color Label',
            value: 'labelColor',
            default: '#FFA500',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Es grupal',
            value: 'isGroup',
            values: {
                valueEdit: 'is_group'
            },
            default: false,
            type: 'Boolean',
            isShow: false,
            required: false
        },
        {
            label: 'Acumula horas',
            value: 'isAccumulation',
            values: {
                valueEdit: 'is_accumulation'
            },
            default: false,
            type: 'Boolean',
            isShow: false,
            required: false
        },
        {
            label: 'Mínimo',
            value: 'minimum',
            // default: 1,
            type: 'Number',
            conditionals: [
                {
                    value: 'isGroup',
                    conditional: 'equals',
                    equalTo: true,
                    type: 'show'
                }
            ],
            isShow: false,
            required: false
        },
        {
            label: 'Máximo',
            value: 'maximum',
            conditionals: [
                {
                    value: 'isGroup',
                    conditional: 'equals',
                    equalTo: true,
                    type: 'show'
                }
            ],
            // default: 0,
            type: 'Number',
            isShow: false,
            required: false
        },
        {
            label: 'Cupos',
            value: 'availableSeats',
            // default: 0,
            values: {
                valueEdit: 'available_seats'
            },
            type: 'Number',
            isShow: false,
            required: false
        },
        // {
        //     label: 'Ponente',
        //     value: 'speakerId',
        //     type: 'Table',
        //     uri: 'speaker/list-all',
        //     isShow: true,
        //     required: true
        // },
        {
            label: 'Locación',
            value: 'locationId',
            values: {
                valueEdit: 'location.id'
            },
            type: 'Table',
            uri: 'location/list-all',
            isShow: true,
            required: true
        },
        {
            label: 'typeCode',
            value: 'typeCode',
            values: {
                valueEdit: 'type_code'
            },
            default: 'TIEV001',
            type: 'Hidden',
            isShow: true,
            required: true
        },
        // {
        //     label: 'Eje tematico',
        //     value: 'themeCode',
        //     values: {
        //         valueList: 'type.name',
        //         valueEdit: 'type.id'
        //     },
        //     type: 'Table',
        //     keyTable: 'code',
        //     uri: 'setting/list-internal-code/TIET000?internalCode=true',
        //     isShow: false,
        //     required: true
        // },
        {
            label: 'categoryCode',
            value: 'categoryCode',
            default: 'TICT002',
            type: 'Hidden',
            isShow: true,
            required: true
        },
        // {
        //     label: 'Tipo de evento',
        //     value: 'classCode',
        //     values: {
        //         valueList: 'type.name',
        //         valueEdit: 'type.id'
        //     },
        //     keyTable: 'code',
        //     type: 'Table',
        //     uri: 'setting/list-internal-code/TICL000?internalCode=true',
        //     isShow: false,
        //     required: true
        // },
    ]
    const gEvent = new GEvent(app, 'ol-academic', 'TICT002/TIEV001', attributes, true)
    const init = () => {
        gEvent.init()
    }
    return {
        init
    }
}