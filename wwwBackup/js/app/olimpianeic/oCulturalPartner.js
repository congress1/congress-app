import GEvent from "../event/gEvent.js";

export default function oCulturalPartner(app) {
    let attributes = [
        {
            label: 'Nombre',
            value: 'name',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Descripción',
            value: 'description',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Bases',
            value: 'urlBase',
            type: 'Text',
            values: {
                valueEdit: 'url_base'
            },
            isShow: true,
            required: true
        },
        {
            label: 'Fecha Inicio',
            value: 'date',
            type: 'Date',
            isShow: true,
            default: new Date(),
            required: true
        },
        {
            label: 'Fecha Fin',
            value: 'endDate',
            type: 'Date',
            values: {
                valueEdit: 'end_date'
            },
            isShow: true,
            default: new Date(),
            required: true
        },
        {
            label: 'Hora de inicio (09:00)',
            value: 'startTime',
            values: {
                valueEdit: 'start_time'
            },
            type: 'Time',
            isShow: true,
            required: true
        },
        {
            label: 'Hora de fin (09:00)',
            values: {
                valueEdit: 'end_time'
            },
            value: 'endTime',
            type: 'Time',
            isShow: true,
            required: true
        },
        {
            label: 'Color Label',
            value: 'labelColor',
            default: '#FFA500',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Es grupal',
            value: 'isGroup',
            values: {
                valueEdit: 'is_group'
            },
            default: false,
            type: 'Boolean',
            isShow: false,
            required: false
        },
        {
            label: 'Acumula horas',
            value: 'isAccumulation',
            values: {
                valueEdit: 'is_accumulation'
            },
            default: false,
            type: 'Boolean',
            isShow: false,
            required: false
        },
        {
            label: 'Mínimo',
            value: 'minimum',
            // default: 1,
            conditionals: [
                {
                    value: 'isGroup',
                    conditional: 'equals',
                    equalTo: true,
                    type: 'show'
                }
            ],
            type: 'Number',
            isShow: false,
            required: false
        },
        {
            label: 'Máximo',
            value: 'maximum',
            default: 1,
            conditionals: [
                {
                    value: 'isGroup',
                    conditional: 'equals',
                    equalTo: true,
                    type: 'show'
                }
            ],
            type: 'Number',
            isShow: false,
            required: false
        },
        {
            label: 'Cupos',
            value: 'availableSeats',
            values: {
                valueEdit: 'available_seats'
            },
            default: 1,
            type: 'Number',
            isShow: false,
            required: false
        },
        // {
        //     label: 'Ponente',
        //     value: 'speakerId',
        //     type: 'Table',
        //     uri: 'speaker/list-all',
        //     isShow: true,
        //     required: true
        // },
        {
            label: 'Locación',
            value: 'locationId',
            values: {
                valueEdit: 'location.id'
            },
            type: 'Table',
            uri: 'location/list-all',
            isShow: true,
            required: true
        },
        {
            label: 'typeCode',
            value: 'typeCode',
            values: {
                valueEdit: 'type_code'
            },
            default: 'TIEV002',
            type: 'Hidden',
            isShow: true,
            required: true
        },
        {
            label: 'themeCode',
            value: 'themeCode',
            default: 'TIET007',
            type: 'Hidden',
            isShow: true,
            required: true
        },
        {
            label: 'categoryCode',
            value: 'categoryCode',
            default: 'TICT002',
            type: 'Hidden',
            isShow: true,
            required: true
        },
        {
            label: 'classCode',
            value: 'classCode',
            default: 'TICL001',
            type: 'Hidden',
            isShow: true,
            required: true
        },
    ]
    const gEvent = new GEvent(app, 'ol-cultural-partner', 'TICT002/TIEV002', attributes, true)
    const init = () => {
        gEvent.init()
    }
    return {
        init
    }
}