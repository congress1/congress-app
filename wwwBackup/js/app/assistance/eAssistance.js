import getRouteApi from "../api.js";
import FormGenerator from "../formGenerator.js";
import ListGenerator from "../listGenerator.js";

const $ = Dom7;

export default function eAssistance(app) {

    let loading = false

    const submit = (dni) => {
        const eventId = app.views.main.router.currentRoute.params.eventId
        const type = app.views.main.router.currentRoute.params.type
        inscriptionAttendance(eventId, dni, type)
    }
    const getParticipants = (eventId) => {
        const type = app.views.main.router.currentRoute.params.type
        console.log('type', type)
        console.log(`${getRouteApi()}/congress/v1/api/event/${type === 'free' ? 'list-participants-event' : 'list-inscription-event'}/${eventId}`)
        app.request({
            url: `${getRouteApi()}/congress/v1/api/event/${type === 'free' ? 'list-participants-event' : 'list-inscription-event'}/${eventId}`,
            method: 'GET',
            contentType: 'application/json',
            // headers: {
            //     'Authorization': 'Bearer ' + localStorage.getItem('token') // reemplaza token con tu token
            // },
            success: (data, status, xhr) => {
                const dataL = JSON.parse(data)
                console.log(dataL)
            },
            error: function (xhr, status) {
                console.log('Error: ' + status);
            }
        });
    }
    const inscriptionAttendance = (eventId, documentNumber, type) => {
        loading = true;
        app.request({
            url: `${getRouteApi()}/congress/v1/api/inscription/${type === 'payment' ? 'attendance_olimpianeic' : 'attendance'}`,
            method: `${type === 'payment' ? 'PUT' : 'POST'}`,
            contentType: 'application/json',
            data: {
                eventId,
                documentNumber
            },
            // headers: {
            //     'Authorization': 'Bearer ' + localStorage.getItem('token') // reemplaza token con tu token
            // },
            success: (data, status, xhr) => {
                app.dialog.alert(JSON.parse(data).message, 'Éxito', () => {
                    loading = false
                });
            },
            error: function (xhr, status) {
                const response = JSON.parse(xhr.responseText).errors;
                if (response.length > 0) {
                    app.dialog.alert(response[0], 'Error', () => {
                        loading = false
                    });
                }
            }
        });
    }
    const assistanceManual = () => {
        const eventId = app.views.main.router.currentRoute.params.eventId
        const type = app.views.main.router.currentRoute.params.type
        console.log(app.views.main.router)
        let attributes = [
            {
                label: 'Evento',
                value: 'eventId',
                type: 'Hidden',
                default: eventId,
                isShow: true,
                required: true
            },
            {
                label: 'DNI',
                value: 'documentNumber',
                type: 'Text',
                isShow: true,
                required: true
            },
        ]
        const formGenerator = new FormGenerator('inscription-attendance', attributes, 'containerManual', '', app)
        formGenerator.initForm()
        if (type === 'payment') {
            formGenerator.setUriCustomSubmit('inscription/attendance_olimpianeic')
            formGenerator.setMethodCreate('PUT')
        } else {
            formGenerator.setUriCustomSubmit('inscription/attendance')
        }
    }
    const initLinksAssistance = () => {
        const eventId = app.views.main.router.currentRoute.params.eventId
        const type = app.views.main.router.currentRoute.params.type
        $('#button-go-assistance').on('click', (e) => {
            console.log('go-assistance-manual clicked');
            app.views.main.router.navigate(`/event/assistance/${eventId}/${type}/`);
        })
        $('#button-go-assistance-manual').on('click', (e) => {
            console.log('go-assistance-manual clicked');

            app.views.main.router.navigate(`/event/assistance-manual/${eventId}/${type}/`);
        })
        $('#button-go-assistance-participant').on('click', (e) => {
            console.log('go-assistance-manual clicked');

            app.views.main.router.navigate(`/event/assistance-participant/${eventId}/${type}/`);
        })
        $('#button-back-qr').on('click', (e) => {
            console.log('back assistance')
            app.popup.close(`.scanner-qr`);
            destroyQrV2()
        })
        $('#button-go-qr').on('click', (e) => {
            console.log('button-go-qr');

            loadQrV2()
            // app.views.main.router.navigate(`/event/assistance/qr/${eventId}/`);
            app.popup.open(`.scanner-qr`);

        })
        if (type === 'free') {
            $('#assistance-btn').hide();
            $('#assistance-div').removeClass('margin-bottom margin-top-auto padding-vertical').addClass('justify-content-center');
            $('#first-assistance-div').removeClass('col').addClass('col-90');

        }
        $('#button-open-assistance').on('click', (e) => {
            console.log('button-go-assistance');
            // app.views.main.router.navigate(`/event/assistance/qr/${eventId}/`);
            assistances_list()
            app.popup.open(`.scanner-assistance`);

        })
    }

    const assistances_list = () => {
        let attributes = [
            {
                label: 'Nombre',
                value: 'name',
                type: 'Text',
                isShow: true,
                required: true
            },
            {
                label: 'Universidad',
                value: 'university',
                type: 'Text',
                isShow: true,
                required: true
            },
        ]

        const eventId = app.views.main.router.currentRoute.params.eventId
        console.log('eventId', eventId)
        const gList = new ListGenerator('event/list-participants-event/' + eventId, attributes, 'assistance-container', '', app)
        gList.noneMargin()
        gList.initList()
    }

    const viewParticipant = () => {
        const eventId = app.views.main.router.currentRoute.params.eventId
        // getParticipants(eventId)
    }
    const loadQr = () => {
        const eventId = app.views.main.router.currentRoute.params.eventId

        //todo falta agregar api para ver datos de evento
        app.request({
            url: `${getRouteApi()}/congress/v1/api/event/${eventId}`,
            method: 'GET',
            contentType: 'application/json',
            // headers: {
            //     'Authorization': 'Bearer ' + localStorage.getItem('token') // reemplaza token con tu token
            // },
            success: (data, status, xhr) => {
                const row = JSON.parse(data).data

                const html = `
                <div class="col">
                    <h3>${row.name}</h3>
                </div>
                `
                const sAssistanceTitle = $('#assistance-title')
                sAssistanceTitle.empty()
                sAssistanceTitle.append(html)
                assistanceManual()
            },
            error: function (xhr, status) {
                console.log('Error: ' + status);
            }
        });
    }
    const loadQrV1 = () => {
        function onScanSuccess(decodedText, decodedResult) {
            console.log(`Escaneado contenido: ${decodedText}`);
            if (!loading) {
                submit(decodedText)
            }
        }

        var html5QrcodeScanner = new Html5QrcodeScanner(
            "reader", {fps: 10, qrbox: 250});
        html5QrcodeScanner.render(onScanSuccess);
        $('#html5-qrcode-button-camera-start').text('iniciar el escáner')
    }

    function loadQrV2() {
        const video = document.getElementById('preview');
        const scannerFrame = document.getElementById('scanner-frame');
        const scannerLine = document.getElementById('scanner-line');

        let canvasElement = document.getElementById('scanner-canvas');

        // Si canvasElement no existe ya en el DOM, crear uno
        if (!canvasElement) {
            canvasElement = document.createElement('canvas');
            canvasElement.id = 'scanner-canvas';
            // document.body.appendChild(canvasElement);
        }

        const canvas = canvasElement.getContext('2d');
        var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
        navigator.mediaDevices.getUserMedia({
                video: {
                    facingMode: 'environment',
                    width: {ideal: height},
                    height: {ideal: width}
                }
            }
        ).then(function (stream) {
            video.srcObject = stream;
            video.setAttribute('playsinline', true);
            video.play();
            requestAnimationFrame(tick);
        });

        function tick() {
            if (video.readyState === video.HAVE_ENOUGH_DATA) {
                // scannerFrame.style.left = '50%';
                // scannerFrame.style.top = '50%';
                const videoPosition = video.getBoundingClientRect();
                scannerLine.style.left = videoPosition.left + 'px';
                scannerLine.style.top = videoPosition.top + 'px';

                canvasElement.hidden = false;
                canvasElement.height = video.videoHeight;
                canvasElement.width = video.videoWidth;
                canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);

                const imageData = canvas.getImageData(0, 0, canvasElement.width, canvasElement.height);
                const code = jsQR(imageData.data, imageData.width, imageData.height, {
                    inversionAttempts: 'dontInvert',
                });
                if (code && code.data.length === 8) {
                    console.log('QR Code: ', code.data);
                    if (!loading) {
                        submit(code.data)
                    }
                }
            }
            requestAnimationFrame(tick);
        }
    }

    function destroyQrV2() {
        const video = document.getElementById('preview');

        // Parar todas las pistas del stream de video
        if (video.srcObject) {
            const stream = video.srcObject;
            const tracks = stream.getTracks();

            tracks.forEach((track) => {
                track.stop();
            });

            // Limpiar la fuente del video
            video.srcObject = null;
        }

        // Eliminar el canvas
        let canvasElement = document.getElementById('scanner-canvas');
        if (canvasElement) {
            canvasElement.remove();
        }

        // Terminar el ciclo de la animación
        // guardando el id del requestAnimationFrame en una variable para luego cancelarlo
        // necesitarás modificar la función loadQrV2 a algo como esto:
        // window.scanAnim = requestAnimationFrame(tick);
        // Y aquí detenerlo:
        cancelAnimationFrame(window.scanAnim);
    }
    return {
        submit,
        assistanceManual,
        initLinksAssistance,
        viewParticipant,
        loadQr
    }
}