import getRouteApi from "../api.js";

const $ = Dom7;
export default function hSpeaker(app) {
    const init = () => {

        const loadSpinner = () => {
            console.log("inicio spinner home/Speaker")
            app.dialog.preloader('cargando...');
        }

        const closeSpinner = () => {
            console.log("fin spinner home/Speaker")
            app.dialog.close();
        }
        loadSpinner()
        const speakerId = app.views.main.router.currentRoute.params.speakerId
        console.log('init h speaker', speakerId)


        app.request({
            url: `${getRouteApi()}/congress/v1/api/speaker/${speakerId}`,
            method: 'GET',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                console.log(JSON.parse(data))
                const speaker = JSON.parse(data).data
                //
                $('#speaker-view').append(`
                <img src="${speaker.url_image}" alt="" style="width: 100%">
                <h2>${speaker.name}</h2>
                <p>Nacionalidad: ${speaker.nationality}</p>
                <p>${speaker.description}</p>
                
                `)

                closeSpinner()
            },
            error: (xhr, status) => {
                console.log('Error: ' + status);
            }
        });
    }
    return {
        init
    }
}