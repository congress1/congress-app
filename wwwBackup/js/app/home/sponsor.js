import getRouteApi from "../api.js";

const $ = Dom7;
export default function hSponsor(app) {
    const init = () => {

        const loadSpinner = () => {
            console.log("inicio spinner home/sponsor")
            app.dialog.preloader('cargando...');
        }

        const closeSpinner = () => {
            console.log("fin spinner home/sponsor")
            app.dialog.close();
        }
        loadSpinner()
        const sponsorId = app.views.main.router.currentRoute.params.sponsorId
        console.log('init h sponsor', sponsorId)

        app.request({
            url: `${getRouteApi()}/congress/v1/api/sponsor/${sponsorId}`,
            method: 'GET',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                console.log(JSON.parse(data))
                const sponsor = JSON.parse(data).data
                //
                $('#sponsor-view').append(`
                <img src="${sponsor.url_image}" alt="" style="width: 100%">
                <h2>${sponsor.name}</h2>
                <a class="social-networks-icon external" href="${sponsor.url_social_network}" style="font-size: 25px">
                  <i class="bi bi-facebook"></i>
                </a>
                <p>${sponsor.description}</p>
                
                `)

                closeSpinner()
            },
            error: (xhr, status) => {
                console.log('Error: ' + status);
            }
        });
    }
    return {
        init
    }
}