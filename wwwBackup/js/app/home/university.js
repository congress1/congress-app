import ListGenerator from "../listGenerator.js";

export default function hUniversity(app) {
    const init = () => {
        console.log('init hUniversity')
        let entity = 'university'
        let attributes = [
            {
                label: 'Nombre',
                value: 'name',
                type: 'Text',
                isShow: true,
                required: true
            },
            {
                label: 'Acronimo',
                value: 'acronym',
                type: 'Text',
                isShow: true,
                required: true
            },
            {
                label: 'Lugar',
                value: 'location',
                type: 'Text',
                isShow: false,
                required: true
            },
        ]

        const gList = new ListGenerator(entity, attributes, 'university-list', '', app)
        gList.noneMargin()
        gList.initList()
    }
    return {
        init
    }
}