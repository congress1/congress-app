import ListGenerator from "../listGenerator.js";

export default function hLocation(app) {
    const init = () => {
        console.log('init hLocation')
        let entity = 'location'
        let attributes = [
            {
                label: 'Nombre',
                value: 'name',
                type: 'Text',
                isShow: true,
                required: true
            },
            {
                label: 'Descripción',
                value: 'description',
                type: 'Text',
                isShow: true,
                required: true
            }
        ]

        const gList = new ListGenerator(entity, attributes, 'location-list', '', app)
        gList.noneMargin()
        gList.initList()
    }
    return {
        init
    }
}