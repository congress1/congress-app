import GenerateCrud from "./generateCrud.js";

export default function organizer(app) {
    let entity = 'organizer'
    let attribute = [
        {
            label: 'Nombre',
            value: 'name',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Celular',
            value: 'cellPhone',
            values: {
                valueList: 'cell_phone',
                valueEdit: 'cell_phone'
            },
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Número de whatsapp',
            value: 'whatsappNumber',
            values: {
                valueList: 'whatsapp_number',
                valueEdit: 'whatsapp_number'
            },
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Red social',
            value: 'urlSocialNetwork',
            values: {
                valueList: 'url_social_network',
                valueEdit: 'url_social_network'
            },
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Cargo',
            value: 'positionCode',
            values: {
                valueList: 'position.name',
                valueEdit: 'position.id'
            },
            type: 'Table',
            uri: 'setting/list-internal-code/TIOR000',
            isShow: true,
            required: true
        },
    ]
    const generateCrud = new GenerateCrud(entity, attribute, 'containerGCOrganizer', app);

    const initGenerateCrud = () => {
        console.log('init form');
        return generateCrud.initGenerateCrud("popup-form", "Nuevo formulario")
    }
    const initForm = () => {
        console.log('init form');
        return generateCrud.initForm()
    }
    const generateList = () => {
        console.log('init list');
        return generateCrud.initList()
    }
    return {
        initGenerateCrud,
        initForm,
        generateList
    }


}