import GenerateCrud from "./generateCrud.js";

export default function locations(app) {
    let entity = 'location'
    let attribute = [
        {
            label: 'Nombre',
            value: 'name',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Descripción',
            value: 'description',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Latitud',
            value: 'latitude',
            type: 'Text',
            isShow: false,
            required: true
        },
        {
            label: 'Longitud',
            value: 'longitude',
            type: 'Text',
            isShow: false,
            required: true
        }
    ]
    const generateCrud = new GenerateCrud(entity, attribute, 'containerGCLocation', app);

    const initGenerateCrud = () => {
        console.log('init form');
        return generateCrud.initGenerateCrud("popup-form", "Nuevo formulario")
    }
    const initForm = () => {
        console.log('init form');
        return generateCrud.initForm()
    }
    const generateList = () => {
        console.log('init list');
        return generateCrud.initList()
    }
    return {
        initGenerateCrud,
        initForm,
        generateList
    }


}