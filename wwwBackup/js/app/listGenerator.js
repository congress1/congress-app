import getRouteApi from "./api.js";

const $ = Dom7;
class ListGenerator {
    allData = []
    data = [
        {
            "name": "Bryan Mc Giver",
            "dni": "71450637",
            "gender": "Male"
        },
        {
            "name": "Diego",
            "dni": "71450655",
            "gender": "Male"
        }
        // aquí puedes tener más objetos con datos
    ];
    paginate = {
        currentPage: 1,
        totalElements: 0,
        totalPages: 0,
    }
    isMargin = true

    constructor(entity, attributes, containerId, popupClass, app, randomNumber) {
        this.$ = $;
        this.entity = entity;
        this.randomNumber = randomNumber;
        this.attributes = attributes;
        this.containerId = containerId;
        this.popupClass = popupClass;
        this.app = app;
    }

    noneMargin() {
        this.isMargin = false
    }
    openEditForm(index) {
        if (index !== null) {
            let dataItem = this.allData[index];
            console.log(dataItem)
            this.app.emit('eventEdit' + this.entity + this.randomNumber, dataItem.id);
        } else {
            this.app.emit('eventEdit' + this.entity + this.randomNumber, null);
        }
        this.app.popup.open(`.${this.popupClass}`);
    }

    resolvePath(path, obj) {
        return path.split('.').reduce((prev, curr) => {
            return (prev ? prev[curr] : undefined);
        }, obj);
    }

    generateListSkeleton() {
        this.$(`#${this.containerId}`).empty();
        let listHtml = ``;
        if (this.isMargin) {
            listHtml += '<div class="mt-50 container-glist">';
        } else {
            listHtml += '<div class="container-glist">';
        }
        const allDataAux = [
            {
                item1: ".................................",
                item2: "..........................",
                item3: "..........................",
            },
            {
                item1: ".................................",
                item2: "..........................",
                item3: "..........................",
            },
            {
                item1: ".................................",
                item2: "..........................",
                item3: "..........................",
            },
            {
                item1: ".................................",
                item2: "..........................",
                item3: "..........................",
            },
            {
                item1: ".................................",
                item2: "..........................",
                item3: "..........................",
            },
        ]
        allDataAux.forEach((dataItem, index) => {

            listHtml += `<div id="item-${index}" class="card margin-bottom-half skeleton-text skeleton-effect-wave">
                        <div class="card-content card-content-padding">
                            <div class="row">
                                <div class="col-auto">
                                    <div>
                                        Item 1: ${dataItem.item1}
                                    </div>
                                    <div>
                                        Item 2: ${dataItem.item2}
                                    </div>
                                    <div>
                                        Item 3: ${dataItem.item3}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`;
        });

        listHtml +=
            '</div>';

        const $container = this.$(`#${this.containerId}`);
        $container.append(listHtml);

    }
    generateList() {
        this.$(`#${this.containerId}`).empty();
        let listHtml = ``;
        if (this.popupClass) {
            listHtml += `<button class="floating-button" id="openButton">+</button>`;
        }
        if (this.isMargin) {
            listHtml += '<div class="mt-50 container-glist">';
        } else {
            listHtml += '<div class="container-glist">';
        }
        if (this.allData.length === 0) {
            listHtml += '<p>No hay nada que mostrar</p>';
        }
        this.allData.forEach((dataItem, index) => {
            let listItemHtml = '';

            this.attributes.forEach(attribute => {
                if (attribute.isShow) {
                    listItemHtml += `<div>${attribute.label}: ${this.resolvePath(attribute.values ? attribute.values.valueList : attribute.value, dataItem)}</div>`;
                }
            });

            listHtml += `<div id="item-${index}" class="card margin-bottom-half">
                        <div class="card-content card-content-padding">
                            <div class="row">
                                <div class="col-auto">
                                    ${listItemHtml}
                                </div>
                            </div>
                        </div>
                    </div>`;
        });
        const lastPage = Math.round(this.paginate.totalElements / 10)
        console.log('this.paginate.totalElements', this.paginate.totalElements)
        console.log('this.paginate.totalPages', this.paginate.totalPages)
        console.log('lastPage', lastPage)
        if (this.paginate.currentPage <= lastPage && this.allData.length >= 10) {
            console.log('ver mas')
            listHtml += `</ul>
                <div class="footer-fixed">
                    <div style="width: 150px">
                        <button class="button button-white popup-open" id="seeMore">
                            <span class="preloader" id="loadingSeeMore"></span>
                            Ver más
                        </button>
                    </div>
                </div>
                </div>`;
        } else {
            listHtml +=
                '</div>';
        }

        const $container = this.$(`#${this.containerId}`);
        const $seeMore = this.$('#seeMore');
        $container.append(listHtml);

        $('#loadingSeeMore').hide();
        // Ahora detectamos el evento click en cada li, por su ID
        this.allData.forEach((dataItem, index) => {
            $container.on('click', `#item-${index}`, () => {
                // Llamamos a la función openEditForm con el índice del elemento
                this.openEditForm(index);
            });
        });

        $container.on('click', '#openButton', () => {
            // Llamamos a la función openEditForm con el índice del elemento
            this.openEditForm(null);
        });

        $container.on('scroll', function () {
            console.log('scroll')
            if ($container.scrollTop() + $container.innerHeight() >= $container[0].scrollHeight) {
                // Has llegado al final del div, cargamos más contenido
                alert('debo cargar mas')
            }
        })
        $container.on('click', '#seeMore', () => {
            console.log('ver más')
            console.log(this.containerId)
            $('#loadingSeeMore').show();
            $('#seeMore').attr('disabled', true);
            if (this.paginate.currentPage < this.paginate.totalPages) {
                this.paginate.currentPage = this.paginate.currentPage + 1
                this.fetchData()
            }
            console.log(this.paginate.currentPage)
        })
    }

    generateListOld() {
        this.$(`#${this.containerId}`).empty();
        let listHtml = `<button class="floating-button" id="openButton">+</button>`;
        listHtml += '<div class="list"><ul>';

        this.allData.forEach((dataItem, index) => {
            let listItemHtml = '';

            this.attributes.forEach(attribute => {
                if(attribute.isShow){
                    listItemHtml += `<div class="item item-title">${attribute.label}: ${this.resolvePath(attribute.values ? attribute.values.valueList : attribute.value, dataItem)}</div>`;
                }
            });

            listHtml += `<li id="item-${index}">
                        <a href="#" class="item-link item-content">
                            <div class="item-inner">
                                <div class="item-title">
                                    ${listItemHtml}
                                </div>
                                <div class="item-after"></div>
                            </div>
                        </a>
                    </li>`;
        });
        const lastPage = Math.round(this.paginate.totalElements / this.paginate.totalPages)
        if (this.paginate.currentPage <= lastPage && this.allData.length >= 10) {
            console.log('ver mas')
            listHtml += `</ul>
                <div class="footer-fixed">
                    <div style="width: 150px">
                        <button class="button button-fill popup-open" id="seeMore">
                        <span class="preloader" id="loadingSeeMore"></span>
                        Ver más</button>
                    </div>
                </div>
                </div>`;
        } else {
            listHtml += '</ul>' +
                '</div>';
        }

        const $container = this.$(`#${this.containerId}`);
        const $seeMore = this.$('#seeMore');
        $container.append(listHtml);

        $('#loadingSeeMore').hide();
        // Ahora detectamos el evento click en cada li, por su ID
        this.allData.forEach((dataItem, index) => {
            $container.on('click', `#item-${index}`, () => {
                // Llamamos a la función openEditForm con el índice del elemento
                this.openEditForm(index);
            });
        });

        $container.on('click', '#openButton', () => {
            // Llamamos a la función openEditForm con el índice del elemento
            this.openEditForm(null);
        });

        $container.on('scroll', function () {
            console.log('scroll')
            if ($container.scrollTop() + $container.innerHeight() >= $container[0].scrollHeight) {
                // Has llegado al final del div, cargamos más contenido
                alert('debo cargar mas')
            }
        })
        $container.on('click', '#seeMore', () => {
            console.log('ver más')
            console.log(this.containerId)
            $('#loadingSeeMore').show();
            $('#seeMore').attr('disabled', true);
            if (this.paginate.currentPage < this.paginate.totalPages) {
                this.paginate.currentPage = this.paginate.currentPage + 1
                this.fetchData()
            }
            console.log(this.paginate.currentPage)
        })
    }

    clearAllData() {
        this.allData = []
    }
    fetchData() {
        this.generateListSkeleton()
        $('#loadingSeeMore').show();
        $('#seeMore').attr('disabled', true);
        this.app.request({
            url: `${getRouteApi()}/congress/v1/api/${this.entity}?page=${this.paginate.currentPage}`,
            method: 'GET',
            contentType: 'application/json',
            // headers: {
            //     'Authorization': 'Bearer ' + localStorage.getItem('token') // reemplaza token con tu token
            // },
            success: (data, status, xhr) => {
                this.data = JSON.parse(data).data; // Parsea la respuesta a un objeto JSON
                this.allData = this.allData.concat(this.data)
                this.paginate = JSON.parse(data).paginate
                this.generateList()
                $('#seeMore').removeAttr('disabled');
                $('#loadingSeeMore').hide();
            },
            error: function (xhr, status) {
                console.log('Error: ' + status);
                $('#seeMore').removeAttr('disabled');
                $('#loadingSeeMore').hide();
            }
        });
    }
    initList() {
        this.fetchData()
    }

}

export default ListGenerator;