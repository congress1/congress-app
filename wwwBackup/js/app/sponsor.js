import GenerateCrud from "./generateCrud.js";

export default function sponsor(app) {
    let entity = 'sponsor'
    let attribute = [
        {
            label: 'Nombre',
            value: 'name',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Descripción',
            value: 'description',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Link',
            value: 'urlSocialNetwork',
            values: {
                valueList: 'url_social_network',
                valueEdit: 'url_social_network'
            },
            type: 'Text',
            isShow: true,
            required: true
        },
        // {
        //     label: 'Url de imagen',
        //     value: 'urlImage',
        //     values: {
        //         valueList: 'url_image',
        //         valueEdit: 'url_image'
        //     },
        //     type: 'Text',
        //     isShow: true,
        //     required: true
        // },
        {
            label: 'Tipo',
            value: 'typeCode',
            values: {
                valueList: 'type.name',
                valueEdit: 'type.id'
            },
            type: 'Table',
            uri: 'setting/list-internal-code/TISP000',
            isShow: false,
            required: true
        },
    ]
    const generateCrud = new GenerateCrud(entity, attribute, 'containerGCSponsor', app);

    const initGenerateCrud = () => {
        console.log('init form');
        return generateCrud.initGenerateCrud("popup-form", "Nuevo formulario")
    }
    const initForm = () => {
        console.log('init form');
        return generateCrud.initForm()
    }
    const generateList = () => {
        console.log('init list');
        return generateCrud.initList()
    }
    return {
        initGenerateCrud,
        initForm,
        generateList
    }


}