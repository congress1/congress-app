import getRouteApi from "./api.js";

const $ = Dom7;

// var $ = jQuery.noConflict();
class FormGenerator {
    id
    formAux = {}
    tableCache = {};
    uriCustomSubmit = null
    entityUnique = null
    listSelected = []
    methodCreate = 'POST'

    constructor(entity, attributes, containerId, popupClass, app, randomNumber) {
        this.$ = $;
        this.entity = entity;
        this.entityUnique = entity + randomNumber;
        this.attributes = attributes;
        this.containerId = containerId;
        this.popupClass = popupClass;
        this.app = app;
        this.id = null
    }

    setId(id) {
        this.id = id
        this.initForm()
    }

    setPopupClass(popupClass) {
        this.popupClass = popupClass
    }

    setUriCustomSubmit(uriCustomSubmit) {
        this.uriCustomSubmit = uriCustomSubmit
    }

    setMethodCreate(methodCreate) {
        this.methodCreate = methodCreate
    }

    generateInputField(attribute) {
        const required = attribute.required ? "required" : "";

        return `<div class="col-100 medium-50 large-33 margin-bottom-half margin-top">
                <div class="list form-list no-margin">
                    <ul>
                        <li class="item-content item-input">
                            <div class="item-inner">
                                <div class="item-title item-floating-label">${attribute.label}</div>
                                <div class="item-input-wrap">
                                    <input type="${attribute.type.toLowerCase()}" id="${this.entityUnique}-${attribute.value}" ${required} />
                                    <div id="error-${this.entityUnique}-${attribute.value}" class="item-input-error-message d-block"></div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>`;
    }

    generateInputFieldTime(attribute) {
        const required = attribute.required ? "required" : "";

        return `<div class="col-100 medium-50 large-33 margin-bottom-half">
                <div class="list form-list no-margin">
                    <ul>
                        <li class="item-content item-input">
                            <div class="item-inner">
                                <div class="item-title item-floating-label">${attribute.label}</div>
                                <div class="item-input-wrap">
                                    <input type="text" id="${this.entityUnique}-${attribute.value}" ${required} readonly="readonly"/>
                                    <div id="error-${this.entityUnique}-${attribute.value}" class="item-input-error-message d-block"></div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>`;
    }

    generateSelectField(attribute) {
        const required = attribute.required ? "required" : "";
        // const options = attribute.options.map(option => `<option>${option}</option>`).join('');
        const options = attribute.options.map(option => `<option value="${option.value}">${option.label}</option>`).join('');
        return `<div class="col-100 medium-50 large-33 margin-bottom-half">
                <div class="list form-list no-margin">
                    <ul>
                        <li class="item-content item-input">
                            <div class="item-inner">
                                <div class="item-title item-floating-label">${attribute.label}</div>
                                <div class="item-input-wrap">
                                    <div>
                                        <select id="${this.entityUnique}-${attribute.value}" ${required}>
                                            ${options}
                                        </select>
                                        <span>x</span>
                                    </div>
                                    <div id="error-${this.entityUnique}-${attribute.value}" class="item-input-error-message d-block"></div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>`;
    }

    getTable(uri) {
        return new Promise((resolve, reject) => {
            this.app.request({
                url: `${getRouteApi()}/congress/v1/api/${uri}`,
                method: 'GET',
                contentType: 'application/json',
                success: (data, status, xhr) => {
                    const result = JSON.parse(data).data;
                    console.log(result);
                    resolve(result);
                },
                error: (xhr, status) => {
                    console.log('Error: ' + status);
                    resolve([]);
                }
            });
        });
    }

    generateTableSelectFieldV2(attribute) {
        const required = attribute.required ? "required" : "";
        const data = this.tableCache[attribute.uri];
        const rows = data.map(option => `
                <tr class="${this.entityUnique}-${attribute.value}-tr">
                    <td class="">${option.name}</td>
                    <td class="label-cell ">
                        <input type="checkbox" class="${this.entityUnique}-${attribute.value}-checkbox" name="demo-checkbox" value="${option[attribute.keyTable ?? 'id']}"/>
                    </td>
                </tr>
            `).join('');

        return `<div class="col-100 medium-50 large-33 margin-bottom-half">
                <div class="list form-list no-margin">
                    <ul>
                        <li class="">
                        
                        <div>
                        <button class="button mt-2" id="${this.entityUnique}-${attribute.value}-add">
                          Agregar ${attribute.label}
                        </button>                        
                        <div id="error-${this.entityUnique}-${attribute.value}" class="item-input-error-message d-block"></div>
                        <div class="d-block card data-table p-2 text-align-center" style="display: none;" 
                                id="${this.entityUnique}-${attribute.value}-table">
                            <table id="${this.entityUnique}-${attribute.value}-selects" class="">
                                <thead>
                                    <tr>
                                        <th class="numeric-cell c-white" style="font-size: 16px">#</th>
                                        <th class="c-white" style="font-size: 16px">${attribute.label}</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        </div>
                        <div class="popup ${this.entityUnique}-${attribute.value}-popup-add">
                            <div class="page">
                                <div class="navbar">
                                  <div class="navbar-bg"></div>
                                  <div class="navbar-inner">
                                    <div class="title" id="popup-title">Agregar</div>
                                    <div class="right">
                                        <a href="#" class="link popup-close" 
                                            id="${this.entityUnique}-${attribute.value}-popup-add-close">
                                            Cerrar
                                        </a>
                                    </div>
                                  </div>
                                </div>
                                <div class="page-content">                                
                                    <div class="data-table">   
                                    <div>
                                        <form data-search-container=".search-list" data-search-in=".item-title" class="searchbar searchbar-init">
                                          <div class="searchbar-inner">
                                            <div class="searchbar-input-wrap">
                                              <input type="search" placeholder="Buscar"/>
                                              <i class="searchbar-icon"></i>
                                              <span class="input-clear-button"></span>
                                            </div>
                                            <span class="searchbar-disable-button if-not-aurora">Cancel</span>
                                          </div>
                                        </form>
                                    </div>                                  
                                    <table>
                                        <tbody id="${this.entityUnique}-${attribute.value}-body">
                                            ${rows}
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </li>
                    </ul>
                </div>
            </div>`;
    }
    generateTableSelectField(attribute) {
        const required = attribute.required ? "required" : "";
        const data = this.tableCache[attribute.uri];
        const options = data?.map(option => `<option value="${option[attribute.keyTable ?? 'id']}">${option.name}</option>`).join('');
        if (!attribute.isMultiple) {
            return `<div class="col-100 medium-50 large-33 margin-bottom-half">
                <div class="list form-list no-margin">
                    <ul>
                        <li class="item-content item-input"  id="${this.entityUnique}-${attribute.value}-li">
                            <div class="item-inner">
                                <div class="item-title item-floating-label">${attribute.label}</div>
                                <div class="item-input-wrap">
                                    <div class="d-flex">
                                        <select id="${this.entityUnique}-${attribute.value}" ${required}>
                                            ${options}
                                        </select>
                                        <div class="clear-select" id="${this.entityUnique}-${attribute.value}-select">
                                            <span>x</span>
                                        </div>
                                    </div>
                                    <div id="error-${this.entityUnique}-${attribute.value}" class="item-input-error-message d-block"></div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>`;
        } else {
            return `<div class="col-100 medium-50 large-33 margin-bottom-half">
                        <div class="list form-list no-margin">
                            <ul>
                                <li >
                                   <a class="item-link smart-select smart-select-init" data-open-in="popover">
                                        <select id="${this.entityUnique}-${attribute.value}" ${required} multiple>
                                            ${options}
                                        </select>                            
                                        <div id="error-${this.entityUnique}-${attribute.value}" class="item-input-error-message d-block"></div>
                                        <div class="item-content p-0">
                                          <div class="item-inner">
                                            <div class="item-title">${attribute.label}</div>
                                          </div>
                                        </div>
                                      </a>
                                </li>
                            </ul>
                        </div>
                    </div>`;
        }
    }

    generateBooleanField(attribute) {
        const required = attribute.required ? "required" : "";
        return `<div class="col-100 medium-50 large-33 margin-bottom-half">
            <div class="list form-list no-margin">
                <ul>
                    <li class="item-content item-input">
                    <div class="row">
                            <div class="col-auto">
                                <label class="toggle toggle-init color-green">
                                    <input type="checkbox" id="${this.entityUnique}-${attribute.value}" ${required} value="0"/>
                                    <span class="toggle-icon"></span>
                                </label>
                            </div>
                            <div class="col">
                                <h5 class="no-margin-bottom">${attribute.label}</h5>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>`;
    }

    generateAttributeField(attribute) {
        switch (attribute.type) {
            case 'Text':
            case 'Number':
            case 'Date':
            case 'Color':
                return this.generateInputField(attribute);
            case 'Time':
                return this.generateInputFieldTime(attribute);
            case 'Enum':
                return this.generateSelectField(attribute);
            case 'Boolean':
                return this.generateBooleanField(attribute);
            case 'Table':
                return this.generateTableSelectField(attribute);
            case 'TableList':
                return this.generateTableSelectFieldV2(attribute);
            default:
                return '';
        }
    }

    generateAttributeConditional(attribute) {
        const conditionals = attribute.conditionals
        console.log("valor1 :: ", attribute.value)
        if (conditionals) {
        console.log("conditionals :: ", conditionals);
            conditionals.forEach(conditional => {
                let valueConditional = this.$(`#${this.entityUnique}-${conditional.value}`).val()
                console.log("valor de conditional :: ", conditional.value, " igual a true? :: ", valueConditional === "true")
                if (valueConditional === "true") {
                    valueConditional = true
                } else if (valueConditional === "false") {
                    valueConditional = false
                }
                console.log("condicional :: ", conditional.conditional, " igual a equals :: ",conditional.conditional === 'equals')
                if (conditional.conditional === 'equals') {
                    if (conditional.type === 'show') {
                        console.log('valueConditional', valueConditional)
                        console.log('conditional.equalTo', conditional.equalTo)
                        if (valueConditional === conditional.equalTo) {
                            attribute.showConditional = 'Show';
                        } else {
                            attribute.showConditional = 'Hidden';
                        }
                    }
                }
            })
        }
        return attribute
    }

    generateShowHiddenConditional(attribute) {
        const conditionals = attribute.conditionals
        if (conditionals) {
            if (attribute.showConditional === 'Show') {
                this.$(`#${this.entityUnique}-${attribute.value}`).parent().parent().parent().parent().parent().parent().show()
            } else {
                this.$(`#${this.entityUnique}-${attribute.value}`).parent().parent().parent().parent().parent().parent().hide()
            }
        }
    }

    resolvePath(path, obj) {
        return path.split('.').reduce((prev, curr) => {
            return (prev ? prev[curr] : undefined);
        }, obj);
    }

    setValue(attribute) {

        const value = this.resolvePath(attribute.values ? attribute.values.valueEdit : attribute.value, this.formAux) ?? attribute.default ?? null;

        switch (attribute.type) {
            case 'Text':
            case 'Number':
            case 'Date':
            case 'Enum':
            case 'Time':
                if (value) {
                    this.$(`#${this.entityUnique}-${attribute.value}`).parent().parent().parent().addClass('item-input-with-value');
                    console.log('set value', value)
                    console.log('etiqueta', `#${this.entityUnique}-${attribute.value}`)
                    this.$(`#${this.entityUnique}-${attribute.value}`).val(value)
                } else {
                    console.log('no entro pipipi tag', `#${this.entityUnique}-${attribute.value}`)
                    this.$(`#${this.entityUnique}-${attribute.value}`).val(null)
                }
                break
            case 'Boolean':
                console.log('Boolean value table', value, attribute, this.formAux)
                if (value) {
                    this.$(`#${this.entityUnique}-${attribute.value}`).parent().parent().parent().addClass('item-input-with-value');
                    console.log('set value', attribute, value)
                    this.$(`#${this.entityUnique}-${attribute.value}`).prop('checked', value);
                    this.$(`#${this.entityUnique}-${attribute.value}`).val(value)
                } else {
                    this.$(`#${this.entityUnique}-${attribute.value}`).val(false)
                }
                break
            case 'Hidden':
                if (value) {
                    this.formAux[attribute.value] = value
                    console.log('this.formAux', this.formAux)
                }
                break
            case 'TableList':
            case 'Table':
                console.log('value table', value, attribute.value)
                if (value) {
                    this.$(`#${this.entityUnique}-${attribute.value}`).parent().parent().parent().addClass('item-input-with-value');
                    this.$(`#${this.entityUnique}-${attribute.value}`).val(value)
                } else {
                    this.$(`#${this.entityUnique}-${attribute.value}`).val(null)
                }
                break
            default:
        }

    }

    generateHalfHourBlocks = () => {
        const timeBlocks = [];
        for (let i = 0; i < 24; ++i) {
            // generate a string for each hour and the half hour mark
            const hourString = i.toString().padStart(2, '0');
            timeBlocks.push(hourString + ":00");
            timeBlocks.push(hourString + ":15");
            timeBlocks.push(hourString + ":30");
        }
        return timeBlocks;
    }
    bindFieldValidation(attribute) {
        if (attribute.type === 'Table') {
            this.$(`#${this.entityUnique}-${attribute.value}-select`).hide()
            this.$(`#${this.entityUnique}-${attribute.value}-select`).on('click', (e) => {
                console.log(e, 'limpiar')
                this.$(`#${this.entityUnique}-${attribute.value}`).val(null)
                console.log(this.$(`#${this.entityUnique}-${attribute.value}`).parent().parent().parent())
                this.$(`#${this.entityUnique}-${attribute.value}-li`).removeClass('item-input-with-value')
                this.$(`#${this.entityUnique}-${attribute.value}-li`).addClass('item-input')
                // this.$(`#${this.entityUnique}-${attribute.value}`).removeClass('input-with-value')
                this.$(`#${this.entityUnique}-${attribute.value}-select`).hide()
            })
        }
        if (attribute.type === 'Time') {
            this.app.picker.create({
                inputEl: `#${this.entityUnique}-${attribute.value}`,
                cols: [
                    {
                        textAlign: 'center',
                        values: this.generateHalfHourBlocks()
                    }
                ]
            });
        }
        if (attribute.type === 'TableList') {
            //todo evento post creacion de fields
            this.$(`#${this.entityUnique}-${attribute.value}-add`).on('click', (e) => {
                console.log('click', e, `#${this.entityUnique}-${attribute.value}-popup-add`)
                this.app.popup.open(`.${this.entityUnique}-${attribute.value}-popup-add`);
            })
            var selectedTemp = [];
            var selectedIds = [];
            var isMultipleSelection = attribute.isMultiple; // Puedes cambiar este valor según tus requerimientos
            this.$('input[type="search"]').on('input', (event) => {
                let searchVal = $(event.target).val().toLowerCase();
                this.$(`#${this.entityUnique}-${attribute.value}-body .inscription-enroll-participants-tr`).each((index, element) => {
                    let tdElement = this.$(index).find('td:first-child');
                    if (tdElement.length > 0) {
                        let cellText = tdElement.text();
                        if (cellText === null) {
                            console.error("No text in cell:", tdElement);
                        } else {
                            cellText = cellText.toLowerCase();
                            if (!cellText.includes(searchVal)) {
                                // El texto de búsqueda no coincide, oculta la fila
                                this.$(index).hide();
                            } else {
                                // El texto de búsqueda coincide, muestra la fila
                                this.$(index).show();
                            }
                        }
                    }
                });
            });
            this.$(`.${this.entityUnique}-${attribute.value}-checkbox`).on('change', (event) => {
                console.log('Cambio registrado');
                this.$(`#error-${this.entityUnique}-${attribute.value}`).text('')
                var checkbox = $(event.target);
                var value = checkbox.val();
                console.log('value', value)
                var name = checkbox.parent().parent().find('td').text();// Recupera el nombre de la celda adyacente
                if (!isMultipleSelection) {
                    // Si no es selección múltiple, quita la marca a todos los otros checkboxes
                    this.$(`.${this.entityUnique}-${attribute.value}-checkbox`).each(function () {
                        if (this !== checkbox[0]) {
                            this.checked = false;
                        }
                    });
                    selectedTemp = [];
                    selectedIds = [];
                }

                if (checkbox.is(':checked')) {
                    // Si el checkbox está seleccionado, agrega el valor al array
                    console.log('selectedTemp.length', selectedTemp.length)
                    console.log('selectedIds.length', selectedIds.length)
                    console.log('attribute.maximum', attribute.maximum)
                    if (selectedTemp.length >= (attribute.maximum ?? 1) && selectedIds.length >= (attribute.maximum ?? 1)) {
                        checkbox.prop('checked', false);
                        this.app.dialog.alert('Excede el limite máximo', 'Error');
                        return
                    } else {
                        selectedTemp.push({value: value, name: name});
                        selectedIds.push(value);
                        console.log('selectedTemp', selectedTemp)
                        console.log('selectedIds', selectedIds)
                    }
                } else {
                    // Si el checkbox no está seleccionado, remueve el valor del array
                    selectedTemp = selectedTemp.filter(item => item.value !== value);
                    selectedIds = selectedIds.filter(item => item !== value);
                }

                // Reconstruye toda la lista
                var listContent = '';
                for (let i = 0; i < selectedTemp.length; i++) {
                    listContent += `
            <tr id="item-${selectedTemp[i].value}">
                <td class="numeric-cell">${i + 1}</td>
                <td class="">${selectedTemp[i].name}</td>
            </tr>
        `;
                }

                this.$(`#${this.entityUnique}-${attribute.value}-selects tbody`).html(listContent);
                this.listSelected[`${this.entityUnique}-${attribute.value}`] = selectedIds
                // Muestra u oculta la tabla dependiendo de si hay elementos seleccionados
                if (selectedTemp.length === 0) {
                    this.$(`#${this.entityUnique}-${attribute.value}-table`).hide();
                } else {
                    this.$(`#${this.entityUnique}-${attribute.value}-table`).show();
                }
            });
            // Event handler para las filas, con función flecha
            this.$(`tr.${this.entityUnique}-${attribute.value}-tr`).on('click', (event) => {
                if (!$(event.target).is(`.${this.entityUnique}-${attribute.value}-checkbox`)) {

                    // Encuentra el checkbox en la fila
                    var checkbox = $(event.currentTarget).find(`input.${this.entityUnique}-${attribute.value}-checkbox`);

                    // Cambia el estado del checkbox
                    checkbox.prop('checked', !checkbox.prop('checked')).change();
                }
            });

        }
        this.$(`#${this.entityUnique}-${attribute.value}`).on('input blur', (event) => {
            console.log("eventTarget :: ", event.target)
            let checkCurrent = event.target.checked;
            let checkvalue = event.target.value;
            console.log("checkValue", checkvalue);
            let fieldValue;
            if (attribute.type === 'Boolean') {
                fieldValue = this.$(`#${this.entityUnique}-${attribute.value}`).val(checkCurrent);
            } else {
                fieldValue = this.$(`#${this.entityUnique}-${attribute.value}`).val(checkvalue);
            }
            console.log('change2', fieldValue)
            if (fieldValue) {
                this.$(`#${this.entityUnique}-${attribute.value}-select`).show()
            } else {
                this.$(`#${this.entityUnique}-${attribute.value}-select`).hide()
            }
            if (!(attribute.required && !fieldValue)) {
                $(`#error-${this.entityUnique}-${attribute.value}`).text('');
                $(`#${this.entityUnique}-${attribute.value}`).removeClass('input-invalid');
            } else {
                this.setErrorId(attribute.value, `El campo ${attribute.label} es requerido.`);
            }
            console.log("type :: ", attribute.type)
            if (attribute.type !== 'Boolean') {
                this.attributes.forEach((attribute, index) => {
                    this.attributes[index] = this.generateAttributeConditional(this.attributes[index]);
                    console.log("!== boolean", this.attributes[index])
                    this.generateShowHiddenConditional(this.attributes[index]);
                });
            }
        });
    }

    setValidationBackend(attribute, message) {
        this.setErrorId(attribute, message);
    }

    setErrorId(attribute, message) {
        $(`#error-${this.entityUnique}-${attribute}`).text(message);
        $(`#${this.entityUnique}-${attribute}`).addClass('input-invalid')
    }

    clearForm() {
        console.log("clear form")
        this.formAux = {}
        this.id = null
        this.setDefaultValueAttribute()
    }

    submit(data) {
        console.log("submit1 uri --> ", `${getRouteApi()}/congress/v1/api/${this.entity}/${this.id}`);
        this.$(`#loading-${this.entityUnique}`).show();
        this.$(`#button-${this.entityUnique}`).attr('disabled', true);
        if (this.id) {
            this.app.request({
                url: `${getRouteApi()}/congress/v1/api/${this.entity}/${this.id}`,
                method: 'PUT',
                contentType: 'application/json',
                data: data,
                success: (data, status, xhr) => {
                    this.app.popup.close(`.${this.popupClass}`);
                    console.log('data', data)
                    this.app.emit('reloadList' + this.entityUnique, data);
                    // alert(data.message)
                    this.app.dialog.alert(JSON.parse(data).message, 'Éxito');
                    this.buttonDefault();
                    this.clearForm()
                },
                error: (xhr, status) => {
                    console.log('Error: ' + status);
                    this.catchLogic(xhr, status)
                }
            });
        } else {
            console.log("submit2 uri --> ", this.uriCustomSubmit ? `${getRouteApi()}/congress/v1/api/${this.uriCustomSubmit}` : `${getRouteApi()}/congress/v1/api/${this.entity}`);
            this.app.request({
                url: this.uriCustomSubmit ? `${getRouteApi()}/congress/v1/api/${this.uriCustomSubmit}` : `${getRouteApi()}/congress/v1/api/${this.entity}`,
                method: this.methodCreate,
                contentType: 'application/json',
                data: data,
                success: (data, status, xhr) => {
                    if (this.popupClass) {
                        this.app.popup.close(`.${this.popupClass}`);
                    }
                    console.log('data', data)
                    this.app.dialog.alert(JSON.parse(data).message, 'Éxito');
                    this.app.emit('reloadList' + this.entityUnique, data);
                    this.buttonDefault();
                    this.clearForm()
                },
                error: (xhr, status) => {
                    console.log('Error: ' + status);
                    this.catchLogic(xhr, status)
                },
            });
        }
    }

    catchLogic(xhr, status) {
        if (status >= 400 && status <= 499) {
            const response = JSON.parse(xhr.responseText).errors;
            if (response.length > 0) {
                this.app.dialog.alert(response[0], 'Error');
            } else if (typeof response === 'object') {
                console.log('La respuesta es un objeto');
                console.log(response)
                const firstErrorValue = Object.values(response)[0];
                console.log(firstErrorValue);
                this.app.dialog.alert(firstErrorValue, 'Error');
                Object.keys(response).forEach((key) => this.setValidationBackend(key, response[key]))
            }
            // this.app.dialog.alert(JSON.parse(xhr.responseText).errors[0], 'Error');
            // document.getElementsByClassName('dialog-title')[0].style.color = 'red';
            this.buttonDefault();
        }
    }
    buttonDefault() {
        console.log('buttonDefault')
        this.$(`#loading-${this.entity}`).hide();
        this.$(`#button-${this.entity}`).removeAttr('disabled');
    }

    deleteEntity() {
        console.log('entity :: ',this.entity, ' id :: ', this.id)
        // this.app.request({
        //     url: `${getRouteApi()}/congress/v1/api/${this.entity}/${this.id}`,
        //     method: 'DELETE',
        //     contentType: 'application/json',
        //     success: (data, status, xhr) => {
        //         if (this.popupClass) {
        //             this.app.popup.close(`.${this.popupClass}`);
        //         }
        //         this.$(`#button-delete`).removeAttr('disabled');
        //         console.log('data', data)
        //         this.app.emit('reloadList', data);
        //         this.clearForm()
        //     },
        //     error: function (xhr, status) {
        //         console.log('Error: ' + status);
        //     }
        // });
    }

    bindSubmitButton() {
        this.$(`#button-${this.entity}`).on('click', (e) => {
            e.preventDefault();
            let allFieldsValid = true;
            const formData = {};
            this.attributes.forEach(attribute => {
                let fieldValue = this.$(`#${this.entityUnique}-${attribute.value}`).val();
                formData[attribute.value] = !fieldValue ? null : fieldValue;
                if (attribute.type === 'Hidden') {
                    formData[attribute.value] = attribute.default;
                }
                if (attribute.type === 'TableList') {
                    formData[attribute.value] = this.listSelected[`${this.entityUnique}-${attribute.value}`];
                    fieldValue = this.listSelected[`${this.entityUnique}-${attribute.value}`]
                }
                if (attribute.type !== 'Hidden') {
                    if (attribute.type === 'TableList') {
                        if (!((fieldValue ?? []).length >= attribute.minimum)) {
                            allFieldsValid = false;
                            this.$(`#error-${this.entityUnique}-${attribute.value}`).text(`No cumple con cantidad mínima ${attribute.minimum}`);
                        } else {
                            this.$(`#error-${this.entityUnique}-${attribute.value}`).text(''); // Limpiar mensaje
                        }
                    } else {
                        if (attribute.required && !fieldValue) {
                            allFieldsValid = false;
                            this.$(`#error-${this.entityUnique}-${attribute.value}`).text(`El campo ${attribute.label} es requerido.`);
                        } else {
                            this.$(`#error-${this.entityUnique}-${attribute.value}`).text(''); // Limpiar mensaje
                        }
                        if (attribute.min !== undefined && attribute.max !== undefined && fieldValue !== undefined) {
                            if ((attribute.min <= fieldValue.length && fieldValue.length <= attribute.max)) {
                                this.$(`#error-${this.entityUnique}-${attribute.value}`).text(''); // Limpiar mensaje
                            } else {
                                allFieldsValid = false;
                                this.$(`#error-${this.entityUnique}-${attribute.value}`).text(`El campo ${attribute.label} debe tener minimo ${attribute.min} y máximo ${attribute.max}.`);
                            }
                        }
                    }
                }
            });
            console.log(formData)
            if (allFieldsValid) {
                console.log('Todos los campos son válidos');

                this.submit(formData)
            }

        });
    }

    async preloadTables() {
        const tableAttributes = this.attributes.filter(attribute => attribute.type === 'Table' || attribute.type === 'TableList');
        const tableRequests = tableAttributes.map(attribute => this.getTable(attribute.uri));
        const tableData = await Promise.all(tableRequests);

        // Populate tableCache
        tableAttributes.forEach((attribute, index) => {
            this.tableCache[attribute.uri] = tableData[index];
        });
    }

    showImage(containerId) {
        console.log("dentro de show image :: ", this.formAux);

        if (this.containerId.includes("containerGCSponsorForm") || this.containerId.includes("containerGCSpeakerForm")) {

            let htmlBase = `
            <div class="inscription-section-actions justify-content-center">
                <div class="inscription-section-child-icons" id="${this.containerId}-file">
                    <i class="bi bi-file-earmark-arrow-up" ></i>
                </div>
            </div>
            <input type="file" accept="image/*"  id="${this.containerId}-file-input" style="display: none">
            `;
            this.$(`#${this.containerId}`).append(htmlBase);
            //si tiene imagen se muestra en formulario
            let image = this.formAux?.url_image;
            if(image) {
                console.log("existe imagen en :: ", this.entity)
                let html = `
                    <div class="inscription-image-preview-container mt-15" id="${this.containerId}-container-preview">
                    <div id="${this.containerId}-preview-action" class="button-x"></div>
                        <div id="${this.containerId}-load-image">
                           <img src="${image}" alt="" class="inscription-image-preview">
                        </div>
                    </div>
                    `
                this.$(`#${this.containerId}`).append(html);
                $('.inscription-section-actions').hide();
                const sPreviewAction = `#${this.containerId}-preview-action`
                $(sPreviewAction).append(`<i class="bi bi-x"></i>`)
                $(sPreviewAction).on('click', () => {
                    $(`#${this.containerId}-load-image`).empty()
                    const iipc = `#${this.containerId}-container-preview`
                    $(iipc).empty()
                    $(iipc).append(`
                    <div id="${this.containerId}-preview-action" class="button-x"></div>
                    <div id="${this.containerId}-preview"></div>
                    `
                    )
                    $(iipc).hide()
                    $('.inscription-section-actions').show()
                })

            } else {
                console.log("no existe imagen en :: ", this.entity)
                const html = `
                      <div class="inscription-image-preview-container" id="${this.containerId}-container-preview">
                        <div id="${this.containerId}-preview-action" class="button-x"></div>
                            <div id="${this.containerId}-preview"></div>
                      </div>
                    `
                $(`#${this.containerId}`).append(html);
                $(`#${this.containerId}-container-preview`).hide()
            }
            return  '';
        }
        return  '';
    }

    handleOnChange = (e, entityId, photoInput, elementId) => {
        let file = e.target.files[0];
        let preview = document.getElementById(`${this.containerId}-preview`);
        let reader = new FileReader();
        console.log("cambio de imagen")
        reader.onloadend =  () => {
            console.log("dentro de onloadend")
            if (file) {
                console.log("existe file")
                const iipc = `#${this.containerId}-container-preview`
                $(iipc).show()
                let image = new Image();
                image.classList.add('skeleton-effect-image');
                image.src = reader.result;
                image.classList.add('inscription-image-preview');
                preview.innerHTML = '';
                preview.append(image);
                $('.inscription-section-actions').hide()
                this.submitAction(entityId, elementId, image)
                const sPreviewAction = `#${this.containerId}-preview-action`
                $(sPreviewAction).append(`
                                <i class="bi bi-x"></i>
                            `)
                $(sPreviewAction).on('click', () => {
                    $(`#${this.containerId}-load-image`).empty()
                    const iipc = `#${this.containerId}-container-preview`
                    $(iipc).empty()
                    $(iipc).append(`
                                <div id="${this.containerId}-preview-action" class="button-x"></div>
                                <div id="${this.containerId}-preview"></div>`
                    )
                    $(iipc).hide()
                    $('.inscription-section-actions').show()
                })

                photoInput.value = '';
            }
        }

        if (file) {
            reader.readAsDataURL(file);
        }
    }

    submitAction = (entityId, elementId = 'photo-input', image) => {
        const photoInput = document.getElementById(`${this.containerId}-${elementId}`);
        if (photoInput.files.length > 0) {
            const photoFile = photoInput.files[0];
            this.uploadImage(entityId, photoFile, image);
        } else {
            console.log("No photo was taken");
        }
    }

     uploadImage = (entityId, photoFile, image) => {
        let formData = new FormData();
        formData.append('file', photoFile);

        // Muestra el cargador
        try {
            this.app.request({
                url: `${getRouteApi()}/congress/v1/api/${this.entity}/upload-image/${entityId}`,
                method: 'PUT',
                data: formData,
                xhrFields: {
                    onprogress: function (e) {
                        if (e.lengthComputable) {
                            console.log(e.loaded / e.total * 100 + '%');
                        }
                    }
                },
                success: (data, status, xhr) => {
                    image.classList.remove('skeleton-effect-image')
                    this.app.dialog.alert(JSON.parse(data).message, 'Éxito');

                },
                error: (xhr, status) => {
                    alert(xhr)
                    image.classList.remove('skeleton-effect-image')
                    console.log('Error: ' + status);
                }
            });
        } catch (error) {
            console.log(`Ha ocurrido un error durante el servicio de cargar imagen :: ${error}`);
        }
    }

    loadForm() {
        console.log("evento: ", this.containerId)
        this.$(`#${this.containerId}`).empty();
        const textButton = this.id ? 'Actualizar' : 'Guardar'
        const buttonDelete = this.id ?
            `
            <!--<button class="button button-fill color-red mt-2" id="button-delete">
                          Eliminar
                        </button>-->
            <!--<div class="col-90 medium-50 large-40 text-align-center margin-top-auto padding-vertical">
                <a id="button-delete" class="button button-fill button-large button-raised color-theme">Eliminar</a>
            </div>--> 
            `
            : ''
        let formHtml = '<div class="row margin-bottom">';
        this.attributes.forEach((attribute, index) => {
            this.attributes[index] = this.generateAttributeConditional(this.attributes[index]);
        });
        this.attributes.forEach(attribute => {
            formHtml += this.generateAttributeField(attribute);
        });
        formHtml += '</div>';

        formHtml += `
            ${this.showImage(this.containerId)}
            <div class="row justify-content-center">
                <div class="col-90 medium-50 large-40 text-align-center margin-top-auto padding-vertical">
                <!--<span class="preloader" id="loading-${this.entity}"></span>-->
                <a id="button-${this.entity}" class="button button-fill button-large button-raised color-theme">${textButton}</a>
                ${buttonDelete}
                </div> 
            </div>   
        `;
        this.$(`#${this.containerId}`).append(formHtml);

        this.attributes.forEach(attribute => this.bindFieldValidation(attribute));
        this.bindSubmitButton();

        this.$(`#loading-${this.entity}`).hide();

        this.setDefaultValueAttribute()

        this.attributes.forEach((attribute) => {
            this.generateShowHiddenConditional(attribute);
        });
        this.$('#button-delete').click(() => {
            console.log('eliminar')
            this.deleteEntity()
        });

        $(`#${this.containerId}-file`).on('click', () => {
            console.log("dentro de change el id de la entidad es :: ", this.formAux?.id ," entidad:: ",this.entity)
                let photoInput = document.getElementById(`${this.containerId}-file-input`);
                $(photoInput).off('change');
                $(photoInput).on('change', (e) => this.handleOnChange(e, this.formAux?.id, photoInput, 'file-input'));
                photoInput.click();
        })

    }

    loadSpinner() {
        console.log("inicio spinner formGenerator")
        this.app.dialog.preloader('cargando...');
    }

    closeSpinner(){
        console.log("fin spinner formGenerator")
        this.app.dialog.close();
    }

    loadEntity() {
        console.log('obtener desde un api la data')
        this.loadSpinner()
        this.app.request({
            url: `${getRouteApi()}/congress/v1/api/${this.entity}/${this.id}`,
            method: 'GET',
            contentType: 'application/json',
            success: (data, status, xhr) => {
                console.log(data)
                this.formAux = JSON.parse(data).data
                this.loadForm()
                this.closeSpinner();
            },
            error:  (xhr, status) => {
                console.log('Error: ' + status);
                this.closeSpinner();
            }
        });
    }

    async initForm() {
        this.formAux = {}
        if (this.id) {
            console.log("Entro con id en initForm con evento:: ", this.entity)
            this.loadEntity()
        } else {
            await this.preloadTables();
            this.loadForm()
        }

    }

    setDefaultValueAttribute() {
        console.log("data desde set Value:: ", this.formAux)
        this.attributes.forEach(attribute => {
            console.log("atributes padre :: ", attribute)
            //se setea el valor
            this.setValue(attribute);
            //para los de tipo boolean se creara la funcion que seteara el valor
            console.log("atribute debajo de padre :: ", attribute.type )
            if (attribute.type === 'Boolean') {
                let isChecked = this.$(`#${this.entityUnique}-${attribute.value}`).val();
                console.log("dentro de Boolean: ", `#${this.entityUnique}-${attribute.value}`, "valor2 :: ",this.$(`#${this.entityUnique}-${attribute.value}`).val())
                console.log("valor2 :: ",this.$(`#${this.entityUnique}-${attribute.value}`))
                if(isChecked){
                    this.attributes.forEach((attribute, index) => {
                        this.attributes[index] = this.generateAttributeConditional(this.attributes[index]);
                        console.log(this.attributes[index])
                        this.generateShowHiddenConditional(this.attributes[index]);
                    });
                }
                this.$(`#${this.entityUnique}-${attribute.value}`).change((event) => {
                    console.log('change', event.target.checked);
                    if (event.target.checked) {
                        $(event.target).val(true);
                    } else {
                        $(event.target).val(false);
                    }

                    this.attributes.forEach((attribute, index) => {
                        this.attributes[index] = this.generateAttributeConditional(this.attributes[index]);
                        console.log(this.attributes[index])
                        this.generateShowHiddenConditional(this.attributes[index]);
                    });
                });
            }
        });
    }
}

export default FormGenerator;