import getRouteApi from "../api.js";

const $ = Dom7;

export default function forgotPassword(app) {
    let loadingForgotPassword = false
    const logicForgotPassword = () => {
        var isValid = true;
        var usuario = $("#fg-email");
        var valUsuario = $("#fg-email").val();
        var fgLoginEmailError = $("#fg-login-email-error");

        if (valUsuario === "") {
            usuario.addClass('is-invalid');
            fgLoginEmailError.show();
            fgLoginEmailError.text('Por favor, ingresa tu usuario.');
            isValid = false;
        } else {
            usuario.removeClass('is-invalid');
            fgLoginEmailError.text('');
            fgLoginEmailError.hide();
        }

        // Bloquea la sumisión si el formulario no es válido
        const formForgotPassword = {
            "email": valUsuario
        }
        return {isValid, formForgotPassword}
    }
    const sendCode = () => {
        console.log('boton enviar correo')
        const {isValid, formForgotPassword} = logicForgotPassword()
        if (!isValid) {
            return
        }

        console.log("correo: "+formForgotPassword.email)
        loadingForgotPassword = true
        $('#loadingForgotPassword').show();
        $('#buttonForgotPassword').attr('disabled', true);
        app.request({
            url: `${getRouteApi()}/congress/v1/api/mail/send-code`,
            method: 'POST',
            contentType: 'application/json',
            data: formForgotPassword,
            success: function (data, status, xhr) {
                console.log(data)
                $('#buttonLogin').attr('disabled', false);
                $('#loadingLogin').hide();
                const router = app.views.main.router;
                router.navigate(`/sendCode/${formForgotPassword.email}/`);
            },
            error: function (xhr, status) {
                console.log('Error: ' + status);
                console.log(JSON.parse(xhr.responseText).errors)
                const response = JSON.parse(xhr.responseText).errors;
                app.dialog.alert(response[0], 'Error');
                $('#buttonForgotPassword').removeAttr('disabled');
                $('#loadingForgotPassword').hide();
            }
        });
    }
    const validateCode = () => {
        const email = app.views.main.router.currentRoute.params.email
        $('.digit-input').on('input', function () {
            const input = $(this);
            if (input.val().length) {
                const nextInput = input.next();
                if (nextInput.length) {
                    // Si hay otro cuadro de texto, mover el enfoque.
                    nextInput.focus();
                } else {
                    let value = '';
                    $('.digit-input').each(function () {
                        value += $(this).val();
                    });
                    const requestForgotPassword = {
                        "email": email,
                        "code": value
                    }
                    app.request({
                        url: `${getRouteApi()}/congress/v1/api/mail/validate-code`,
                        method: 'POST',
                        contentType: 'application/json',
                        data: requestForgotPassword,
                        success: function (data, status, xhr) {
                            console.log(data)
                            $('#buttonLogin').attr('disabled', false);
                            $('#loadingLogin').hide();
                            console.log(`/resetPassword/${email}/${value}`)
                            const router = app.views.main.router;
                            router.navigate(`/resetPassword/${email}/${value}/`);
                        },
                        error: function (xhr, status) {
                            console.log('Error: ' + status);
                            console.log(JSON.parse(xhr.responseText).errors)
                            const response = JSON.parse(xhr.responseText).errors;
                            app.dialog.alert(response[0], 'Error');
                            $('#buttonForgotPassword').removeAttr('disabled');
                            $('#loadingForgotPassword').hide();

                            $('.digit-input').val('');
                            $('.digit-input').first().focus();
                        }
                    });
                }
            }
        });
    }

    const logicResetPassword = (email) => {
        var isValid = true;
        var newPassword = $("#fg-new-password");
        var valNewPassword = newPassword.val();
        var fgNewPasswordError = $("#fg-new-password-error");

        if (valNewPassword === "") {
            newPassword.addClass('is-invalid');
            fgNewPasswordError.show();
            fgNewPasswordError.text('Por favor, ingresa tu usuario.');
            isValid = false;
        } else if (5 <= valNewPassword.length && valNewPassword.length <= 8) {
            newPassword.addClass('is-invalid');
            fgNewPasswordError.show();
            fgNewPasswordError.text('Por favor, el campo nueva contraseña debe tener minimo 5 y máximo 8 caracteres.');
        } else {
            newPassword.removeClass('is-invalid');
            fgNewPasswordError.text('');
            fgNewPasswordError.hide();
        }

        // Bloquea la sumisión si el formulario no es válido
        const requestResetPassword = {
            "email": email,
            "newPassword": valNewPassword
        }
        return {isValid, requestResetPassword}
    }

    const changePassword = () => {
        const email = app.views.main.router.currentRoute.params.email
        console.log('validate reset password')
        const {isValid, requestResetPassword} = logicResetPassword(email)
        console.log('isValid', isValid, requestResetPassword)
        if (!isValid) {
            return
        }

        loadingForgotPassword = true
        $('#loadingForgotPassword').show();
        $('#buttonForgotPassword').attr('disabled', true);

        app.request({
            url: `${getRouteApi()}/congress/v1/api/mail/change-password`,
            method: 'POST',
            contentType: 'application/json',
            data: requestResetPassword,
            success: function (data, status, xhr) {
                console.log(data)
                app.dialog.alert('usted realizó su cambio de contraseña con éxito','Mensaje', function() {
                    const router = app.views.main.router;
                    router.navigate(`/login/`);
                });
            },
            error: function (xhr, status) {
                console.log('Error: ' + status);
                console.log(JSON.parse(xhr.responseText).errors)
                const response = JSON.parse(xhr.responseText).errors;
                app.dialog.alert(response[0], 'Error');
                $('#buttonForgotPassword').removeAttr('disabled');
                $('#loadingForgotPassword').hide();
            }
        });
        // alert(requestResetPassword.newPassword)
    }
    return {
        sendCode,
        validateCode,
        changePassword,
    }
}