

const $ = Dom7;

export default function layout(app) {
    let user = {}
    const navbarAdmin = (uri) => {
        const $generateNavbar = $('#generate-navbar');
        $generateNavbar.empty();

        const navItems = [
            {href: ["/home/"], icon: "bi bi-house-fill", text: "Inicio"},
            {
                href: [
                    "/event-academic/",
                    "/event-cultural-partner/"
                ], icon: "bi bi-calendar-week-fill", text: "Eventos"
            },
            {
                href: [
                    "/olimpianeic-academic/",
                    "/olimpianeic-cultural-partner/",
                    "/olimpianeic-deport/"
                ],
                icon: "bi bi-award-fill",
                text: "Olimpianeic"
            },
            {
                href: [
                    "/inscription/",
                ], icon: "bi bi-bookmark-check-fill", text: "Inscripciones"
            },
        ];

        let html = '<div class="container"><ul class="nav nav-pills nav-justified">';
        const halfLength = Math.ceil(navItems.length / 2);

        for (let i = 0; i < halfLength; i++) {
            const item = navItems[i];
            const isActive = item.href.includes(uri);
            html += `
            <li class="nav-item">
                <a class="nav-link ${isActive ? 'active' : ''}" href="${item.href[0]}">
                    <span>
                        <i class="nav-icon ${item.icon}"></i>
                        <span class="nav-text">${item.text}</span>
                    </span>
                </a>
            </li>`;
        }

        for (let i = halfLength; i < navItems.length; i++) {
            const item = navItems[i];
            const isActive = item.href.includes(uri);
            html += `
            <li class="nav-item">
                <a class="nav-link ${isActive ? 'active' : ''}" href="${item.href[0]}">
                    <span>
                        <i class="nav-icon ${item.icon}"></i>
                        <span class="nav-text">${item.text}</span>
                    </span>
                </a>
            </li>`;
        }

        html += '</ul></div>';

        $generateNavbar.append(html);
    }
    const navbarUser = (uri) => {
        const $generateNavbar = $('#generate-navbar');
        $generateNavbar.empty();

        const navItems = [
            {href: ["/home/"], icon: "bi bi-house-fill", text: "Inicio"},
            {
                href: [
                    "/event-academic/",
                    "/event-cultural-partner/"
                ], icon: "bi bi-calendar-week-fill", text: "Eventos"
            },
            {
                href: [
                    "/olimpianeic-academic/",
                    "/olimpianeic-cultural-partner/",
                    "/olimpianeic-deport/"
                ],
                icon: "bi bi-award-fill",
                text: "Olimpianeic"
            },
            {
                href: [
                    "/inscription/",
                ], icon: "bi bi-bookmark-check-fill", text: "Inscripciones"
            },
        ];

        let html = '<div class="container"><ul class="nav nav-pills nav-justified">';
        const halfLength = Math.ceil(navItems.length / 2);

        for (let i = 0; i < halfLength; i++) {
            const item = navItems[i];
            const isActive = item.href.includes(uri);
            html += `
            <li class="nav-item">
                <a class="nav-link ${isActive ? 'active' : ''}" href="${item.href[0]}">
                    <span>
                        <i class="nav-icon ${item.icon}"></i>
                        <span class="nav-text">${item.text}</span>
                    </span>
                </a>
            </li>`;
        }

        html += `
        <li class="nav-item centerbutton">
            <button type="button" class="nav-link popup-open" data-popup=".popup-menu">
                <span class="theme-radial-gradient">
                    <i class="bi bi-grid size-22"></i>
                </span>
            </button>
        </li>`;

        for (let i = halfLength; i < navItems.length; i++) {
            const item = navItems[i];
            const isActive = item.href.includes(uri);
            html += `
            <li class="nav-item">
                <a class="nav-link ${isActive ? 'active' : ''}" href="${item.href[0]}">
                    <span>
                        <i class="nav-icon ${item.icon}"></i>
                        <span class="nav-text">${item.text}</span>
                    </span>
                </a>
            </li>`;
        }

        html += '</ul></div>';

        $generateNavbar.append(html);

    }
    const optionsAdmin = () => {
        const $generateMenu = $('#generate-menu')
        $generateMenu.empty()
        const html = `
        <div class="list links-list accordion-list no-margin-top">
            <ul>
                <li>
                    <a href="/panel/sponsor/" class="panel-close">
                        <div class="avatar avatar-40 rounded icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-amd" viewBox="0 0 16 16">
                          <path d="m.334 0 4.358 4.359h7.15v7.15l4.358 4.358V0zM.2 9.72l4.487-4.488v6.281h6.28L6.48 16H.2z"/>
                        </svg>
                        </div>
                         Patrocinadores
                    </a>
                </li>
                <li>
                    <a href="/panel/organizer/" class="panel-close">
                        <div class="avatar avatar-40 rounded icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-buildings-fill" viewBox="0 0 16 16">
                          <path d="M15 .5a.5.5 0 0 0-.724-.447l-8 4A.5.5 0 0 0 6 4.5v3.14L.342 9.526A.5.5 0 0 0 0 10v5.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V14h1v1.5a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5zM2 11h1v1H2zm2 0h1v1H4zm-1 2v1H2v-1zm1 0h1v1H4zm9-10v1h-1V3zM8 5h1v1H8zm1 2v1H8V7zM8 9h1v1H8zm2 0h1v1h-1zm-1 2v1H8v-1zm1 0h1v1h-1zm3-2v1h-1V9zm-1 2h1v1h-1zm-2-4h1v1h-1zm3 0v1h-1V7zm-2-2v1h-1V5zm1 0h1v1h-1z"/>
                        </svg>
                        </div>
                         Organizadores
                    </a>
                </li>
                <li>
                    <a href="/panel/university/" class="panel-close">
                        <div class="avatar avatar-40 rounded icon"><i class="bi bi-bank"></i></div>
                         Universidades
                    </a>
                </li>
                <li>
                    <a href="/panel/participant/" class="panel-close">
                        <div class="avatar avatar-40 rounded icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-backpack2-fill" viewBox="0 0 16 16">
                          <path d="M5 13h6v-3h-1v.5a.5.5 0 0 1-1 0V10H5z"/>
                          <path d="M6 2v.341C3.67 3.165 2 5.388 2 8v1.191l-1.17.585A1.5 1.5 0 0 0 0 11.118V13.5A1.5 1.5 0 0 0 1.5 15h1c.456.607 1.182 1 2 1h7c.818 0 1.544-.393 2-1h1a1.5 1.5 0 0 0 1.5-1.5v-2.382a1.5 1.5 0 0 0-.83-1.342L14 9.191V8a6 6 0 0 0-4-5.659V2a2 2 0 1 0-4 0m2-1a1 1 0 0 1 1 1v.083a6 6 0 0 0-2 0V2a1 1 0 0 1 1-1m0 3a4 4 0 0 1 3.96 3.43.5.5 0 1 1-.99.14 3 3 0 0 0-5.94 0 .5.5 0 1 1-.99-.14A4 4 0 0 1 8 4M4.5 9h7a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-4a.5.5 0 0 1 .5-.5"/>
                        </svg></div>
                         Participantes
                    </a>
                </li>
                <li>
                    <a href="/panel/speaker/" class="panel-close">
                        <div class="avatar avatar-40 rounded icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-earmark-person-fill" viewBox="0 0 16 16">
                              <path d="M9.293 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.707A1 1 0 0 0 13.707 4L10 .293A1 1 0 0 0 9.293 0M9.5 3.5v-2l3 3h-2a1 1 0 0 1-1-1M11 8a3 3 0 1 1-6 0 3 3 0 0 1 6 0m2 5.755V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1v-.245S4 12 8 12s5 1.755 5 1.755"/>
                            </svg>
                        </div>
                         Ponentes
                    </a>
                </li>
                <li>
                    <a href="/panel/location/" class="panel-close">
                        <div class="avatar avatar-40 rounded icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-earmark-person-fill" viewBox="0 0 16 16">
                              <path d="M9.293 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.707A1 1 0 0 0 13.707 4L10 .293A1 1 0 0 0 9.293 0M9.5 3.5v-2l3 3h-2a1 1 0 0 1-1-1M11 8a3 3 0 1 1-6 0 3 3 0 0 1 6 0m2 5.755V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1v-.245S4 12 8 12s5 1.755 5 1.755"/>
                            </svg>
                        </div>
                         Locación
                    </a>
                </li>
                <li>
                    <a href="/statistics/" class="panel-close">
                        <div class="avatar avatar-40 rounded icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clipboard-data-fill" viewBox="0 0 16 16">
                              <path d="M6.5 0A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0zm3 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5z"/>
                              <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1A2.5 2.5 0 0 1 9.5 5h-3A2.5 2.5 0 0 1 4 2.5zM10 8a1 1 0 1 1 2 0v5a1 1 0 1 1-2 0zm-6 4a1 1 0 1 1 2 0v1a1 1 0 1 1-2 0zm4-3a1 1 0 0 1 1 1v3a1 1 0 1 1-2 0v-3a1 1 0 0 1 1-1"/>
                            </svg>
                        </div>
                         Estadísticas
                    </a>
                </li>
                <li>
                    <a href="/calendar/" class="panel-close">
                        <div class="avatar avatar-40 rounded icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clipboard-data-fill" viewBox="0 0 16 16">
                              <path d="M6.5 0A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0zm3 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5z"/>
                              <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1A2.5 2.5 0 0 1 9.5 5h-3A2.5 2.5 0 0 1 4 2.5zM10 8a1 1 0 1 1 2 0v5a1 1 0 1 1-2 0zm-6 4a1 1 0 1 1 2 0v1a1 1 0 1 1-2 0zm4-3a1 1 0 0 1 1 1v3a1 1 0 1 1-2 0v-3a1 1 0 0 1 1-1"/>
                            </svg>
                        </div>
                         Calendario
                    </a>
                </li>
                <li>
                    <a href="/configuration/" class="panel-close">
                        <div class="avatar avatar-40 rounded icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-gear-fill" viewBox="0 0 16 16">
                              <path d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872zM8 10.93a2.929 2.929 0 1 1 0-5.86 2.929 2.929 0 0 1 0 5.858z"/>
                            </svg>
                        </div> Configuración
                        <span class="badge color-orange margin-left-half">new</span>
                    </a>
                </li>
                <li>
                    <a class="panel-close" id="logoutAction">
                        <div class="avatar avatar-40 rounded icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-down-left-square-fill" viewBox="0 0 16 16">
                          <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2zm8.096-10.803L6 9.293V6.525a.5.5 0 0 0-1 0V10.5a.5.5 0 0 0 .5.5h3.975a.5.5 0 0 0 0-1H6.707l4.096-4.096a.5.5 0 1 0-.707-.707"/>
                        </svg>
                        </div> Salir
                    </a >
                </li>
            </ul>
        </div>
        `
        $generateMenu.append(html)
    }
    const optionsUser = () => {
        const $generateMenu = $('#generate-menu')
        $generateMenu.empty()
        const html = `
        <div class="list links-list accordion-list no-margin-top">
            <ul>
                <li>
                    <a href="/configuration/" class="panel-close">
                        <div class="avatar avatar-40 rounded icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-gear-fill" viewBox="0 0 16 16">
                              <path d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872zM8 10.93a2.929 2.929 0 1 1 0-5.86 2.929 2.929 0 0 1 0 5.858z"/>
                            </svg>
                        </div> Configuración
                        <span class="badge color-orange margin-left-half">new</span>
                    </a>
                </li>                
                <li>
                    <a href="/calendar/" class="panel-close">
                        <div class="avatar avatar-40 rounded icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clipboard-data-fill" viewBox="0 0 16 16">
                              <path d="M6.5 0A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0zm3 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5z"/>
                              <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1A2.5 2.5 0 0 1 9.5 5h-3A2.5 2.5 0 0 1 4 2.5zM10 8a1 1 0 1 1 2 0v5a1 1 0 1 1-2 0zm-6 4a1 1 0 1 1 2 0v1a1 1 0 1 1-2 0zm4-3a1 1 0 0 1 1 1v3a1 1 0 1 1-2 0v-3a1 1 0 0 1 1-1"/>
                            </svg>
                        </div>
                         Calendario
                    </a>
                </li>
                <li>
                    <a class="panel-close" id="logoutAction">
                        <div class="avatar avatar-40 rounded icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-down-left-square-fill" viewBox="0 0 16 16">
                          <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2zm8.096-10.803L6 9.293V6.525a.5.5 0 0 0-1 0V10.5a.5.5 0 0 0 .5.5h3.975a.5.5 0 0 0 0-1H6.707l4.096-4.096a.5.5 0 1 0-.707-.707"/>
                        </svg>
                        </div> Salir
                    </a >
                </li>
            </ul>
        </div>
        `
        $generateMenu.append(html)

    }
    const generateComponentQR = () => {
        console.log('generateComponentQR')
        const $cQR = $('#generate-cQR')
        $cQR.empty()
        const idQrGenerate = "area-para-qr-" + Date.now() + Math.floor(Math.random() * 1000);
        const selectIdQrGenerate = $(`#${idQrGenerate}`)
        console.log('idQrGenerate', idQrGenerate)
        const html = `
        <div class="popup popup-menu elevation-2 popup-swipe-to-close bg-color-2 c-white">
           <div class="row h-100 justify-content-center align-item-center">
                <div class="col-100 align-self-center">
                  <h1 class="margin-bottom text-align-center"><span class=" fw-light">Muestra tu</span><br />QR</h1>
                  <div class="text-align-center">
                    <div class="qr-content">
                      <div id="${idQrGenerate}" class="qr-generate"></div>
                    </div>
                    <p class="small margin-bottom">Para poder ingresar a los distintos<br />eventos</p>
                  </div>
                </div>
              </div>
            </div>
        `
        $cQR.append(html)
        setTimeout(() => generateQR(idQrGenerate), 100)

    }

    const generateComponentBenefit = () => {
        console.log('generateComponentBenefit')
        const $cQR = $('#generate-cBenefit')
        $cQR.empty()
        const idBenefitGenerate = "area-para-benefit-" + Date.now() + Math.floor(Math.random() * 1000);
        console.log('idBenefitGenerate', idBenefitGenerate)
        const html = `
        <div class="popup popup-menu-benefit elevation-2 popup-swipe-to-close bg-color-2 c-white" id="modal-benefit">
           <div class="row h-100 justify-content-center align-item-center">
                <div class="col-100 align-self-center">
                  <h1 class="margin-bottom text-align-center"><span class=" fw-light">Beneficio</span></h1>
                  <div class="text-align-center">
                  <div class="row margin-vertical text-align-center" id="benefit-content">
                  </div>
                </div>
              </div>
            </div>
        `
        $cQR.append(html)

    }
    const generateQR = (id) => {
        console.log('generate QR');
        var dniUsuario = user.datos.dni; // Reemplaza esto con el DNI real del usuario

        // Comprueba si el elemento con el ID generado existe
        var $qrAreaElement = $(`#${id}`);

        if ($qrAreaElement.length) {
            console.log($qrAreaElement)
            var qrcode = new QRCode($qrAreaElement[0], {
                text: dniUsuario,
                width: 128,
                height: 128,
                colorDark: "#000000",
                colorLight: "#ffffff",
            });

            const user = JSON.parse(localStorage.getItem('user'))
            const role = user.rol
            if (role !== 'ADMIN') {
                closePopup()
            }
        } else {
            console.log("El elemento " + id + " no se encuentra en el documento.");
        }
    }
    const closePopup = () => {
        // $('.centerbutton .nav-link').on('click', function () {
        //     $(this).toggleClass('active')
        // })
        const swipeToClosePopup = app.popup.create({
            el: '.popup-swipe-to-close',
            swipeToClose: true,
        });
        swipeToClosePopup.on('close', function (popup) {
            $('.centerbutton .nav-link').removeClass('active')
        });
    }
    const generateOptions = (uri) => {
        user = JSON.parse(localStorage.getItem('user'))
        let stage = user?.datos?.stage ? `img/app/stages/${user.datos.stage}.png` : 'img/user1.jpg';
        console.log("stage current profile:: ", stage)
        $('#img_stage').attr('src', stage);
        const role = user.rol
        const sidebarUser = $("#sidebar-user")
        if (role === 'ADMIN') {
            optionsAdmin()
            navbarAdmin(uri)
            sidebarUser.empty()
            const html = `
                <h5>${user.user}</h5>
                <p class="text-secondary-50 size-12">${role}</p>
            `
            sidebarUser.append(html)
        } else {
            generateComponentQR()
            generateComponentBenefit()
            optionsUser()
            navbarUser(uri)
            sidebarUser.empty()
            const html = `
                <h5>${user.user}</h5>
                <p class="text-secondary-50 size-12 text-uppercase">${user.datos.promotion}</p>
            `
            sidebarUser.append(html)
        }
    }
    return {
        generateOptions,
        closePopup
    }
}