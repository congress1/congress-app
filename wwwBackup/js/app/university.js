import GenerateCrud from "./generateCrud.js";

export default function university(app) {
    let entity = 'university'
    let attribute = [
        {
            label: 'Nombre',
            value: 'name',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Acronimo',
            value: 'acronym',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Lugar',
            value: 'location',
            type: 'Text',
            isShow: false,
            required: true
        },
    ]
    const generateCrud = new GenerateCrud(entity, attribute, 'containerGCUniversity', app);
    
    const initGenerateCrud = () => {
        console.log('initGenerateCrud', entity);
        return generateCrud.initGenerateCrud("popup-form", "Nuevo formulario")
    }
    const initForm = () => {
        console.log('init form', entity);
        return generateCrud.initForm()
    }
    const generateList = () => {
        console.log('init list', entity);
        return generateCrud.initList()
    }
    return {
        initGenerateCrud,
        initForm,
        generateList
    }



}