import getRouteApi from "../api.js";

const $ = Dom7;

export default function confirmCode(app) {
    const validateCode = () => {
        const email = app.views.main.router.currentRoute.params.email
        $('.digit-input-2').on('input', function () {
            const input = $(this);
            if (input.val().length) {
                const nextInput = input.next();
                if (nextInput.length) {
                    // Si hay otro cuadro de texto, mover el enfoque.
                    nextInput.focus();
                } else {
                    let value = '';
                    $('.digit-input-2').each(function () {
                        value += $(this).val();
                    });
                    const requestForgotPassword = {
                        "email": email,
                        "code": value,
                        "type": "2"
                    }
                    app.request({
                        url: `${getRouteApi()}/congress/v1/api/mail/validate-code`,
                        method: 'POST',
                        contentType: 'application/json',
                        data: requestForgotPassword,
                        success: function (data, status, xhr) {
                            console.log(data)
                        },
                        error: function (xhr, status) {
                            console.log('Error: ' + status);
                            // console.log(JSON.parse(xhr.responseText).errors)
                            // const response = JSON.parse(xhr.responseText).errors;
                            // app.dialog.alert(response[0], 'Error');

                            $('.digit-input-2').val('');
                            $('.digit-input-2').first().focus();
                        }
                    });
                }
            }
        });
    }

    return {
        validateCode
    }
}