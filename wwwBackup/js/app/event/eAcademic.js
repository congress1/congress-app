import GEvent from "./gEvent.js";

export default function eAcademic(app) {

    let attributes = [
        {
            label: 'Nombre',
            value: 'name',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Descripción',
            value: 'description',
            type: 'Text',
            isShow: true,
            required: true
        },
        {
            label: 'Fecha Inicio',
            value: 'date',
            type: 'Date',
            isShow: true,
            default: new Date(),
            required: true
        },
        {
            label: 'Fecha Fin',
            value: 'endDate',
            type: 'Date',
            values: {
                valueEdit: 'end_date'
            },
            isShow: true,
            default: new Date(),
            required: true
        },
        {
            label: 'Hora de inicio (09:00)',
            value: 'startTime',
            values: {
                valueEdit: 'start_time'
            },
            type: 'Time',
            isShow: true,
            required: true
        },
        {
            label: 'Hora de fin (09:00)',
            value: 'endTime',
            values: {
                valueEdit: 'end_time'
            },
            type: 'Time',
            isShow: true,
            required: true
        },
        {
            label: 'Color Label',
            value: 'labelColor',
            default: '#FFA500',
            type: 'Color',
            isShow: true,
            required: true
        },
        {
            label: 'Acumula horas',
            value: 'isAccumulation',
            values: {
                valueEdit: 'is_accumulation'
            },
            default: false,
            type: 'Boolean',
            isShow: false,
            required: false
        },
        {
            label: 'Ponente',
            value: 'speakerId',
            values: {
                valueEdit: 'speaker.id'
            },
            type: 'Table',
            uri: 'speaker/list-all',
            isShow: true,
            required: true
        },
        {
            label: 'Locación',
            value: 'locationId',
            values: {
                valueEdit: 'location.id'
            },
            type: 'Table',
            uri: 'location/list-all',
            isShow: true,
            required: true
        },
        {
            label: 'typeCode',
            value: 'typeCode',
            values: {
                valueEdit: 'type_code_internal'
            },
            default: 'TIEV001',
            type: 'Hidden',
            isShow: true,
            required: true
        },
        {
            label: 'categoryCode',
            value: 'categoryCode',
            default: 'TICT001',
            type: 'Hidden',
            isShow: true,
            required: true
        },
        {
            label: 'Eje tematico',
            value: 'themeCode',
            values: {
                //valueList: 'type.name',
                valueEdit: 'theme_code_internal'
            },
            default: null,
            type: 'Table',
            keyTable: 'code',
            uri: 'setting/list-internal-code/TIET000?internalCode=true',
            isShow: false,
            // required: true
        },
        {
            label: 'Tipo de evento',
            value: 'classCode',
            values: {
                valueList: 'type.name',
                valueEdit: 'class_code_internal'
            },
            keyTable: 'code',
            type: 'Table',
            uri: 'setting/list-internal-code/TICL000?internalCode=true',
            isShow: false,
            required: true
        },
    ]
    const gEvent = new GEvent(app, 'academic', 'TICT001/TIEV001', attributes)
    const init = () => {
        gEvent.init()
    }
    return {
        init
    }
}