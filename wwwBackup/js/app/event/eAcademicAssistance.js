import getRouteApi from "../api.js";
import FormGenerator from "../formGenerator.js";

const $ = Dom7;

export default function eAcademicAssistance(app) {

    const submit = (dni) => {
        const eventId = app.views.main.router.currentRoute.params.eventId
        alert(`eventId: ${eventId} dni: ${dni}`)
        inscriptionAttendance(eventId, dni)
    }
    const inscriptionAttendance = (eventId, documentNumber) => {
        app.request({
            url: `${getRouteApi()}/congress/v1/api/inscription/attendance`,
            method: 'POST',
            contentType: 'application/json',
            data: {
                eventId,
                documentNumber
            },
            // headers: {
            //     'Authorization': 'Bearer ' + localStorage.getItem('token') // reemplaza token con tu token
            // },
            success: (data, status, xhr) => {
                const dataL = JSON.parse(data)
                console.log(dataL)
                alert(data)
            },
            error: function (xhr, status) {
                console.log('Error: ' + status);
            }
        });
    }
    const assistanceManual = () => {
        const eventId = app.views.main.router.currentRoute.params.eventId
        console.log(app.views.main.router)
        let attributes = [
            {
                label: 'Evento',
                value: 'eventId',
                type: 'Hidden',
                default: eventId,
                isShow: true,
                required: true
            },
            {
                label: 'DNI',
                value: 'documentNumber',
                type: 'Text',
                isShow: true,
                required: true
            },
        ]
        const formGenerator = new FormGenerator('inscription-attendance', attributes, 'containerManual', '', app)
        formGenerator.initForm()
        formGenerator.setUriCustomSubmit('inscription/attendance')
    }
    const initLinksAssistance = () => {
        const eventId = app.views.main.router.currentRoute.params.eventId
        $('#button-go-assistance').on('click', (e) => {
            console.log('go-assistance-manual clicked');
            app.views.main.router.navigate(`/event-academic/assistance/${eventId}/`);
        })
        $('#button-go-assistance-manual').on('click', (e) => {
            console.log('go-assistance-manual clicked');

            app.views.main.router.navigate(`/event-academic/assistance-manual/${eventId}/`);
        })
        $('#button-go-assistance-participant').on('click', (e) => {
            console.log('go-assistance-manual clicked');

            app.views.main.router.navigate(`/event-academic/assistance-participant/${eventId}/`);
        })
    }
    return {
        submit,
        assistanceManual,
        initLinksAssistance,
    }
}