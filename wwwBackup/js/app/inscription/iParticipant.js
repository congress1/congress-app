import ListGenerator from "../listGenerator.js";

export default function iParticipant(app) {
    const init = () => {
        const type = app.views.main.router.currentRoute.params.type

        let attributes = [
            {
                label: 'Nombre',
                value: 'name',
                type: 'Text',
                isShow: true,
                required: true
            },
            {
                label: 'Universidad',
                value: 'university',
                values: {
                    valueList: type === 'free' ? 'university' : 'university.name',
                },
                type: 'Text',
                isShow: true,
                required: true
            },
        ]

        const eventId = app.views.main.router.currentRoute.params.eventId
        console.log('eventId', eventId)
        const gList = new ListGenerator(type === 'free' ? 'event/list-participants-event/' + eventId : 'event/list-inscription-event/' + eventId, attributes, 'participant-container', '', app)
        gList.initList()
    }
    return {
        init
    }
}