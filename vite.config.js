// vite.config.js
import {defineConfig} from 'vite'
import path from "path";

const SRC_DIR = path.resolve(__dirname, './src');
const PUBLIC_DIR = path.resolve(__dirname, './src/public');
const BUILD_DIR = path.resolve(__dirname, './www',);
export default defineConfig({
    root: SRC_DIR,
    publicDir: PUBLIC_DIR,
    base: '',
    build: {
        outDir: BUILD_DIR,
        assetsInlineLimit: 0,
        emptyOutDir: true,
        rollupOptions: {
            treeshake: false,
        },
    },
})